FROM registry.gitlab.com/panterasbox/panterasbot/docker-node:latest

ARG NPM_TOKEN

COPY . /panterasbot
WORKDIR /panterasbot
RUN npm config set always-auth true
RUN npm config set -- "//panterasbox.bytesafe.dev/r/panterasbox/:_authToken" "${NPM_TOKEN}"
RUN yarn config set registry https://panterasbox.bytesafe.dev/r/panterasbox/
RUN yarn install
RUN yarn build

EXPOSE 2050
 
CMD npm run start
