#!/bin/bash

docker run -d \
  -p 2050:2050 \
  --mount type=bind,src=/var/aws/etc/panterasbot,dst=/root/.aws \
  --name panterasbot \
  registry.gitlab.com/panterasbox/panterasbot/panterasbot/master:%TAG%

docker logs -f panterasbot >> /var/log/docker/panterasbot.log 2>&1 &
