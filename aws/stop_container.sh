#!/bin/bash

CONTAINER=`docker ps -a -q --filter name=panterasbot --format="{{.ID}}"`

if [ -z $CONTAINER ];
then 
  echo "No container running."
else 
  docker rm $(docker stop $CONTAINER);
fi

docker container prune -f;
docker volume prune -f;
docker image prune -af;
