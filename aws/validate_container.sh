#!/bin/bash

curl localhost:2050 -s -f -o /dev/null; 
HTTPSTATUS=$?

if [[ $HTTPSTATUS -ne 0 ]];
then 
  echo "Health check failed.";
  exit $?
else
  echo "Health check passed.";
  docker container prune -f;
  docker volume prune -f;
  docker image prune -af;
  exit 0
fi
