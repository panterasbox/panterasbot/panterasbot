import { gql } from "@apollo/client";
import { Player } from "../components/App/recoil";
import { capitalize } from "./util";

export const getFullName = (player?: Player | null) => {
  if (!player) {
    return undefined;
  }
  return player.lastName
    ? `${capitalize(player.firstName)} ${capitalize(player.lastName)}`
    : capitalize(player.firstName);
};

export const playerQuery = gql`
  query getPlayer($playerId: ID) {
    player(id: $playerId) {
      id
      _id
      firstName
      lastName
      avatar
      user {
        id
        _id
        avatar
        isGuest
        isStreamer
      }
    }
  }
`;
