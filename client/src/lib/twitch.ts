import { gql } from "@apollo/client";

export const streamQuery = gql`
  query getTwitchStream {
    twitchStream {
      id
      _id
      externalId
      gameId
      language
      title
      type
      thumbnailUrl
      startedAt
      userId
      userName
      viewerCount
      tagIds
      createdAt
      updatedAt
    }
  }
`;
