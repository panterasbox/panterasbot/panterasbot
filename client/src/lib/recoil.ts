import { RecoilState, RecoilValue } from "recoil";
import { recoil } from "../components/Backend/BackendProvider";

export type ValOrUpdater<T> = T | ((currVal: T) => T);

export interface RecoilAccessor {
  get?: <T>(atom: RecoilValue<T>) => T;
  resolve?: <T>(atom: RecoilValue<T>) => Promise<T>;
  set?: <T>(atom: RecoilState<T>, valOrUpdater: ValOrUpdater<T>) => void;
  reset?: (atom: RecoilState<any>) => void;
}

export function recoilGet<T>(atom: RecoilValue<T>): T {
  return recoil.get!(atom);
}

export function recoilResolve<T>(atom: RecoilValue<T>): Promise<T> {
  return recoil.resolve!(atom);
}

export function recoilSet<T>(
  atom: RecoilState<T>,
  valOrUpdater: ValOrUpdater<T>
): void {
  return recoil.set!(atom, valOrUpdater);
}

export function recoilReset(atom: RecoilState<any>): void {
  return recoil.reset!(atom);
}
