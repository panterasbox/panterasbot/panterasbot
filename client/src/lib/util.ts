import moment from "moment";

export const serverUrl = (websocket = false) => {
  return (
    (websocket
      ? window.location.protocol === "https:"
        ? "wss:"
        : "ws:"
      : window.location.protocol === "https:"
      ? "https:"
      : "http:") +
    "//" +
    window.location.hostname +
    ":" +
    (process.env.NODE_ENV == "development" ? 2050 : window.location.port)
  );
};

export class StringBuffer {
  buffer = new Array<string>();
  index = 0;

  constructor(str?: string) {
    str && this.append(str);
    this.index = str ? 1 : 0;
  }

  append(str: string) {
    this.buffer[this.index] = str;
    this.index += 1;
    return this;
  }

  prepend(str: string) {
    this.buffer.unshift(str);
    this.index += 1;
    return this;
  }

  toString() {
    return this.buffer.join("");
  }
}

export const buildTemplate = <T extends any[]>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  ...transforms: Function[]
): ((parts: TemplateStringsArray, ...args: T) => string) => {
  return (parts: TemplateStringsArray, ...args: T): string => {
    const result = new StringBuffer();
    for (let i = 0; i < parts.length; i++) {
      result.append(parts[i]);
      result.append(transforms[i](args[i]));
    }
    return result.toString();
  };
};

export const formatDuration = (duration: moment.Duration): string => {
  let negative = false;
  if (duration.asMilliseconds() < 0) {
    duration = duration.abs();
    negative = true;
  }

  const sb = new StringBuffer(String(duration.seconds()).padStart(2, "0"));
  if (duration.minutes()) {
    sb.prepend(":");
    sb.prepend(String(duration.minutes()).padStart(2, "0"));
  } else {
    if (duration.hours()) {
      sb.prepend("00:");
    } else {
      sb.prepend("0:");
    }
  }
  if (duration.hours()) {
    sb.prepend(":");
    sb.prepend(duration.hours().toString());
  }
  if (negative) {
    sb.prepend("-");
  }
  return sb.toString();
};

export const remainingDuration = (seconds: number, time: number): number => {
  time = time / 1000;
  const current = moment().valueOf() / 1000;
  return seconds - (current - time);
};

export const capitalize = (str: string): string => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};
