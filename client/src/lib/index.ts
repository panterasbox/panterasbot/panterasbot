export interface ActionMeta {
  sentTime?: number;
  receivedTime?: number;
}

export interface PayloadAction<T> {
  type: string;
  meta?: ActionMeta;
  payload: T;
}
