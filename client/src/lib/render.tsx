import React from "react";
import { Box } from "@mui/material";
import styled from "styled-components";
import { MessagePayload } from "../components/Backend";
import Exit from "../components/Terminal/Exit";

const Message = styled.div``;

const Echo = styled.div``;

const Paragraph = styled.p<{ $indent: string }>`
  ${(props) => `text-indent: ${props.$indent};`}
  margin-block-start: 0.5em;
  margin-block-end: 0.5em;
`;

const Inline = styled.span``;

const Prefix = styled.span`
  margin-right: 0.5em;
`;

const Location = styled.h1`
  font-size: inherit;
  font-weight: inherit;
  line-height: inherit;
`;

const Exits = styled.ol`
  display: inline;
  list-style-type: none;
  padding-left: 0;
`;

const Inventory = styled.ol`
  list-style-type: none;
  padding-left: 0;
  margin-block-start: 0;
`;

const Stuff = styled.li``;

const Voice = styled.div``;

interface Renderer {
  render(message: string): JSX.Element;
}

export class RendererFactory {
  static getRenderer(message: MessagePayload): Renderer {
    if (message.topic.startsWith("vision.")) {
      const params = (message.params as unknown) as VisionParams;
      if (params.target.type === "location") {
        return new LocationRenderer(
          (message.params as unknown) as LocationParams
        );
      }
    } else if (message.topic.startsWith("voice.say.")) {
      return new VoiceRenderer((message.params as unknown) as VoiceParams);
    }
    return new BasicRenderer();
  }
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface DoorParam {}

export interface ExitParam {
  direction: string;
  path: string;
  door?: DoorParam;
  destination?: string;
  hidden: boolean;
  blocked: boolean;
  muffled: boolean;
  noFollow: boolean;
}

export interface InventoryParam {
  id: string;
  template: string | undefined;
}

interface VisionParams {
  target: {
    type: string;
    id: string;
    template: string | undefined;
  };
}

interface LocationParams extends VisionParams {
  inventory: InventoryParam[];
  exits: Record<string, ExitParam>;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface VoiceParams {}

class BasicRenderer implements Renderer {
  renderNode(node: Node, key = 0): JSX.Element {
    if (node.nodeType == Node.TEXT_NODE) {
      return <span key={`text${key}`}>{node.nodeValue}</span>;
    }

    const children: JSX.Element[] = [];
    node.childNodes.forEach(
      (value: ChildNode, key: number, parent: NodeListOf<ChildNode>) => {
        children.push(this.renderNode(value, key));
      }
    );

    const empty = <React.Fragment key={`node${key}`}></React.Fragment>;
    if (node.nodeType != Node.ELEMENT_NODE) {
      return empty;
    }

    const el = node as Element;
    return this.renderElement(el, children, key) || empty;
  }

  render(message: string): JSX.Element {
    const doc = new DOMParser().parseFromString(message, "text/xml");
    return this.renderNode(doc.documentElement);
  }

  renderElement(
    el: Element,
    children: JSX.Element[],
    key: number
  ): JSX.Element | undefined {
    switch (el.nodeName) {
      case "message":
        return <Message key={`message${key}`}>{children}</Message>;
      case "echo":
        return <Echo key={`echo${key}`}>{children}</Echo>;
      case "p":
        const indentAttr = el.attributes.getNamedItem("indent");
        let indent = "0";
        if (indentAttr) {
          indent = indentAttr.value;
        }
        return (
          <Paragraph key={`paragraph${key}`} $indent={indent}>
            {children}
          </Paragraph>
        );
      case "flex":
        return (
          <Box
            key={`box${key}`}
            sx={{
              display: "flex",
              flexWrap: "nowrap",
            }}
          >
            {children}
          </Box>
        );
      case "inline":
        return <Inline key={`inline${key}`}>{children}</Inline>;
      default:
        return undefined;
    }
  }
}

class LocationRenderer extends BasicRenderer {
  params: LocationParams;

  constructor(params: LocationParams) {
    super();
    this.params = params;
  }

  renderElement(
    el: Element,
    children: JSX.Element[],
    key: number
  ): JSX.Element | undefined {
    const basicResult = super.renderElement(el, children, key);
    if (basicResult) {
      return basicResult;
    }

    switch (el.nodeName) {
      case "location":
        return <Location key={`location${key}`}>{children}</Location>;
      case "exits":
        return <Exits key={`exits${key}`}>{children}</Exits>;
      case "exit":
        const directionAttr = el.attributes.getNamedItem("indent");
        let exit: ExitParam | undefined = undefined;
        if (directionAttr) {
          exit = this.params.exits[directionAttr.value];
        }
        return (
          <Exit exit={exit} key={`exit${key}`}>
            {children}
          </Exit>
        );
      case "inventory":
        return <Inventory key={`inventory${key}`}>{children}</Inventory>;
      case "stuff":
        return <Stuff key={`stuff${key}`}>{children}</Stuff>;
      case "blocked":
        return <React.Fragment key={`fragment${key}`}>🚫</React.Fragment>;
      default:
        return (
          <React.Fragment key={`fragment${key}`}>{children}</React.Fragment>
        );
    }
  }
}

class VoiceRenderer extends BasicRenderer {
  params: VoiceParams;

  constructor(params: VoiceParams) {
    super();
    this.params = params;
  }

  renderElement(
    el: Element,
    children: JSX.Element[],
    key: number
  ): JSX.Element | undefined {
    const basicResult = super.renderElement(el, children, key);
    if (basicResult) {
      return basicResult;
    }

    switch (el.nodeName) {
      case "voice":
        return <Voice key={`voice${key}`}>{children}</Voice>;
      case "prefix":
        return <Prefix key={`prefix${key}`}>{children}</Prefix>;
      case "subject":
        return <Inline key={`subject${key}`}>{children}</Inline>;
      default:
        return (
          <React.Fragment key={`fragment${key}`}>{children}</React.Fragment>
        );
    }
  }
}
