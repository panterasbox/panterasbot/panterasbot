import { createTheme } from "@mui/material/styles";
import { indigo } from "@mui/material/colors";

declare module "@mui/material/styles/createTheme" {
  interface Theme {
    drawerWidth: number;
  }
  interface ThemeOptions {
    drawerWidth: number;
  }
}

const defaultTheme = createTheme({
  palette: {
    primary: indigo,
    secondary: indigo,
  },
  drawerWidth: 200,
});

export { defaultTheme };
