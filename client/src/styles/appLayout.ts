export const toolbarMixin = (toolbar: any) => `
  min-height: ${toolbar.minHeight}px;
  @media (min-width:0px) and (orientation: landscape) {
    min-height: ${toolbar["@media (min-width:0px) and (orientation: landscape)"].minHeight}px;
  }
  @media (min-width:600px) {
    min-height: ${toolbar["@media (min-width:600px)"].minHeight}px;
  }
`;

export const monospaceMixin = () => `
  font-family: "Cascadia Code", "Consolas", "Lucida Console", monospace;
`;
