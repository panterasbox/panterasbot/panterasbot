import * as React from "react";
import * as ReactDOM from "react-dom";
import { RecoilRoot } from "recoil";
import { ApolloProvider } from "@apollo/client";
import { StyledEngineProvider, CssBaseline } from "@mui/material";
import { ErrorBoundary } from "react-error-boundary";
import { defaultTheme } from "./styles/themes";
import ThemeProvider from "./styles/ThemeProvider";
import * as serviceWorker from "./serviceWorker";
import { serverUrl } from "./lib/util";
import App from "./components/App/App";
import ErrorFallback from "./components/App/ErrorFallback";
import BackendProvider from "./components/Backend/BackendProvider";
import apolloClient from "./components/Backend/ApolloClient";

fetch(serverUrl() + "/", {
  mode: "cors",
  cache: "no-cache",
  credentials: "include",
})
  .then((response) => {
    ReactDOM.render(
      <React.StrictMode>
        <RecoilRoot>
          <BackendProvider>
            <ApolloProvider client={apolloClient}>
              <StyledEngineProvider injectFirst>
                <ThemeProvider theme={defaultTheme}>
                  <CssBaseline />
                  <ErrorBoundary FallbackComponent={ErrorFallback}>
                    <React.Suspense fallback={<h1>Loading...</h1>}>
                      <App />
                    </React.Suspense>
                  </ErrorBoundary>
                </ThemeProvider>
              </StyledEngineProvider>
            </ApolloProvider>
          </BackendProvider>
        </RecoilRoot>
      </React.StrictMode>,
      document.getElementById("root")
    );
  })
  .catch((e) => {
    console.warn(e);
  });

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
