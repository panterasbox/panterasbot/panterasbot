import { Box } from "@mui/material";
import React from "react";
import styled from "styled-components";

const ActionBox = styled(Box)`
  width: 50%;
  flex: 1;
`;

const ActionPane = () => {
  return <ActionBox>Action!</ActionBox>;
};

export default ActionPane;
