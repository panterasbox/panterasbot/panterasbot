import { ApolloClient, InMemoryCache, createHttpLink } from "@apollo/client";
import { serverUrl } from "../../lib/util";

const GRAPHQL_URL = serverUrl() + "/graphql";

const link = createHttpLink({
  uri: GRAPHQL_URL,
  credentials: "include",
});

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link,
});

export default client;
