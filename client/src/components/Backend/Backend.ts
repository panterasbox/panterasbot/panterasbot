import { EventEmitter } from "events";
import moment from "moment";
import queue from "queue";
import {
  CommandPayload,
  ConnectionPayload,
  ConnectPayload,
  MessagePayload,
  StreamStatusPayload,
  ViewersPayload,
} from ".";
import { PayloadAction, ActionMeta } from "../../lib";
import { recoilGet, recoilSet, recoilResolve } from "../../lib/recoil";
import { serverUrl } from "../../lib/util";
import { appInteractiveState, appPlayerState, Player } from "../App/recoil";
import { streamStandby } from "../Overlay/recoil";
import { messagesState, viewersState } from "../StreamerDash/recoil";
import { promptState } from "../Terminal/recoil";

const WEBSOCKET_URL = serverUrl(true) + "/backend";

export type UserAgentPayload = string;

class BackendEmitter extends EventEmitter {}

export const eventEmitter = new BackendEmitter();

export class Backend {
  socket: WebSocket;
  msgQueue: queue = queue({ concurrency: 1 });

  constructor() {
    eventEmitter.on("ready", (e) => {
      if (this.msgQueue) {
        this.msgQueue.start();
        this.msgQueue.autostart = true;
      }
    });
    eventEmitter.on("halt", (e) => {
      if (this.msgQueue) {
        this.msgQueue.stop();
        this.msgQueue.autostart = false;
      }
    });

    const ws = new WebSocket(WEBSOCKET_URL);

    ws.onmessage = (event: MessageEvent<string>) => {
      this.msgQueue && this.msgQueue.push(() => this.receive(event.data));
    };

    ws.onopen = (event: Event) => {
      const message: PayloadAction<UserAgentPayload> = {
        type: "client/userAgent",
        payload: navigator.userAgent,
        meta: { sentTime: moment().valueOf() },
      };
      const data = JSON.stringify(message);
      return this.socket.send(data);
    };

    ws.onclose = (event: CloseEvent) => {
      this.msgQueue && this.msgQueue.end();
    };

    this.socket = ws;
  }

  async receive(data: string) {
    const message: PayloadAction<any> = JSON.parse(data);
    if (!message.meta) {
      message.meta = {};
    }
    message.meta.receivedTime = moment().valueOf();
    console.debug("Received", message);

    if (message.type === "websocket/message") {
      this.doMessage(message.payload, message.meta);
      switch (message.payload.topic) {
        case "system.connect":
          this.doConnect(message.payload);
          break;
        case "system.viewerChange":
          await this.doViewers(message.payload);
          break;
        case "notify.streamStatus":
          await this.doStreamStatus(message.payload);
          break;
        case "notify.connection":
          await this.doConnection(message.payload);
          break;
        case "mudlog.error":
        case "mudlog.warn":
        case "mudlog.info":
        case "mudlog.debug":
        case "mudlog.trace":
          break;
        default:
          break;
      }
    } else if (message.type === "websocket/prompt") {
      this.doPrompt(message.payload);
    } else if (message.type === "websocket/echo") {
      this.doEcho(message.payload, message.meta);
    }
  }

  send(message: PayloadAction<CommandPayload>): void {
    if (!message.meta) {
      message.meta = {};
    }
    message.meta.sentTime = moment().valueOf();
    console.debug("Sending", message);
    const data = JSON.stringify(message);
    return this.socket.send(data);
  }

  disconnect() {
    return this.socket.close();
  }

  doMessage(payload: MessagePayload, meta: ActionMeta) {
    const messages = recoilGet(messagesState);
    recoilSet(messagesState, [
      ...messages,
      [payload, meta] as [MessagePayload, ActionMeta],
    ]);
  }

  doConnect(payload: ConnectPayload) {
    recoilSet(appInteractiveState, payload.params.interactiveId);
  }

  async doConnection(payload: ConnectionPayload) {
    const params = payload.params;
    const interactiveId = await recoilResolve(appInteractiveState);
    if (params.event == "event.connect") {
      if (params.interactiveId == interactiveId) {
        recoilSet(appPlayerState, params.player);
      }
    } else if (params.event == "event.disconnect") {
      if (params.interactiveId == interactiveId) {
        recoilSet(appPlayerState, null);
      }
    }
  }

  async doViewers(payload: ViewersPayload) {
    const viewers = await recoilResolve(viewersState);
    recoilSet(viewersState, payload.params.viewers);
    if (viewers < 0 || payload.params.viewers > viewers) {
      const audio = new Audio(
        "https://panterasbox.s3.us-west-2.amazonaws.com/viewerUp.ogg"
      );
      audio.play();
    }
  }

  doPrompt(payload: string) {
    recoilSet(promptState, payload);
  }

  doEcho(payload: string, meta: ActionMeta) {
    this.doMessage(
      {
        topic: "system.echo",
        message: `<echo>${payload}</echo>`,
        params: {},
      },
      meta
    );
  }

  doStreamStatus(payload: StreamStatusPayload) {
    console.log("setting status", payload);
    recoilSet(streamStandby, payload.params);
  }
}

const backend = new Backend();
export default backend;
