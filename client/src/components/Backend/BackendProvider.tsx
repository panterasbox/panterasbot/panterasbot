import React, { createContext, useEffect } from "react";
import backend, { Backend, eventEmitter } from "./Backend";
import { RecoilState, RecoilValue, useRecoilCallback } from "recoil";
import { RecoilAccessor, ValOrUpdater } from "../../lib/recoil";

export const recoil: RecoilAccessor = {};

type ContextValue = Backend | null;

const BackendContext = createContext<ContextValue>(null);

function BackendProvider({ children }: any) {
  recoil.get = useRecoilCallback<[RecoilValue<any>], any>(
    ({ snapshot }) =>
      function <T>(atom: RecoilValue<T>): T {
        return snapshot.getLoadable(atom).contents;
      },
    []
  );

  recoil.resolve = useRecoilCallback<[RecoilValue<any>], Promise<any>>(
    ({ snapshot }) =>
      function <T>(atom: RecoilValue<T>): Promise<T> {
        return snapshot.getPromise(atom);
      },
    []
  );

  recoil.set = useRecoilCallback<
    [RecoilState<any>, any | ValOrUpdater<any>],
    void
  >(({ set }) => set, []);

  recoil.reset = useRecoilCallback<[RecoilState<any>], void>(
    ({ reset }) => reset,
    []
  );

  useEffect(() => {
    eventEmitter.emit("ready");
    return () => {
      eventEmitter.emit("halt");
    };
  });

  return (
    <BackendContext.Provider value={backend}>
      {children}
    </BackendContext.Provider>
  );
}

export default BackendProvider;
