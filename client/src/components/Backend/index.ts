import deepmerge from "deepmerge";
import moment from "moment";
import { Player } from "../App/recoil";
import { StreamStatus } from "../Overlay/recoil";
import backend from "./Backend";

export type TopicName = string;

export interface IMessagePayload {
  topic: TopicName;
  message: string;
  params: Record<string, unknown>;
}

export class MessagePayload implements IMessagePayload {
  topic!: string;
  message!: string;
  params!: Record<string, unknown>;

  constructor(payload: Partial<IMessagePayload>) {
    Object.assign(
      this,
      deepmerge(
        {
          params: {},
        },
        payload
      )
    );
  }
}

export interface IConnectPayload extends IMessagePayload {
  params: {
    interactiveId: string;
  };
}

export class ConnectPayload extends MessagePayload implements IConnectPayload {
  params!: {
    interactiveId: string;
  } & typeof MessagePayload.prototype.params;

  constructor(payload: Partial<IConnectPayload>) {
    super(payload);
    Object.assign(
      this,
      deepmerge(
        {
          params: {
            interactiveId: undefined,
          },
        },
        payload
      )
    );
  }
}

export interface IConnectionPayload extends IMessagePayload {
  params: {
    player: Player;
    interactiveId: string;
    event: string;
    linkdead: boolean;
    time: number;
  };
}

export class ConnectionPayload
  extends MessagePayload
  implements IConnectionPayload {
  params!: {
    player: Player;
    interactiveId: string;
    event: string;
    linkdead: boolean;
    time: number;
  } & typeof MessagePayload.prototype.params;

  constructor(payload: Partial<IConnectionPayload>) {
    super(payload);
    Object.assign(
      this,
      deepmerge(
        {
          params: {
            player: undefined,
            interactiveId: undefined,
            event: undefined,
            linkdead: false,
            time: undefined,
          },
        },
        payload
      )
    );
  }
}

export interface IViewersPayload extends IMessagePayload {
  params: {
    viewers: number;
    previous: number;
  };
}

export class ViewersPayload extends MessagePayload implements IViewersPayload {
  params!: {
    viewers: number;
    previous: number;
  } & typeof MessagePayload.prototype.params;

  constructor(payload: Partial<IViewersPayload>) {
    super(payload);
    Object.assign(
      this,
      deepmerge(
        {
          params: {
            viewers: -1,
            previous: -1,
          },
        },
        payload
      )
    );
  }
}

export interface IStreamStatusPayload extends IMessagePayload {
  params: {
    status: StreamStatus;
    duration?: number;
    time: number;
  };
}

export class StreamStatusPayload
  extends MessagePayload
  implements IStreamStatusPayload {
  params!: {
    status: StreamStatus;
    duration?: number;
    time: number;
  } & typeof MessagePayload.prototype.params;

  constructor(payload: Partial<IViewersPayload>) {
    super(payload);
    Object.assign(
      this,
      deepmerge(
        {
          params: {
            status: StreamStatus.Standby,
            duration: undefined,
            time: moment().valueOf(),
          },
        },
        payload
      )
    );
  }
}

export interface CommandPayload {
  command: string;
}

export const sendCommand = (payload: CommandPayload): void => {
  const action = {
    type: "backend/doCommand",
    payload,
  };
  backend.send(action);
};
