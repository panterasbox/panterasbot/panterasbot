import React from "react";
import { useRecoilValue } from "recoil";
import styled from "styled-components";
import { promptState } from "./recoil";

const PromptContainer = styled.span`
  padding-left: 3px;
  padding-right: 5px;
`;

const CommandPrompt = (props: any) => {
  const prompt = useRecoilValue(promptState);

  return <PromptContainer>{prompt}</PromptContainer>;
};

export default CommandPrompt;
