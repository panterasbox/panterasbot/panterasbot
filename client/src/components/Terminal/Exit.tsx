import React from "react";
import styled from "styled-components";
import { ExitParam } from "../../lib/render";

const ListItem = styled.li`
  display: inline;
  & ~ &::before {
    content: ", ";
  }
`;

const Exit = (
  props: React.PropsWithChildren<{
    exit: ExitParam | undefined;
  }>
) => {
  return <ListItem>{props.children}</ListItem>;
};

export default Exit;
