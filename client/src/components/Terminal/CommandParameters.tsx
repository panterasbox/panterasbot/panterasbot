import { Grid } from "@mui/material";
import React from "react";
import styled from "styled-components";

const ParamContainer = styled(Grid)``;

const CommandParameters = (props: any) => {
  return (
    <ParamContainer container>
      <Grid item xs={6}>
        field1
      </Grid>
      <Grid item xs={6}>
        val1
      </Grid>
      <Grid item xs={6}>
        field2
      </Grid>
      <Grid item xs={6}>
        val2
      </Grid>
    </ParamContainer>
  );
};

export default CommandParameters;
