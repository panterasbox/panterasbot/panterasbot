import React, { useRef, useState } from "react";
import TextField from "@mui/material/TextField";
import styled from "styled-components";
import CommandPrompt from "./CommandPrompt";
import { Box, Grid, IconButton } from "@mui/material";
import KeyboardReturnIcon from "@mui/icons-material/KeyboardReturn";
import { sendCommand } from "../Backend";
import { monospaceMixin } from "../../styles/appLayout";

const CommandLineBox = styled(Box)`
  width: 100%;
  display: inline-flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: stretch;
  align-content: center;
`;

const MultilineGrid = styled(Grid)`
  width: 100%;
`;

const CommandPreview = styled(Box)`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  font-style: italic;
`;

const CommandLine = styled(TextField)`
  & .MuiOutlinedInput-root {
    border-radius: 0;
    padding: 1px 0px;
  }

  & .MuiInputBase-inputMultiline {
    ${(props) => monospaceMixin()}
    background: white;
    padding: 0px 5px;
  }
`;

const CommandMultiline = styled(CommandLine)`
  width: 100%;
`;

const CommandButton = styled(IconButton)`
  padding: 0px 3px;
  background: ${(props) => props.theme.palette.primary["500"]};
  color: white;
  border-radius: 0;
  & :hover {
    background: white;
    color: ${(props) => props.theme.palette.primary["500"]};
  }
`;

function fitTextarea(textarea: HTMLTextAreaElement, maxHeight: number) {
  let adjustedHeight = textarea.clientHeight;
  if (!maxHeight || maxHeight > adjustedHeight) {
    adjustedHeight = Math.max(textarea.scrollHeight, adjustedHeight);
    if (maxHeight) {
      adjustedHeight = Math.min(maxHeight, adjustedHeight);
    }
    if (adjustedHeight > textarea.clientHeight) {
      textarea.style.height = adjustedHeight + "px";
    }
  }
}

const CommandBar = () => {
  const [command, setCommand] = useState("");
  const [multiline, setMultiline] = useState(false);
  const commandLineRef = useRef<HTMLTextAreaElement>();

  const doCommand = () => {
    sendCommand({ command });
    setCommand("");
    setMultiline(false);
    if (commandLineRef.current) {
      commandLineRef.current.value = "";
    }
  };

  const onCommandLineKeyUp: React.KeyboardEventHandler<HTMLDivElement> = (
    event
  ) => {
    const textarea = event.target as HTMLTextAreaElement;
    if (event.key == "Enter" && !event.shiftKey) {
      doCommand();
      return;
    }
    if (textarea.scrollHeight > textarea.clientHeight) {
      setMultiline(true);
    }
    setCommand(textarea.value);
  };

  const onCommandMultilineKeyUp: React.KeyboardEventHandler<HTMLDivElement> = (
    event
  ) => {
    const textarea = event.target as HTMLTextAreaElement;
    if (event.key == "Enter" && !event.shiftKey) {
      doCommand();
      return;
    }
    setCommand(textarea.value);
    fitTextarea(textarea, 80);
  };

  const advanceCursor: React.FocusEventHandler<HTMLTextAreaElement> = (
    event
  ) => {
    const textarea = event.currentTarget;
    textarea.setSelectionRange(textarea.value.length, textarea.value.length);
  };

  return (
    <>
      <CommandLineBox>
        <CommandPrompt />
        {multiline ? (
          <CommandPreview sx={{ flexGrow: 1 }}>{command}</CommandPreview>
        ) : (
          <CommandLine
            inputRef={commandLineRef}
            sx={{ flexGrow: 1 }}
            autoFocus
            multiline
            rows={1}
            onKeyUp={onCommandLineKeyUp}
            defaultValue={command}
          />
        )}
        <CommandButton onClick={doCommand}>
          <KeyboardReturnIcon fontSize="small" />
        </CommandButton>
      </CommandLineBox>
      {multiline ? (
        <MultilineGrid container>
          <Grid item xs={12}>
            <CommandMultiline
              autoFocus
              multiline
              rows={2}
              onKeyUp={onCommandMultilineKeyUp}
              onFocus={advanceCursor}
              defaultValue={command}
            />
          </Grid>
          {/* <Grid item xs={6}>
              <CommandParameters />
            </Grid> */}
        </MultilineGrid>
      ) : (
        <></>
      )}
    </>
  );
};

export default CommandBar;
