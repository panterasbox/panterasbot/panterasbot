import { atom } from "recoil";

export const promptState = atom<string>({
  key: "prompt",
  default: "",
});
