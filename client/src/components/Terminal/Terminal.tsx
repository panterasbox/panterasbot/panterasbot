import React, { useEffect, useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import { defaultTheme } from "../../styles/themes";
import CommandBar from "./CommandBar";
import styled from "styled-components";
import { messagesState } from "../StreamerDash/recoil";
import { useRecoilValue } from "recoil";
import { RendererFactory } from "../../lib/render";

const TerminalContainer = styled.div`
  font-family: Verdana, Arial, Helvetica, sans-serif;
  background-color: dimGray;
  color: white;
  width: 50%;
  min-height: 50vh;
  max-width: 50%;
  max-height: inherit;
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
`;

const MessagesContainer = styled.div`
  position: relative;
  background: black;
  color: white;
  min-height: 0;
  flex: 1 1 auto;
  display: flex;
`;

interface ResetScrollProps {
  $messageWidth: number;
  $atBottom: boolean;
}

const ResetScroll = styled.div<ResetScrollProps>`
  ${(props) => `display: ${props.$atBottom ? "none" : "block"};`}
  ${(props) => `width: ${props.$messageWidth}px;`}
  position: absolute;
  bottom: 0;
  left: 0;
  text-align: center;
  background-color: rgba(0, 0, 0, 0.7);
  padding: 0.5em 1em;
  cursor: pointer;
`;

interface MessageBufferProps {
  $overflowed: boolean;
}

const MessageBuffer = styled.div<MessageBufferProps>`
  overflow-y: auto;
  overflow-x: visible;
  flex-grow: 1;
  ${(props) => (props.$overflowed ? `` : `margin-top: auto;`)}
`;

const MessageText = styled.div`
  width: 100%;
`;

const CommandBarContainer = styled.div`
  flex: 0 0 auto;
`;

const Terminal = () => {
  const messages = useRecoilValue(messagesState);
  const [messagesContainer, setMessagesContainer] = useState<HTMLDivElement>();
  const [messageBuffer, setMessageBuffer] = useState<HTMLDivElement>();
  const [overflowed, setOverflowed] = useState(false);
  const [messageWidth, setMessageWidth] = useState(0);
  const [atBottom, setAtBottom] = useState(false);

  useEffect(() => {
    if (!messageBuffer || !messagesContainer) {
      setOverflowed(false);
      return;
    }

    new ResizeObserver(() => {
      setOverflowed(
        messageBuffer.offsetHeight >= messagesContainer.offsetHeight
      );
      if (atBottom) {
        messageBuffer.scrollTop = messageBuffer.scrollHeight;
      }
    }).observe(messageBuffer);
    setOverflowed(messageBuffer.offsetHeight >= messagesContainer.offsetHeight);

    messageBuffer.addEventListener("scroll", (e) => {
      setAtBottom(
        messageBuffer.offsetHeight == 0 ||
          messageBuffer.scrollTop ==
            messageBuffer.scrollHeight - messageBuffer.offsetHeight
      );
    });

    setAtBottom(
      messageBuffer.offsetHeight == 0 ||
        messageBuffer.scrollTop ==
          messageBuffer.scrollHeight - messageBuffer.offsetHeight
    );

    setMessageWidth(messageBuffer.lastElementChild?.clientWidth || 0);
  }, [messagesContainer, messageBuffer]);

  useEffect(() => {
    if (messageBuffer) {
      if (atBottom) {
        messageBuffer.scrollTop = messageBuffer.scrollHeight;
      }
    }
  }, [messages]);

  return (
    <Router basename={process.env.PUBLIC_URL}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={defaultTheme}>
          <CssBaseline />
          <TerminalContainer>
            <MessagesContainer
              ref={(r) => setMessagesContainer(r || undefined)}
            >
              <MessageBuffer
                ref={(r) => setMessageBuffer(r || undefined)}
                $overflowed={overflowed}
              >
                {messages.map(([message, meta], index) => {
                  const renderer = RendererFactory.getRenderer(message);
                  return (
                    <MessageText key={`msg${index}`}>
                      {renderer.render(message.message)}
                    </MessageText>
                  );
                })}
              </MessageBuffer>
              <ResetScroll
                $atBottom={atBottom}
                $messageWidth={messageWidth}
                onClick={(e) =>
                  messageBuffer?.scroll({
                    top: messageBuffer.offsetHeight,
                    left: 0,
                    behavior: "auto",
                  })
                }
              >
                More messages below
              </ResetScroll>
            </MessagesContainer>
            <CommandBarContainer>
              <CommandBar />
            </CommandBarContainer>
          </TerminalContainer>
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};

export default Terminal;
