import React from "react";
import { Switch, Route } from "react-router-dom";
import StreamStandby from "../Overlay/StreamStandby";
import StreamerDash from "../StreamerDash/StreamerDash";
import ViewerDash from "../ViewerDash/ViewerDash";
import DefaultDash from "./DefaultDash";
import NotFound from "./NotFound";

const AppRoutes = () => {
  return (
    <Switch>
      <Route exact path="/dash" render={() => <DefaultDash />} />
      <Route exact path="/dash/streamer" render={() => <StreamerDash />} />
      <Route
        exact
        path="/dash/viewer"
        render={(routerProps) => <ViewerDash />}
      />
      <Route exact path="/overlay/standby" render={() => <StreamStandby />} />
      <Route path="*" render={(routerProps) => <NotFound />} />
    </Switch>
  );
};

export default AppRoutes;
