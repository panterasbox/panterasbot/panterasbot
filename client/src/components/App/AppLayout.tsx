import React from "react";
import AppBar from "@mui/material/AppBar";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import styled from "styled-components";
import AppToolbar from "./AppToolbar";
import AppDrawer from "./AppDrawer";
import { defaultTheme } from "../../styles/themes";
import { toolbarMixin } from "../../styles/appLayout";
import { getFullName } from "../../lib/player";
import { Container } from "@mui/material";
import AppRoutes from "./AppRoutes";
import { Player } from "./recoil";

type AppLayoutProps = {
  toggleAppDrawer: () => void;
  appDrawerOpen: boolean;
  appPlayer?: Player | null;
};

const App = styled.div`
  display: flex;
  width: 100%;
  min-height: 100vh;
  max-height: 100vh;
  overflow: hidden;
`;

interface StyledAppBarProps {
  $appDrawerOpen: boolean;
}

const StyledAppBar = styled(AppBar)<StyledAppBarProps>`
  ${(props) =>
    props.$appDrawerOpen
      ? `width: calc(100% - ${props.theme.drawerWidth}px);`
      : ``}
  ${(props) =>
    props.$appDrawerOpen ? `margin-left: ${props.theme.drawerWidth}px;` : ``}
  transition: ${(props) =>
    props.theme.transitions.create(["margin", "width"], {
      easing: props.$appDrawerOpen
        ? props.theme.transitions.easing.easeOut
        : props.theme.transitions.easing.sharp,
      duration: props.$appDrawerOpen
        ? props.theme.transitions.duration.enteringScreen
        : props.theme.transitions.duration.leavingScreen,
    })};
  z-index: ${(props) => props.theme.zIndex.drawer + 1};
`;

interface ContentProps {
  $appDrawerOpen: boolean;
}

const Content = styled.div<ContentProps>`
  min-height: 100%;
  flex-grow: 1;
  padding: ${(props) => props.theme.spacing(3)}px;
  transition: ${(props) =>
    props.theme.transitions.create("margin", {
      easing: props.$appDrawerOpen
        ? props.theme.transitions.easing.easeOut
        : props.theme.transitions.easing.sharp,
      duration: props.$appDrawerOpen
        ? props.theme.transitions.duration.enteringScreen
        : props.theme.transitions.duration.leavingScreen,
    })};
  margin-left: ${(props) =>
    props.$appDrawerOpen ? 0 : -props.theme.drawerWidth}px;
  ${(props) => {
    const toolbar = props.theme.mixins.toolbar;
    return `
      --toolbarHeight: ${toolbar.minHeight}px;
      @media (min-width:0px) and (orientation: landscape) {
        --toolbarHeight: ${toolbar["@media (min-width:0px) and (orientation: landscape)"].minHeight}px;
      }
      @media (min-width:600px) {
        --toolbarHeight: ${toolbar["@media (min-width:600px)"].minHeight}px;
      }
    `;
  }}
`;

const DrawerHeader = styled.div`
  display: flex;
  align-items: center;
  padding: 0 8px;
  justify-content: flex-end;
  ${(props) => toolbarMixin(props.theme.mixins.toolbar)}
`;

const AppLayout = (props: AppLayoutProps) => {
  const { toggleAppDrawer, appDrawerOpen } = props;
  return (
    <App>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={defaultTheme}>
          <CssBaseline />
          <StyledAppBar
            position="fixed"
            title={"test"}
            $appDrawerOpen={appDrawerOpen}
          >
            <AppToolbar
              appDrawerOpen={appDrawerOpen}
              toggleAppDrawer={toggleAppDrawer}
              userAvatar={props.appPlayer?.avatar}
            />
          </StyledAppBar>
          <AppDrawer
            open={appDrawerOpen}
            toggle={toggleAppDrawer}
            userFullName={getFullName(props.appPlayer)}
            userAvatar={props.appPlayer?.avatar}
            userIsGuest={!props.appPlayer?.user || props.appPlayer.user.isGuest}
          />
          <Content $appDrawerOpen={appDrawerOpen}>
            <DrawerHeader />
            <Container
              style={{ minHeight: "100%" }}
              component="main"
              disableGutters={true}
              maxWidth={false}
            >
              <AppRoutes />
            </Container>
          </Content>
        </ThemeProvider>
      </StyledEngineProvider>
    </App>
  );
};

export default AppLayout;
