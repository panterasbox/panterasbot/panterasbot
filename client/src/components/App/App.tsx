import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { useRecoilState, useRecoilValue } from "recoil";
import StreamStandby from "../Overlay/StreamStandby";
import AppLayout from "./AppLayout";
import { appDrawerOpenState, appPlayerState } from "./recoil";

const App = () => {
  const [appDrawerOpen, setAppDrawerOpen] = useRecoilState(appDrawerOpenState);
  const toggle = () => setAppDrawerOpen(!appDrawerOpen);
  const appPlayer = useRecoilValue(appPlayerState);

  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Switch>
        <Route exact path="/overlay/standby" render={() => <StreamStandby />} />
        <Route
          path="*"
          render={(routerProps) => (
            <AppLayout
              toggleAppDrawer={toggle}
              appDrawerOpen={appDrawerOpen}
              appPlayer={appPlayer}
            />
          )}
        />
      </Switch>
    </Router>
  );
};

export default App;
