import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { useRecoilValue } from "recoil";
import StreamerDash from "../StreamerDash/StreamerDash";
import ViewerDash from "../ViewerDash/ViewerDash";
import { withIsStreamer } from "./recoil";

const DefaultDash = () => {
  const isStreamer = useRecoilValue(withIsStreamer);

  return (
    <Router basename={process.env.PUBLIC_URL}>
      {isStreamer ? <StreamerDash /> : <ViewerDash />}
    </Router>
  );
};

export default DefaultDash;
