import React from "react";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import MenuIcon from "@mui/icons-material/Menu";
import styled from "styled-components";

type AppToolbarProps = {
  toggleAppDrawer: () => void;
  appDrawerOpen: boolean;
  userAvatar?: string;
};

const Grow = styled.div`
  flex-grow: 1;
`;

const Title = styled(Typography)`
  display: none;
  ${(props) => props.theme.breakpoints.up("sm")} {
    display: block;
  }
`;

interface DrawerButtonProps {
  $appDrawerOpen: boolean;
  $userAvatar?: string;
}

const DrawerButton = styled(IconButton)<DrawerButtonProps>`
  margin-left: 12px;
  margin-right: 20px;
  ${(props) => (props.$appDrawerOpen ? `display: none;` : ``)}
  ${(props) => (props.$userAvatar ? `padding: 8px;` : ``)}
`;

const Avatar = styled.img`
  width: 1.5em;
  height: 1.5em;
  border-radius: 50%;
`;

const AppToolbar = (props: AppToolbarProps) => {
  const { appDrawerOpen, toggleAppDrawer, userAvatar } = props;
  return (
    <Toolbar disableGutters={appDrawerOpen}>
      <DrawerButton
        onClick={(event) => toggleAppDrawer()}
        color="inherit"
        aria-label="Open drawer"
        $appDrawerOpen={appDrawerOpen}
        $userAvatar={userAvatar}
      >
        {props.userAvatar ? <Avatar src={userAvatar} /> : <MenuIcon />}
      </DrawerButton>
      <Title variant="h6" color="inherit" noWrap>
        Pantera's Bot
      </Title>
      <Grow />
    </Toolbar>
  );
};

export default AppToolbar;
