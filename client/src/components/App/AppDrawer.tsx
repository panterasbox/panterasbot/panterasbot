import React from "react";
import Drawer from "@mui/material/Drawer";
import { Grid, Typography } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import PersonIcon from "@mui/icons-material/Person";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeftTwoTone";
import ChevronRightIcon from "@mui/icons-material/ChevronRightTwoTone";
import { useTheme } from "@mui/material/styles";
import styled from "styled-components";
import { toolbarMixin } from "../../styles/appLayout";
import { serverUrl } from "../../lib/util";
import { CommandPayload, sendCommand } from "../Backend";

const TWITCH_AUTH_URL = serverUrl() + "/auth/twitch";

type AppDrawerProps = {
  toggle: () => void;
  open: boolean;
  userFullName?: string;
  userAvatar?: string;
  userIsGuest: boolean;
};

const StyledDrawer = styled(Drawer)`
  width: ${(props) => props.theme.drawerWidth}px;
  flex-shrink: 0;
  & .paper {
    width: ${(props) => props.theme.drawerWidth}px;
  }
`;

const DrawerHeader = styled.div`
  display: flex;
  align-items: center;
  padding: 0 8px;
  justify-content: flex-end;
  ${(props) => toolbarMixin(props.theme.mixins.toolbar)}
`;

const CenteredCell = styled(Grid)`
  margin-left: auto;
  margin-right: auto;
`;

const ToggleButton = styled(IconButton)`
  padding: 6px;
`;

const ToggleCell = styled(CenteredCell)`
  margin-top: auto;
  margin-bottom: auto;
`;

const AvatarWrapper = styled.div`
  width: 48px;
  height: 48px;
`;

const Avatar = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 12px;
`;

const GuestAvatar = styled(PersonIcon)`
  width: 100%;
  height: 100%;
  border-radius: 12px;
  background-color: black;
`;

const FullName = styled(Typography)`
  font-size: 0.75rem;
  line-height: 1.2;
`;

const SigninButton = styled.button`
  font-size: 0.5rem;
  font-weight: bold;
`;

const AppDrawer = (props: AppDrawerProps) => {
  const { open, toggle } = props;
  const theme = useTheme();

  const login = () => (window.location.href = TWITCH_AUTH_URL);
  const logout: CommandPayload = { command: "logout" };

  return (
    <StyledDrawer
      open={open}
      anchor="left"
      variant="persistent"
      classes={{
        paper: "paper",
      }}
    >
      <DrawerHeader>
        <Grid container spacing={0} alignItems="flex-end">
          <CenteredCell item xs={3}>
            <AvatarWrapper>
              {props.userAvatar ? (
                <Avatar src={props.userAvatar} />
              ) : (
                <GuestAvatar />
              )}
            </AvatarWrapper>
          </CenteredCell>
          <CenteredCell item xs={6}>
            <FullName variant="h6" color="inherit">
              {props.userFullName || "Anonymous"}
            </FullName>
            {props.userIsGuest ? (
              <SigninButton onClick={(event) => login()}>Sign in</SigninButton>
            ) : (
              <SigninButton onClick={(event) => sendCommand(logout)}>
                Sign out
              </SigninButton>
            )}
          </CenteredCell>
          <ToggleCell item xs={2}>
            <ToggleButton onClick={(event) => toggle()}>
              {theme.direction === "ltr" ? (
                <ChevronLeftIcon />
              ) : (
                <ChevronRightIcon />
              )}
            </ToggleButton>
          </ToggleCell>
        </Grid>
      </DrawerHeader>
    </StyledDrawer>
  );
};

export default AppDrawer;
