import { atom, selector } from "recoil";
import { playerQuery } from "../../lib/player";
import client from "../Backend/ApolloClient";

export interface User {
  id: string;
  _id: string;
  avatar?: string;
  isGuest: boolean;
  isStreamer: boolean;
}

export interface Player {
  id: string;
  _id: string;
  firstName: string;
  lastName?: string;
  avatar?: string;
  user: User;
}

export const appDrawerOpenState = atom<boolean>({
  key: "appDrawerOpen",
  default: false,
});

export const appInteractiveState = atom<string | undefined>({
  key: "appInteractive",
  default: undefined,
});

export const appPlayerState = atom<Player | null>({
  key: "appPlayer",
  default: selector<Player | null>({
    key: "appPlayer/Default",
    get: async (): Promise<Player | null> => {
      const result = await client.query<{ player: Player | null }>({
        fetchPolicy: "network-only",
        query: playerQuery,
        variables: {},
        errorPolicy: "all",
      });
      return result.data.player;
    },
  }),
});

export const withIsStreamer = selector<boolean>({
  key: "isStreamer",
  get: ({ get }): boolean => {
    const player = get(appPlayerState);
    if (player != null) {
      return player.user.isStreamer;
    }
    return false;
  },
});
