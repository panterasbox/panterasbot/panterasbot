import { atom, selector } from "recoil";
import { ActionMeta } from "../../lib";
import { streamQuery } from "../../lib/twitch";
import { MessagePayload } from "../Backend";
import client from "../Backend/ApolloClient";

export interface TwitchStream {
  id: string;
  _id?: string;
  externalId: string;
  gameId: string;
  language?: string;
  title?: string;
  type?: string;
  thumbnailUrl?: string;
  startedAt: Date;
  userId: string;
  userName: string;
  viewerCount: number;
  tagIds: string[];
  createdAt: Date;
  updatedAt: Date;
}

export const messagesState = atom<[MessagePayload, ActionMeta][]>({
  key: "messages",
  default: [],
});

export const viewersState = atom<number>({
  key: "viewers",
  default: selector<number>({
    key: "viewers/Default",
    get: async (): Promise<number> => {
      try {
        const result = await client.query<{ twitchStream: TwitchStream }>({
          fetchPolicy: "network-only",
          query: streamQuery,
          variables: {},
          errorPolicy: "all",
        });
        return result.data.twitchStream.viewerCount;
      } catch (e) {
        console.warn("Caught error initializing player", e);
      }
      return -1;
    },
  }),
});
