import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import { defaultTheme } from "../../styles/themes";
import { CommandPayload, sendCommand } from "../Backend";
import { useRecoilValue } from "recoil";
import { withIsStreamer } from "../App/recoil";
import { viewersState, messagesState } from "./recoil";

const StreamerDash = () => {
  const viewers = useRecoilValue(viewersState);
  const messages = useRecoilValue(messagesState);
  const isStreamer = useRecoilValue(withIsStreamer);

  const ping: CommandPayload = { command: "ping" };

  const msgs = messages.length
    ? messages
        .map(([msg, meta]) => msg.message)
        .reduce((prev: string, cur: string): string => {
          return prev + "\n" + cur;
        })
    : "";
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={defaultTheme}>
          <CssBaseline />
          <div>
            <h1>Beep boop. Pantera's Bot standing by.</h1>
            <h3>Viewer Count: {viewers}</h3>
            <h3>isStreamer: {isStreamer ? "true" : "false"}</h3>
            <button onClick={() => sendCommand(ping)}>Ping</button>
            <br />
            <textarea readOnly={true} value={msgs}></textarea>
          </div>

          <iframe
            src={
              "https://www.twitch.tv/embed/bobalu_smallberries/chat?parent=bot.panterasbox.com&parent=localhost&font-size=biggest&height=100%25&parent=panterasbox.com&parent=www.panterasbox.com&parent=localhost&darkpopout&width=100%25"
            }
            scrolling="no"
            allow="autoplay; fullscreen"
            title="Twitch"
            sandbox="allow-modals allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox"
            width="476px"
            height="720px"
          ></iframe>
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};

export default StreamerDash;
