import moment from "moment";
import { atom } from "recoil";

export enum StreamStatus {
  Starting = "starting",
  Standby = "standby",
  Live = "live",
  Ending = "ending",
}

export interface StreamStandby {
  status: StreamStatus;
  duration?: number;
  time: number;
}

export const streamStandby = atom<StreamStandby>({
  key: "streamStandby",
  default: {
    status: StreamStatus.Standby,
    time: moment().valueOf(),
  },
});
