import moment from "moment";
import React, { useEffect, useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { useRecoilValue } from "recoil";
import styled from "styled-components";
import { formatDuration, remainingDuration } from "../../lib/util";
import { streamStandby, StreamStatus } from "./recoil";

const StandbyStatus = styled.h1`
  @import url("https://fonts.googleapis.com/css2?family=Cinzel&display=swap");
  width: 100%;
  text-align: center;
  font-size: 6rem;
  background-color: transparent;
  color: white;
  font-family: "Cinzel", sans-serif;
  margin: 0px;
`;

const StandbyDuration = styled.span`
  @import url("https://fonts.googleapis.com/css2?family=Noto+Sans+Mono:wght@500&display=swap");
  font-family: "Noto Sans Mono", monospace;
`;

const StreamStandby = () => {
  const standby = useRecoilValue(streamStandby);
  const [seconds, setSeconds] = useState(0);

  const remaining = remainingDuration(standby.duration || 0, standby.time);

  useEffect(() => {
    setSeconds((s) => remaining);
    let count = remaining;
    const interval = setInterval(() => {
      setSeconds((s) => s - 1);
      if (count-- <= 1) {
        clearInterval(interval);
      }
    }, 1000);
    return () => clearInterval(interval);
  }, [standby]);

  let message = "Stream will resume momentarily";

  const duration = formatDuration(moment.duration(seconds, "seconds"));
  switch (standby.status) {
    case StreamStatus.Starting:
      message = `Stream will begin ${seconds > 0 ? `in ` : "momentarily"}`;
      break;
    case StreamStatus.Ending:
      message = `Stream is ending${seconds > 0 ? ` in ` : ""}`;
      break;
    default:
      message = `Stream will resume ${seconds > 0 ? `in ` : "momentarily"}`;
      break;
  }

  return (
    <Router basename={process.env.PUBLIC_URL}>
      {standby.status !== StreamStatus.Live ? (
        <StandbyStatus>
          {message}
          {seconds > 0 ? <StandbyDuration>{duration}</StandbyDuration> : ""}.
        </StandbyStatus>
      ) : (
        <></>
      )}
    </Router>
  );
};

export default StreamStandby;
