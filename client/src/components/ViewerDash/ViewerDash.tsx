import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import { defaultTheme } from "../../styles/themes";
import styled from "styled-components";
import ActionPane from "../ActionPane/ActionPane";
import TwitchStream from "./TwitchStream";
import { Box } from "@mui/material";
import { appPlayerState, Player } from "../App/recoil";
import Terminal from "../Terminal/Terminal";
import { useRecoilValue } from "recoil";

const ViewerBox = styled(Box)`
  width: 100%;
  min-height: calc(100vh - var(--toolbarHeight));
  max-height: calc(100vh - var(--toolbarHeight));
  display: flex;
  flex-flow: column wrap;
  align-content: stretch;
  overflow: hidden;
`;

const ViewerDash = () => {
  const appPlayer = useRecoilValue(appPlayerState);

  return (
    <Router basename={process.env.PUBLIC_URL}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={defaultTheme}>
          <CssBaseline />
          <ViewerBox>
            <Terminal />
            <TwitchStream />
            <ActionPane />
          </ViewerBox>
        </ThemeProvider>
      </StyledEngineProvider>
    </Router>
  );
};

export default ViewerDash;
