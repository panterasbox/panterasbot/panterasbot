import React, {
  DetailedReactHTMLElement,
  ReactHTMLElement,
  useEffect,
  useState,
} from "react";
import { useRecoilValue } from "recoil";
import styled from "styled-components";
import { streamerState } from "./recoil";

const EMBED_URL = "https://embed.twitch.tv/embed/v1.js";
const TARGET_ID = "twitchEmbed";

const script = document.createElement("script");
script.setAttribute("src", EMBED_URL);
document.body.appendChild(script);

let embed: any = undefined;

const StreamBox = styled.div`
  width: 50%;
  aspect-ratio: 16 / 9;
  display: flex;
  align-items: stretch;
  flex-direction: row;

  .${TARGET_ID} {
    flex: 1;
  }
`;

const TwitchStream = () => {
  const streamer = useRecoilValue(streamerState);
  const [targetDiv, setTargetDiv] = useState<ReactHTMLElement<HTMLElement>>();

  useEffect(() => {
    const newDiv = React.createElement("div", {
      id: TARGET_ID,
      className: TARGET_ID,
    });
    setTargetDiv(newDiv);
  }, [streamer]);

  useEffect(() => {
    if (targetDiv) {
      const options = {
        allowFullscreen: true,
        autoplay: true,
        channel: streamer,
        width: "100%",
        height: "100%",
        layout: "video",
        muted: false,
        parent: ["localhost", "bot.panterasbox.com"],
        theme: "dark",
      };
      // @ts-expect-error Twitch loaded at runtime
      embed = new Twitch.Embed(TARGET_ID, { ...options });
    }
  }, [targetDiv]);

  return <StreamBox>{targetDiv}</StreamBox>;
};

export default TwitchStream;
