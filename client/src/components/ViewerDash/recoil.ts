import { atom } from "recoil";

export const streamerState = atom<string | undefined>({
  key: "streamer",
  default: "bobalu_smallberries",
});
