import { buildSchema as build } from "type-graphql";
import { Application } from "../backend/Application";
import { Interactive } from "../mud/lib/connection/Interactive";
import { PlayerResolver } from "./player";
import { UserResolver } from "./user";
import { TwitchStreamResolver } from "./twitch";

export interface Context {
  sessionId: any;
  sessionInteractive: Interactive | undefined;
  application: Application;
}

export const buildSchema = async () => {
  const schema = await build({
    resolvers: [PlayerResolver, UserResolver, TwitchStreamResolver],
    orphanedTypes: [],
  });
  return schema;
};
