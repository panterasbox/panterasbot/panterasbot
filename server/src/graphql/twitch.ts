import { Ctx, Query, Resolver } from "type-graphql";
import { Context } from ".";
import { TwitchStream } from "../mud/lib/twitch/TwitchStream";

@Resolver((of) => TwitchStream)
export class TwitchStreamResolver {
  constructor() {}

  @Query((returns) => TwitchStream, { nullable: true })
  twitchStream(@Ctx() context: Context): TwitchStream | null {
    return context.application.twitchController.stream || null;
  }
}
