import { Args, ArgsType, Ctx, Field, ID, Query, Resolver } from "type-graphql";
import { Context } from ".";
import { Interactive } from "../mud/lib/connection/Interactive";
import { User } from "../mud/lib/connection/User";

@ArgsType()
export class ByIdArgs {
  @Field((type) => ID, { nullable: true })
  id?: string;
}

@Resolver((of) => User)
export class UserResolver {
  constructor() {}

  @Query((returns) => User, { nullable: true })
  async user(
    @Args() { id }: ByIdArgs,
    @Ctx() context: Context
  ): Promise<User | null> {
    const api = context.application.api;
    const interactive: Interactive | undefined = context.sessionInteractive;

    if (interactive && !id) {
      id = interactive.avatar?.player.user.id;
    }

    if (!id) {
      throw new Error("No user found is session for user query");
    }

    const user = await api.user.getUser(id);
    if (!user) {
      return null;
    }

    if (
      !interactive ||
      !interactive.avatar ||
      interactive.avatar.player.user._id != user._id
    ) {
      user.email = undefined;
    }

    return user;
  }
}
