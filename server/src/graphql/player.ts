import {
  Args,
  ArgsType,
  Ctx,
  Field,
  ID,
  Query,
  registerEnumType,
  Resolver,
} from "type-graphql";
import { Context } from ".";
import { Pronouns } from "../mud/lib/character/Gendered";
import { Interactive } from "../mud/lib/connection/Interactive";
import { Player } from "../mud/lib/connection/Player";

registerEnumType(Pronouns, { name: "Pronouns" });

@ArgsType()
export class ByIdArgs {
  @Field((type) => ID, { nullable: true })
  id?: string;
}

@Resolver((of) => Player)
export class PlayerResolver {
  constructor() {}

  @Query((returns) => Player, { nullable: true })
  async player(
    @Args() { id }: ByIdArgs,
    @Ctx() context: Context
  ): Promise<Player | null> {
    const api = context.application.api;
    const interactive: Interactive | undefined = context.sessionInteractive;

    if (interactive && !id) {
      id = interactive.avatar?.player.id;
    }

    if (!id) {
      throw new Error("No player found is session for player query");
    }

    const player = (await api.stuff.findStuff(id)) as Player | null;
    if (!player) {
      return null;
    }

    if (
      !interactive ||
      !interactive.avatar ||
      interactive.avatar.player.user._id != player.user._id
    ) {
      player.user.email = undefined;
    }

    return player;
  }
}
