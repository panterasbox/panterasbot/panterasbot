import { Args, ArgsType, Ctx, Field, ID, Query, Resolver } from "type-graphql";
import { Context } from ".";
import { Stuff } from "../mud/lib/stuff/Stuff";

@ArgsType()
export class ByIdArgs {
  @Field((type) => ID, { nullable: false })
  id!: string;
}

@Resolver((of) => Stuff)
export class StuffResolver {
  constructor() {}

  @Query((returns) => Stuff, { nullable: true })
  stuff(@Args() { id }: ByIdArgs, @Ctx() context: Context): Stuff | null {
    const api = context.application.api;
    return api.stuff.findStuff(id);
  }
}
