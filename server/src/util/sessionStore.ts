import * as session from "express-session";
import * as redis from "redis";
import { promisify } from "util";

// const REDIS_URL = "redis://cache.panterasbox.net:6379";

declare module "express-session" {
  interface SessionData {
    sid: string;
    twitchId?: string;
    gitlabId?: number;
    passport?: { user: any };
    twitchReferer?: string;
    gitlabReferer?: string;
  }
}
let sessionStore: session.Store;

sessionStore = new session.MemoryStore();
// if (process.env.NODE_ENV == "development") {
// } else {
//   let RedisStore = require("connect-redis")(session);
//   let redisClient = redis.createClient({
//     url: REDIS_URL,
//   });
//   sessionStore = new RedisStore({ client: redisClient });
// }

export const saveSession = async (session: session.SessionData) => {
  if (session) {
    const sid = session.sid;
    if (sid) {
      const setInStore = promisify(sessionStore.set.bind(sessionStore));
      return await setInStore(sid, session);
    }
  }
};

export default sessionStore;
