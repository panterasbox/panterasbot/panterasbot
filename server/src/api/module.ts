import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { EventEmitter } from "events";
import { srcRoot } from "..";
import { NAME_DELIM } from "./object";

const CACHE_EVENT = "ModuleCacheWrite";
const LOAD_EVENT = "ModuleLoad";

interface CacheEvent {
  path: string;
  module: NodeJS.Module | undefined;
  previous: NodeJS.Module | undefined;
}

interface LoadEvent {
  module: NodeJS.Module;
}

const _ModuleApi = ApplicationContextMixin(Object);

export interface ModuleInfo {
  path: string;
  export: string;
}

export class ModuleApi extends _ModuleApi {
  private static infoTable = ModuleApi.buildInfoTable();

  static buildInfoTable(): WeakMap<Function, ModuleInfo> {
    const result = new WeakMap<Function, ModuleInfo>();

    const inspectExports = (mod: NodeJS.Module | undefined) => {
      if (mod && mod.loaded && typeof mod.exports === "object") {
        for (const key of Object.keys(mod.exports)) {
          let xport: Function;
          try {
            if (typeof mod.exports[key] === "function") {
              xport = mod.exports[key];
            } else {
              continue;
            }
          } catch (error) {
            continue;
          }
          result.set(xport, {
            path: mod.path,
            export: key,
          });
        }
      }
    };

    Object.keys(require.cache).forEach((name: string) => {
      if (name.startsWith(srcRoot)) {
        const mod = require.cache[name];
        inspectExports(mod);
      }
    });

    // @ts-ignore
    const eventEmitter = require.eventEmitter as EventEmitter;
    eventEmitter.on(CACHE_EVENT, (event: CacheEvent) => {
      inspectExports(event.module);
    });
    eventEmitter.on(LOAD_EVENT, (event: LoadEvent) => {
      inspectExports(event.module);
    });

    return result;
  }

  find(
    constructor: Function,
    root: NodeJS.Module | undefined = require.main
  ): ModuleInfo | undefined {
    if (!root) {
      throw new Error("Couldn't find root module");
    }

    const info = ModuleApi.infoTable.get(constructor);
    return info;
  }

  resolve(name: string): string {
    if (name.indexOf(NAME_DELIM) < 0) {
      name = `${name}${NAME_DELIM}default`;
    }
    return name;
  }

  async import(name: string): Promise<any> {
    const fileApi = this.application.api.file;
    let [path] = name.split(NAME_DELIM);
    if (!path) {
      throw new Error(`Cannot determine module path: ${name}`);
    }
    try {
      const module = await import(fileApi.externalPath(path));
      return module;
    } catch (err) {
      throw new Error(`Dynamic import failed for ${path}: ${err}`);
    }
  }
}
