import callsites from "callsites";
import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { CommandController } from "../mud/lib/command/CommandController";
import {
  Command,
  Field,
  Validation,
} from "../mud/lib/command/CommandDefinition";
import { CommandGiver } from "../mud/lib/command/CommandGiver";
import { Agent } from "../mud/lib/stuff/Agent";
import { Stuff } from "../mud/lib/stuff/Stuff";
import {
  ValidationContext,
  ValidationFunction,
  ValidationModel,
} from "../mud/lib/validation";
import { ParseError, ParserContext } from "./command-line";

export const VALIDATION_PATH = "/lib/validation";

export interface CommandPayload {
  command: string;
  params: {};
  noEcho: boolean | undefined;
}

export type ModelValue = String | Number | Object | Object[] | Boolean;
export type MultiModelValue<T extends ModelValue> = T[];
export type AnyModelValue =
  | ModelValue
  | MultiModelValue<ModelValue>
  | undefined;
export type CommandModel = Record<string, AnyModelValue>;

export interface CommandContext<
  I extends CommandModel = {},
  O extends CommandModel = {}
> {
  commandGiver: CommandGiver & Agent;
  verb?: string;
  arg?: string;
  command?: Command;
  commandDef?: string;
  subcommand?: string;
  parser?: ParserContext;
  validation?: ValidationContext;
  in?: I;
  out?: O;
  terminate: boolean;
}

export interface ExecutionContext {
  controllers: CommandController<any, any>[];
}

const _CommandApi = ApplicationContextMixin(Object);

export class CommandApi extends _CommandApi {
  getExecutionContext(): ExecutionContext {
    const controllers = new Array<CommandController<any, any>>();
    const stack = callsites().splice(1);
    for (let callsite of stack) {
      const ob = callsite.getThis();
      if (!ob) {
        continue;
      }

      if (ob instanceof CommandController) {
        if (callsite.getFunctionName() === "doCommand") {
          controllers.push(ob as CommandController<any, any>);
        }
      }
    }
    if (!controllers.length) {
      throw new Error("Unable to determine Execution context");
    }

    return {
      controllers,
    };
  }

  resolveFields(
    command: Command,
    context: ParserContext
  ): Record<string, Field> {
    const result = {
      ...command.fields,
      ...(context.subcommandIndex !== undefined
        ? (command.subcommands || {})[context.subcommandIndex].fields
        : {}),
    };
    for (const id of Object.keys(result)) {
      result[id].id = id;
    }
    return result;
  }

  resolveValidations(context: CommandContext): Validation[] {
    return [
      context.command?.validation || [],
      context.parser?.subcommandIndex !== undefined
        ? (context.command?.subcommands || {})[context.parser.subcommandIndex]
            .validation
        : [],
    ].flat(1);
  }

  resolveModel<T extends CommandModel>(context: CommandContext): T {
    if (!context.command) {
      throw new Error("No command defined");
    }
    if (!context.parser) {
      throw new Error("No parser context");
    }
    const fields = this.resolveFields(context.command, context.parser);
    const model: Partial<T> = {};

    for (const fieldRef of Object.keys(fields)) {
      const field = fields[fieldRef];

      if (
        field.required &&
        !(fieldRef in (context.parser?.model || {})) &&
        field.default === undefined
      ) {
        throw new ParseError();
      }

      const value = context.parser?.model[fieldRef] || field.default;
      const resolved = this.resolveModelValue(value, field);
      if (resolved !== undefined) {
        model[fieldRef as keyof T] = resolved as T[keyof T];
      }
    }

    return model as T;
  }

  resolveModelValue(
    value: string | string[] | undefined,
    field: Field
  ): AnyModelValue | undefined {
    if (value === undefined) {
      return value;
    }
    const resolve = (value: string): ModelValue => {
      switch (field.type) {
        case "object":
          const obj = this.application.api.mql.queryOne(value);
          if (obj == null) {
            throw new ParseError();
          }
          return obj;
        case "objects":
          return this.application.api.mql.query(value);
        case "boolean":
          try {
            return new Boolean(value);
          } catch (err) {
            throw new ParseError();
          }
        case "number":
          try {
            return new Number(value);
          } catch (err) {
            throw new ParseError();
          }
        case "string":
        default:
          return value;
      }
    };

    if (Array.isArray(value)) {
      return value.map(resolve);
    } else {
      return resolve(value);
    }
  }

  async resolveValidation<M extends CommandModel, T>(
    validation: Validation
  ): Promise<ValidationFunction<M, ModelValue, T>> {
    const objectApi = this.application.api.object;

    let [lib, method] = validation.method.split(".", 2);
    if (!method) {
      method = lib;
      lib = "";
    }

    if (lib.length) {
      lib = `${VALIDATION_PATH}/${lib}`;
      const name = objectApi.moduleExport(lib, method);
      const result = await objectApi.load(name);
      if (!result) {
        throw new Error(`Validation function could not be loaded: ${name}`);
      }
      if (validation.field.length) {
        return result as (v: ModelValue, params: T) => boolean;
      } else {
        return result as (v: CommandModel, params: T) => boolean;
      }
    } else {
      const result = this[method];
      if (typeof result !== "function") {
        throw new Error(`Validation function could not be loaded: ${method}`);
      }
      if (validation.field.length) {
        return (v: ModelValue, params: T): boolean => {
          return result(this, v, params);
        };
      } else {
        return (v: ModelValue, params: T): boolean => {
          return result(this, v, params);
        };
      }
    }
  }

  collapseModel<T extends CommandModel>(
    model: ValidationModel,
    context: CommandContext
  ): T {
    const result: Partial<T> = {};

    if (!context.command) {
      throw new Error("No command defined");
    }
    if (!context.parser) {
      throw new Error("No parser context");
    }
    const fields = this.resolveFields(context.command, context.parser);
    for (const fieldRef of Object.keys(fields)) {
      const field = fields[fieldRef];

      if (context?.parser?.multi[fieldRef]) {
        result[fieldRef as keyof T] = model[fieldRef] as T[keyof T];
      } else if (Array.isArray(model[fieldRef])) {
        result[fieldRef as keyof T] = model[fieldRef][0] as T[keyof T];
      } else {
        result[fieldRef as keyof T] = model[fieldRef] as T[keyof T];
      }
    }

    return result as T;
  }

  resolveParams<T, M extends CommandModel>(params: any, model: M): T {
    const _resolve = (p: any): any => {
      if (Array.isArray(p)) {
        return p.map((v) => _resolve(v));
      } else if (typeof p === "object") {
        return Object.fromEntries(
          Object.entries(p).map(([k, v]) => [k, _resolve(v)])
        );
      } else if (typeof p === "string") {
        if (p.startsWith("$")) {
          const field = p.substr(1);
          if (field in model) {
            return model[field];
          }
        }
      }
      return p;
    };

    return _resolve(params);
  }

  async command(
    commandGiver: CommandGiver,
    payload: CommandPayload
  ): Promise<CommandContext | undefined> {
    return await commandGiver.doCommand(payload);
  }
}
