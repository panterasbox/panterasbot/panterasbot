import callsites from "callsites";
import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { Stuff } from "../mud/lib/stuff/Stuff";
import { Constructor } from "../util/mixin";

const _CallstackApi = ApplicationContextMixin(Object);

export interface Callsite {
  columnNumber: number | null;
  evalOrigin: string | undefined;
  fileName: string | null;
  functionName: string | null;
  lineNumber: number | null;
  methodName: string | undefined;
  typeName: string | null;
  isConstructor: boolean;
  isEval: boolean;
}

export class CallstackApi extends _CallstackApi {
  callstack<T>(clazz: Constructor<T>): [T, Callsite[]][] {
    const stack = callsites().splice(1);
    const result: [T, Callsite[]][] = [];
    for (let callsite of stack) {
      const ob = callsite.getThis();
      if (!ob) {
        continue;
      }
      let callsites: Callsite[] = [];
      const prepareCallsite = (callsite: callsites.CallSite): Callsite => {
        return {
          columnNumber: callsite.getColumnNumber(),
          evalOrigin: callsite.getEvalOrigin(),
          fileName: callsite.getFileName(),
          functionName: callsite.getFunctionName(),
          lineNumber: callsite.getLineNumber(),
          methodName: callsite.getMethodName(),
          typeName: callsite.getTypeName(),
          isConstructor: callsite.isConstructor(),
          isEval: callsite.isEval(),
        };
      };
      if (result.length && ob === result[result.length - 1][0]) {
        callsites = result[result.length - 1][1];
        callsites.push(prepareCallsite(callsite));
        continue;
      }

      if (ob instanceof clazz) {
        result.push([ob, [prepareCallsite(callsite)]]);
      }
    }
    return result;
  }

  previousStuff(index: number = 0): Stuff | null {
    const stack = this.stuffStack();
    return stack.length > index ? stack[index] : null;
  }
}
