import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import {
  Command,
  Option,
  OptionRef,
  Subcommand,
  Syntax,
} from "../mud/lib/command/CommandDefinition";

const TAB = "\t".charCodeAt(0);
const SPACE = " ".charCodeAt(0);
const COMMAND_INDEX = "";

export type OptCache<T extends string | number> = Record<
  string,
  Record<number, Record<T, Option>>
>;

// { field: value }
export type CommandOpts = Record<string, string[] | boolean>;

export type ParserModel = Record<string, string | string[]>;

export interface ParserContext {
  arg: string;
  opts: OptCache<number>;
  longopts: OptCache<string>;
  subcommand?: string;
  subcommandIndex?: string;
  commandSyntax?: Syntax;
  commandSyntaxIndex?: number;
  subcommandSyntax?: Syntax;
  subcommandSyntaxIndex?: number;
  commandOpts?: CommandOpts;
  subcommandOpts?: CommandOpts;
  model: ParserModel;
  multi: Record<string, boolean>;
}

export class ParseError extends Error {
  constructor(message?: any) {
    super(message);
  }
}

const _CommandLineApi = ApplicationContextMixin(Object);

export class CommandLineApi extends _CommandLineApi {
  parseCommand(command: Command, verb: string, arg: string): ParserContext {
    // XXX cache this
    const [opts, longopts] = this.normalizeOpts(command);

    let pos = 0;
    let context: ParserContext = {
      arg,
      opts,
      longopts,
      model: {},
      multi: {},
    };
    if (command.syntax && command.syntax.length) {
      for (
        let cmdSyntaxIndex = 0;
        cmdSyntaxIndex < command.syntax.length;
        cmdSyntaxIndex++
      ) {
        try {
          pos = this._parseCommand(pos, context, command, cmdSyntaxIndex);
        } catch (e) {
          if (e instanceof ParseError) {
            continue;
          } else {
            throw e;
          }
        }
        break;
      }
    } else {
      pos = this._parseCommand(pos, context, command, -1);
    }

    // this.resolveDefaults(context, command);
    return context;
  }

  normalizeOpts(command: Command): [OptCache<number>, OptCache<string>] {
    const cmdOpts =
      command.opts?.reduce((prev, curr) => {
        if (curr.id) {
          prev[curr.id] = curr;
        } else {
          // XXX throw exception?
        }
        return prev;
      }, {} as Record<string, Option>) || {};

    const _normalize = (
      options: (Option | OptionRef)[] | undefined,
      cmdOpts: Record<string, Option>
    ) => {
      const opts: Record<number, Option> = {};
      const longopts: Record<string, Option> = {};

      if (!options) {
        return [{}, {}];
      }

      options.forEach((option) => {
        let o: Option | undefined = undefined;
        if (typeof option === "string") {
          o = cmdOpts[option];
        } else {
          o = option as Option;
        }
        if (o.opt) {
          opts[o.opt.charCodeAt(0)] = o;
        }
        if (o.longopt) {
          longopts[o.longopt] = o;
        }
      });
      return [opts, longopts];
    };

    const opts: OptCache<number> = {},
      longopts: OptCache<string> = {};

    const iterateSubcommands = () => {
      for (const subcommandIndex in command.subcommands) {
        const subcommand = command.subcommands[subcommandIndex];
        const subcmdOpts =
          subcommand.opts?.reduce((prev, curr) => {
            if (curr.id) {
              prev[curr.id] = curr;
            } else {
              // XXX throw exception?
            }
            return prev;
          }, {} as Record<string, Option>) || {};

        opts[subcommandIndex] ||= {};
        longopts[subcommandIndex] ||= {};
        const syntax = command.subcommands[subcommandIndex].syntax;
        for (const subcommandSyntaxIndex in syntax) {
          const [subcmdOpt, subcmdLongopt] = _normalize(
            syntax[subcommandSyntaxIndex].opts,
            subcmdOpts
          );
          opts[subcommandIndex][subcommandSyntaxIndex] = subcmdOpt;
          longopts[subcommandIndex][subcommandSyntaxIndex] = subcmdLongopt;
        }
      }
    };

    if (command.syntax && command.syntax.length) {
      opts[COMMAND_INDEX] = {};
      longopts[COMMAND_INDEX] = {};

      for (const commandSyntaxIndex in command.syntax) {
        const [cmdOpt, cmdLongopt] = _normalize(
          command.syntax[commandSyntaxIndex].opts,
          cmdOpts
        );
        opts[COMMAND_INDEX][commandSyntaxIndex] = cmdOpt;
        longopts[COMMAND_INDEX][commandSyntaxIndex] = cmdLongopt;

        iterateSubcommands();
      }
    } else {
      opts[COMMAND_INDEX] = {};
      longopts[COMMAND_INDEX] = {};
      opts[COMMAND_INDEX][-1] = {};
      longopts[COMMAND_INDEX][-1] = {};
      iterateSubcommands();
    }

    return [opts, longopts];
  }

  private _parseCommand(
    pos: number,
    context: ParserContext,
    command: Command,
    cmdSyntaxIndex: number
  ): number {
    if (cmdSyntaxIndex >= 0) {
      const cmdSyntax = command.syntax[cmdSyntaxIndex];
      pos = this.parseOpts(pos, context, COMMAND_INDEX, cmdSyntaxIndex);

      // TODO check for badopts, whatever
      if (false) {
        throw new ParseError();
      }

      context.commandSyntax = cmdSyntax;
    } else {
      context.commandSyntax = undefined;
    }

    if (command.subcommands) {
      pos = this.parseSubcommand(pos, context, command.subcommands);
      if (context.subcommandIndex !== undefined) {
        const subcommand = command.subcommands[context.subcommandIndex];
        for (
          let subcmdSyntaxIndex = 0;
          subcmdSyntaxIndex < subcommand.syntax.length;
          subcmdSyntaxIndex++
        ) {
          pos = this.parseOpts(
            pos,
            context,
            context.subcommandIndex,
            subcmdSyntaxIndex
          );

          // TODO check for badopts, whatever
          if (false) {
            continue;
          }

          context.subcommandSyntaxIndex = subcmdSyntaxIndex;
          context.subcommandSyntax = subcommand.syntax[subcmdSyntaxIndex];
          break;
        }
      } else {
        // fail invalid subcommand
      }
    }

    const syntax = context.subcommandSyntax || context.commandSyntax;
    if (!syntax) {
      // wtf?
      throw new ParseError();
    }
    pos = this.parseArgs(pos, context, syntax);

    // set combined opts?

    return pos;
  }

  parseSubcommand(
    pos: number,
    context: ParserContext,
    subcommands: Record<string, Subcommand>
  ): number {
    pos = this.application.api.string.findNonWhitespace(context.arg, pos);

    let idx = context.arg.indexOf(" ", pos);
    if (idx == -1) {
      idx = context.arg.length;
    }
    const subcommand = context.arg.substring(pos, idx);

    if (subcommand in subcommands) {
      context.subcommandIndex = subcommand;
      return pos + subcommand.length;
    } else {
      return pos;
    }
  }

  parseOpts(
    pos: number,
    context: ParserContext,
    cmdIndex: string,
    cmdSyntaxIndex: number
  ): number {
    let done = false;
    const arg = context.arg;
    while (!done && pos < arg.length) {
      switch (arg.charAt(pos)) {
        case " ":
        case "\t":
          pos++;
          break;
        case "-":
          pos++;
          if (pos >= arg.length) {
            continue;
          }
          switch (arg.charAt(pos)) {
            case "-":
              pos++;
              pos = this.parseLongopt(
                pos,
                context,
                context.longopts[cmdIndex][cmdSyntaxIndex]
              );
              break;
            case " ":
              // ignore " - "
              pos++;
              break;
            default:
              pos = this.parseOpt(
                pos,
                context,
                context.opts[cmdIndex][cmdSyntaxIndex]
              );
              break;
          }
          break;
        default:
          done = true;
          break;
      }
    }

    return pos;
  }

  parseLongopt(
    pos: number,
    context: ParserContext,
    longopts: Record<string, Option>
  ): number {
    let idx = context.arg.indexOf(" ", pos);
    if (idx == -1) {
      idx = context.arg.length;
    }
    const longopt = context.arg.substring(pos, idx);
    if (longopt.length == 0) {
      // no more option processing "--"
      return pos + 2;
    }

    if (longopt in longopts) {
      const option = longopts[longopt];
      pos = pos + longopt.length;
      pos = this.application.api.string.findNonWhitespace(context.arg, pos);
      if (option.param !== "none") {
        pos = this.parseParam(
          pos,
          context,
          option.fieldRef,
          option.param === "multi"
        );
      }
    } else {
      // bad opt
    }

    return pos;
  }

  parseOpt(
    pos: number,
    context: ParserContext,
    opts: Record<number, Option>
  ): number {
    let done = false;
    do {
      const opt = context.arg.charCodeAt(pos);
      pos += 1;
      if (opt in opts) {
        const option = opts[opt];
        if (option.param !== "none") {
          pos = this.application.api.string.findNonWhitespace(context.arg, pos);
          pos = this.parseParam(
            pos,
            context,
            option.fieldRef,
            option.param === "multi"
          );
          done = true;
        } else {
          context.model[option.fieldRef] = new Boolean(true).toString();
        }
      } else {
        if (opt === TAB || opt === SPACE) {
          done = true;
        } else {
          // bad opt
        }
      }
      if (pos >= context.arg.length) {
        done = true;
      }
    } while (!done);

    return pos;
  }

  parseParam(
    pos: number,
    context: ParserContext,
    fieldRef: string,
    multi: boolean
  ): number {
    const stringApi = this.application.api.string;
    const end = stringApi.matchQuote(context.arg, pos);
    let param = context.arg.substring(pos, end);
    param = stringApi.unquote(param);
    param = stringApi.unescape(param);
    if (multi) {
      if (!(fieldRef in context.model)) {
        context.model[fieldRef] = [];
        context.multi[fieldRef] = true;
      }
      (context.model[fieldRef] as string[]).push(param);
    } else {
      if (fieldRef in context.model) {
        // XXX throw error?
      }
      context.model[fieldRef] = param;
      context.multi[fieldRef] = false;
    }

    pos = end;
    return pos;
  }

  parseArgs(pos: number, context: ParserContext, syntax: Syntax): number {
    const stringApi = this.application.api.string;

    if (!syntax.args) {
      return pos;
    }

    const syntaxArgs = syntax.args;
    const setModel = (args: (string | string[])[]) => {
      const lastarg = args.length - 1;
      if ((syntaxArgs.length || 0) < lastarg) {
        throw new ParseError();
      }
      const fieldRef = syntaxArgs[lastarg].fieldRef;
      context.model[fieldRef] = args[args.length - 1];
      context.multi[fieldRef] = Array.isArray(args[args.length - 1]);
    };

    if (syntax.maxArgs && syntax.maxArgs >= 0) {
      const args = new Array<string>();
      // hard arg limit
      while (args.length < syntax.maxArgs) {
        pos = stringApi.findNonWhitespace(context.arg, pos);
        if (args.length == syntax.maxArgs - 1) {
          // last arg, consume entire string
          const end = stringApi.matchQuote(context.arg, pos);
          if (end == context.arg.length) {
            // quoted string, unquote
            let param = context.arg.substring(pos, end);
            param = stringApi.unquote(param);
            param = stringApi.unescape(param);
            args.push(param);
            setModel(args);
            pos = end;
          } else {
            // unquoted, add verbatim
            args.push(context.arg.substring(pos));
            setModel(args);
            pos = context.arg.length;
          }
        } else {
          // normal arg, parse out
          const end = this.parseParam(
            pos,
            context,
            syntaxArgs[args.length].fieldRef,
            false
          );
          args.push(context.arg.substring(pos, end));
          pos = end;
        }
      }
    } else {
      const exploded = stringApi.explodeArgs(context.arg);
      const args = new Array<string | string[]>();
      for (let i = 0; i < exploded.length; i++) {
        if (syntaxArgs[args.length].multi) {
          args.push(exploded.slice(i, exploded.length));
          setModel(args);
          break;
        } else {
          args.push(args[i]);
          setModel(args);
        }
      }
      exploded.forEach((arg: string) => {});
      pos = context.arg.length;
    }

    return pos;
  }

  resolveDefaults(context: ParserContext, command: Command): void {
    const commandApi = this.application.api.command;
    const fields = commandApi.resolveFields(command, context);
    for (const field of Object.values(fields)) {
      if (!(field.id in context.model) && field.default) {
        context.model[field.id] = field.default;
      }
    }
  }
}
