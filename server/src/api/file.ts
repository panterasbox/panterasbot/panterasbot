import callsites from "callsites";
import * as Path from "path";
import * as fs from "fs";
import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { Stuff } from "../mud/lib/stuff/Stuff";
import { srcRoot, distRoot } from "../";

export const MUD_ROOT = "/mud/";

const _FileApi = ApplicationContextMixin(Object);

export class FileApi extends _FileApi {
  resolvePath(path: string, rel: string | Stuff | null = null): string {
    if (path.charAt(0) === "/") {
      return path;
    }
    if (rel === null) {
      const caller = callsites()[1];
      rel = caller.getFileName();
      rel = rel ? this.internalPath(rel) || null : null;
    }
    if (rel instanceof Stuff) {
      const moduleApi = this.application.api.module;
      const module = moduleApi.findModule(rel.constructor);
      rel = (module?.path as string) || null;
      rel = rel ? this.internalPath(rel) || null : null;
    }
    if (typeof rel === "string") {
      return Path.join(rel, path);
    }
    return path;
  }

  readFileSync(path: string): string {
    return fs.readFileSync(this.externalPath(path), "utf-8");
  }

  srcPath(internalPath: string): string {
    return Path.join(srcRoot, MUD_ROOT, internalPath);
  }

  distPath(internalPath: string): string {
    return Path.join(distRoot, MUD_ROOT, internalPath);
  }

  externalPath(internalPath: string): string {
    if (internalPath.endsWith(".js")) {
      return this.distPath(internalPath);
    } else {
      return this.srcPath(internalPath);
    }
  }

  internalPath(externalPath: string): string {
    const srcPath = Path.join(srcRoot, MUD_ROOT);
    if (externalPath.startsWith(srcPath)) {
      return Path.sep + Path.relative(srcPath, externalPath);
    }

    const distPath = Path.join(distRoot, MUD_ROOT);
    if (externalPath.startsWith(distPath)) {
      return Path.sep + Path.relative(distPath, externalPath);
    }

    throw new Error(
      `Path provided to internalPath() not external: ${externalPath}`
    );
  }
}
