import { Application } from "../backend/Application";
import { ArrayApi } from "./array";
import { AssertApi } from "./assert";
import { CallstackApi } from "./callstack";
import { CommandApi } from "./command";
import { CommandLineApi } from "./command-line";
import { EventApi } from "./event";
import { FileApi } from "./file";
import { GrammarApi } from "./grammar";
import { MessageApi } from "./message";
import { ModuleApi } from "./module";
import { MqlApi } from "./mql";
import { MudlogApi } from "./mudlog";
import { ObjectApi } from "./object";
import { PlayerApi } from "./player";
import { StringApi } from "./string";
import { StuffApi } from "./stuff";
import { TemplateApi } from "./template";
import { TimeApi } from "./time";
import { UserApi } from "./user";

export class API {
  stuff: StuffApi;
  user: UserApi;
  player: PlayerApi;
  message: MessageApi;
  command: CommandApi;
  string: StringApi;
  array: ArrayApi;
  file: FileApi;
  module: ModuleApi;
  object: ObjectApi;
  mql: MqlApi;
  commandLine: CommandLineApi;
  mudlog: MudlogApi;
  grammar: GrammarApi;
  assert: AssertApi;
  event: EventApi;
  time: TimeApi;
  template: TemplateApi;
  callstack: CallstackApi;

  constructor(application: Application) {
    this.stuff = new StuffApi(application);
    this.user = new UserApi(application);
    this.player = new PlayerApi(application);
    this.message = new MessageApi(application);
    this.command = new CommandApi(application);
    this.string = new StringApi(application);
    this.array = new ArrayApi(application);
    this.file = new FileApi(application);
    this.module = new ModuleApi(application);
    this.object = new ObjectApi(application);
    this.mql = new MqlApi(application);
    this.commandLine = new CommandLineApi(application);
    this.mudlog = new MudlogApi(application);
    this.grammar = new GrammarApi(application);
    this.assert = new AssertApi(application);
    this.event = new EventApi(application);
    this.time = new TimeApi(application);
    this.template = new TemplateApi(application);
    this.callstack = new CallstackApi(application);
  }

  async initialize() {
    // nothing to do
  }
}
