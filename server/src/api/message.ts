import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { Sensor } from "../mud/lib/character/Sensor";
import { CommandGiver } from "../mud/lib/command/CommandGiver";
import { CommandShell } from "../mud/lib/connection/CommandShell";
import { Containable } from "../mud/lib/stuff/Containable";
import { Container } from "../mud/lib/stuff/Container";
import { Avatar } from "../mud/obj/Avatar";
import { hasMixin, Mixins } from "../util/mixin";

export interface MessagePayload {
  topic: string;
  message: string;
  params?: {};
}

const _MessageApi = ApplicationContextMixin(Object);

export class MessageApi extends _MessageApi {
  message(sensor: Sensor, message: MessagePayload): boolean {
    message.message = this.composeMessage(message.message);
    try {
      sensor.interceptMessage(message);
    } catch (e) {
      console.warn("TODO: Handle exceptions from interceptMessage()");
    }
    sensor.onMessage(message);
    return true;
  }

  messageAll(sensors: Sensor[], message: MessagePayload): number {
    message.message = this.composeMessage(message.message);
    let result = 0;
    sensors.forEach((sensor) => {
      try {
        sensor.interceptMessage(message);
      } catch (e) {
        console.warn("TODO: Handle exceptions from interceptMessage()");
      }
      sensor.onMessage(message);
      result++;
    });
    return result;
  }

  messageContainer(
    container: Container,
    message: MessagePayload,
    exclude: Containable[] = []
  ): number {
    const sensors: Sensor[] = [];
    const excludeSet = new Set(exclude);
    container.inventory.forEach((containable) => {
      if (
        !excludeSet.has(containable) &&
        hasMixin(containable, Mixins.Sensor)
      ) {
        sensors.push((containable as unknown) as Sensor);
      }
    });
    return this.messageAll(sensors, message);
  }

  composeMessage(message: string): string {
    return `<?xml version="1.0"?>
<message xmlns="http://bot.panterasbox.com" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="http://bot.panterasbox.com message.xsd">
  ${message}
</message>
    `;
  }

  prompt(shell: CommandShell) {
    const prompt =
      typeof shell.prompt === "function" ? shell.prompt() : shell.prompt;
    shell.onPrompt(prompt);
  }

  echo(shell: CommandShell, command: string) {
    const prompt =
      typeof shell.prompt === "function" ? shell.prompt() : shell.prompt;
    shell.onEcho(command, prompt);
  }
}
