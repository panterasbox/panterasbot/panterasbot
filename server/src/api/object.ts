import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { Constructor } from "../util/mixin";
import * as Path from "path";

export const NAME_DELIM = "#";

const _ObjectApi = ApplicationContextMixin(Object);

export class ObjectApi extends _ObjectApi {
  singletons: Map<Constructor, Object> = new Map();

  moduleExport(module: string, xport: string) {
    return `${module}${NAME_DELIM}${xport}`;
  }

  async load(name: string): Promise<Function> {
    const fileApi = this.application.api.file;
    const moduleApi = this.application.api.module;

    name = moduleApi.resolve(name);
    const [path, xport] = name.split(NAME_DELIM);

    if (!xport) {
      throw new Error(`Cannot determine module export: ${name}`);
    }

    try {
      const distPath = fileApi.distPath(path);
      const thisPath = __dirname;
      const relPath = Path.relative(thisPath, distPath);
      const exports = await import(relPath);
      const result = exports[xport];

      if (typeof result !== "function") {
        throw new Error(`Module export not a function: ${name}`);
      }
      return result;
    } catch (err) {
      console.warn(`Caught error loading module ${xport}`, err);
      throw err;
    }
  }

  async clone<T>(arg: Constructor<T> | string, ...args: any): Promise<T> {
    let blueprint: Constructor<T>;
    if (typeof arg === "string") {
      blueprint = (await this.load(arg)) as Constructor<T>;
    } else {
      blueprint = arg;
    }

    if (!blueprint) {
      throw new Error(`Could not load blueprint for ${arg}`);
    }

    try {
      const result = new blueprint(...args);
      return result;
    } catch (err) {
      console.warn(`Caught error cloning ${name}`, err);
      throw err;
    }
  }

  async singleton<T>(arg: Constructor<T> | string, ...args: any): Promise<T> {
    let blueprint: Constructor<T>;
    if (typeof arg === "string") {
      blueprint = (await this.load(arg)) as Constructor<T>;
    } else {
      blueprint = arg;
    }
    if (this.singletons.has(blueprint)) {
      return this.singletons.get(blueprint) as T;
    }
    const instance: T = new blueprint(...args);
    this.singletons.set(blueprint, instance);
    return instance;
  }
}
