import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { Pronouns } from "../mud/lib/character/Gendered";
import { Player } from "../mud/lib/connection/Player";
import { User } from "../mud/lib/connection/User";
import { TwitchUser } from "../mud/lib/twitch/TwitchUser";
import { AvatarType } from "../mud/obj/Avatar";
import { surnames } from "../util/names";

const _PlayerApi = ApplicationContextMixin(Object);

export class PlayerApi extends _PlayerApi {
  async getPlayer(_id: string): Promise<Player | null> {
    const interactive = this.application.findInteractiveByPlayer(_id);
    let player = interactive?.avatar?.player;
    if (!player) {
      player = (await Player.findById(_id)) as Player;
    }

    const user = await this.application.api.user.getUser(player.userId);
    if (!user) {
      console.warn("No user found for player ", player.id, player.userId);
      return null;
    }
    player.user = user;

    return player;
  }

  newGuest(): Player {
    const user = new User({
      isGuest: true,
      isStreamer: false,
      isFollower: false,
      subscriberTier: 0,
    });

    const player = new Player({
      firstName: "Guest",
      lastName: surnames[Math.floor(Math.random() * surnames.length + 1)],
      userId: user._id,
      pronouns: Pronouns.They,
    });

    player.user = user;

    return player;
  }

  findAvatars(): AvatarType[] {
    return this.application
      .allInteractives()
      .map((interactive) => {
        return interactive.avatar;
      })
      .filter((avatar) => avatar !== undefined) as AvatarType[];
  }

  async loadTwitchPlayer(twitchUser: TwitchUser): Promise<Player> {
    let user = undefined;
    if (twitchUser._id) {
      user = await User.findTwitchUser(twitchUser._id);
    }
    if (!user) {
      user = new User({ twitchUserId: twitchUser._id });
    }
    this.application.api.user.userForTwitchUser(user, twitchUser);
    await user.save();

    if (!user._id) {
      throw new Error(`No user id for twitch user ${twitchUser._id}`);
    }

    let player = await Player.findByUser(user._id);
    if (!player) {
      player = new Player({ userId: user._id });
      // TODO remove this
      [player.firstName, player.lastName] = twitchUser.displayName.split(
        "_",
        2
      );
      await player.save();
    }
    player.user = user;
    return player;
  }

  toPronouns(str: string): Pronouns | null {
    switch (str) {
      case "he":
      case "him":
      case "his":
      case "he/him":
      case "he/him/his":
        return Pronouns.He;
      case "she":
      case "her":
      case "hers":
      case "she/her":
      case "she/her/hers":
        return Pronouns.She;
      case "they":
      case "them":
      case "their":
      case "they/them":
      case "they/them/their":
        return Pronouns.They;
      case "it":
      case "its":
      case "it/it":
      case "it/it/its":
        return Pronouns.It;
      default:
        return null;
    }
  }
}
