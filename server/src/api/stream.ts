import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";

export enum StreamStatus {
  Starting = "starting",
  Standby = "standby",
  Live = "live",
  Ending = "ending",
}

const _StreamApi = ApplicationContextMixin(Object);

export class StreamApi extends _StreamApi {}
