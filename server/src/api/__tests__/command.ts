import { Backend } from "../../backend/Backend";
import { Avatar } from "../../mud/obj/Avatar";
import { Player, PlayerType } from "../../mud/lib/connection/Player";
import { ApplicationContextMixin } from "../../mud/lib/ApplicationContext";

jest.setTimeout(100000);

beforeEach(() => {});

const backend = new Backend();

describe("When command is parsed", () => {
  beforeAll(async () => {
    await backend.initialize();
    await backend.start();
  });

  // it("Parse it", async () => {
  //   const application = backend.application;
  //   const AppCtx = ApplicationContextMixin(Object);
  //   class AppTest extends AppCtx {
  //     async doTest(player: PlayerType) {
  //       const avatar = new Avatar(player);
  //       await avatar.moveToStartLocation();
  //       const actual = await avatar.doCommand({
  //         command: "look here",
  //         params: {}
  //       });
  //       return actual;
  //     }
  //   }
  //   const test = new AppTest(application);
  //   const player = new Player(application);
  //   const actual = await test.doTest(player);
  //   console.log(actual);
  //   expect(actual).toBeTruthy();
  // });

  // it("Logon", async () => {
  //   const application = backend.application;
  //   await backend.onClientConnection({}, {});
  //   console.log(backend.application.allInteractives().map(i => {
  //     return [ i.avatar, i.avatar?.environment ];
  //   }));
  // });
});
