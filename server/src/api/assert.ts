import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";

const _AssertApi = ApplicationContextMixin(Object);

export class AssertApi extends _AssertApi {
  isInstanceOf(object: any, type: Function): void {
    if (!(object instanceof type)) {
      throw new Error(`AssertError: instanceof ${type.name} false`);
    }
  }
}
