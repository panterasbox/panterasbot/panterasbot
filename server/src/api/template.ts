import YAML from "yaml";
import * as Path from "path";
import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { StuffTemplate } from "../mud/lib/domain/StuffTemplate";
import { Stuff, TemplateInfo } from "../mud/lib/stuff/Stuff";
import { Containable } from "../mud/lib/stuff/Containable";
import { hasMixin, Mixins } from "../util/mixin";
import { MultiLocation } from "../mud/lib/location/MultiLocation";
import { Location } from "../mud/lib/location/Location";

const _TemplateApi = ApplicationContextMixin(Object);

export class TemplateApi extends _TemplateApi {
  async loadStuff(path: string): Promise<StuffTemplate> {
    const fileApi = this.application.api.file;
    path = fileApi.resolvePath(path);
    const file = fileApi.readFileSync(`${path}.yaml`);
    let stuffTemplate = await StuffTemplate.findByPath(path);

    const template = YAML.parse(file);
    // TODO validate yaml against schema
    if (stuffTemplate) {
      stuffTemplate.template = template;
    } else {
      stuffTemplate = new StuffTemplate({
        name: Path.basename(path),
        domain: Path.dirname(path),
        schemaPath: template.schema,
        type: "text/yaml",
        path,
        template,
      });
    }

    await stuffTemplate.save();
    return stuffTemplate;
  }

  // loadDomain(template: string): DomainTemplate {

  // }

  async applyTemplate<T extends Stuff>(template: StuffTemplate): Promise<T> {
    const objectApi = this.application.api.object;
    const stuff = await objectApi.clone<T>(template.template.implementation);
    stuff.applyTemplate(template);
    if (
      stuff.template?.id != template._id ||
      stuff.template?.path != template.path
    ) {
      console.warn(
        "Template mismatch after apply, overriding",
        stuff,
        stuff.template
      );
      stuff.template = new TemplateInfo(template.path, template._id);
    }
    return stuff;
  }

  async routeLocationTemplate(
    path: string,
    stuff: Stuff & Containable
  ): Promise<Location | undefined> {
    const stuffApi = this.application.api.stuff;
    const templateApi = this.application.api.template;

    let destination: Location | undefined = undefined;
    const locations = stuffApi.findStuffByTemplate(path);
    for (const location of locations) {
      if (!(location instanceof Location)) {
        continue;
      }
      if (hasMixin(location, Mixins.MultiLocation)) {
        const multi = (location as unknown) as MultiLocation;
        destination = multi.routeEntrance(stuff);
        break;
      } else {
        destination = location;
      }
    }
    if (!destination) {
      const template = await templateApi.loadStuff(path);
      const stuff = await templateApi.applyTemplate(template);
      if (stuff instanceof Location) {
        destination = stuff;
      }
    }
    return destination;
  }
}
