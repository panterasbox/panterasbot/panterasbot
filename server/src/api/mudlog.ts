import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { Agent } from "../mud/lib/stuff/Agent";
import { CommandContext } from "./command";
import esc from "xml-escape";

export enum LogLevel {
  Error = "error",
  Warn = "warn",
  Info = "info",
  Debug = "debug",
  Trace = "trace",
}

const _MudlogApi = ApplicationContextMixin(Object);

export class MudlogApi extends _MudlogApi {
  log(
    level: LogLevel,
    message: string,
    params: {} = {},
    context?: CommandContext,
    target?: Agent
  ): boolean {
    const api = this.application.api;
    if (!context) {
      try {
        context = api.command.getExecutionContext().controllers[0].context;
      } catch (e) {
        // pass
      }
    }
    if (!target && context && context.commandGiver instanceof Agent) {
      target = context.commandGiver as Agent;
    }
    if (!target) {
      return false;
    }

    api.message.message(target, {
      topic: `mudlog.${level}`,
      message: esc(message),
      params: {
        command: context ? context.commandDef : undefined,
        verb: context ? context.verb : undefined,
        subcommand: context ? context.subcommand : undefined,
        ...params,
      },
    });
    return true;
  }

  error(
    message: string,
    params: {} = {},
    context?: CommandContext,
    target?: Agent
  ): boolean {
    return this.log(LogLevel.Error, message, params, context, target);
  }

  warn(
    message: string,
    params: {} = {},
    context?: CommandContext,
    target?: Agent
  ): boolean {
    return this.log(LogLevel.Warn, message, params, context, target);
  }

  info(
    message: string,
    params: {} = {},
    context?: CommandContext,
    target?: Agent
  ): boolean {
    return this.log(LogLevel.Info, message, params, context, target);
  }

  debug(
    message: string,
    params: {} = {},
    context?: CommandContext,
    target?: Agent
  ): boolean {
    return this.log(LogLevel.Debug, message, params, context, target);
  }

  trace(
    message: string,
    params: {} = {},
    context?: CommandContext,
    target?: Agent
  ): boolean {
    return this.log(LogLevel.Trace, message, params, context, target);
  }
}
