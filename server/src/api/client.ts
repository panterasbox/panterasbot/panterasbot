import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";

export type UserAgentPayload = string;

const _ClientApi = ApplicationContextMixin(Object);

export class ClientApi extends _ClientApi {}
