import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { Gendered, Pronouns } from "../mud/lib/character/Gendered";
import { Named } from "../mud/lib/character/Named";
import { Agent } from "../mud/lib/stuff/Agent";

const _GrammarApi = ApplicationContextMixin(Object);

export class GrammarApi extends _GrammarApi {
  possessiveDisplay(named: Named) {
    if (named.displayName.endsWith("s")) {
      return named.displayName + "'";
    } else {
      return named.displayName + "'s";
    }
  }

  subjectiveDisplay(named: Named) {
    return named.displayName;
  }

  possessivePronoun(obj: Named & Gendered & Agent, subj: Agent): string {
    if ((obj as Agent) == subj) {
      return "your";
    } else {
      switch (obj.pronouns) {
        case Pronouns.He:
          return "his";
        case Pronouns.She:
          return "her";
        case Pronouns.They:
          return "their";
        case Pronouns.It:
          return "its";
      }
    }
  }

  subjectivePronoun(obj: Named & Gendered & Agent, subj: Agent): string {
    if ((obj as Agent) == subj) {
      return "you";
    } else {
      switch (obj.pronouns) {
        case Pronouns.He:
          return "he";
        case Pronouns.She:
          return "she";
        case Pronouns.They:
          return "they";
        case Pronouns.It:
          return "it";
      }
    }
  }

  possessive(obj: Named & Gendered & Agent, subj: Agent): string {
    if ((obj as Agent) == subj) {
      return this.possessivePronoun(obj, subj) || this.possessiveDisplay(obj);
    } else {
      return this.possessiveDisplay(obj);
    }
  }

  subjective(obj: Named & Gendered & Agent, subj: Agent): string {
    if ((obj as Agent) == subj) {
      return this.subjectivePronoun(obj, subj) || this.subjectiveDisplay(obj);
    } else {
      return this.subjectiveDisplay(obj);
    }
  }

  pronounBe(obj: Named & Gendered & Agent, subj: Agent): string {
    let verb = "is";
    if ((obj as Agent) == subj) {
      verb = "are";
    } else if (obj.pronouns == Pronouns.They) {
      verb = "are";
    }
    return `${this.subjectivePronoun(obj, subj)} ${verb}`;
  }
}
