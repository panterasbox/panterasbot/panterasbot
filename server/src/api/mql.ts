import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { CommandGiver } from "../mud/lib/command/CommandGiver";
import { Containable } from "../mud/lib/stuff/Containable";
import { hasMixin, Mixins } from "../util/mixin";

const _MqlApi = ApplicationContextMixin(Object);

export class MqlApi extends _MqlApi {
  queryOne(query: string): Object | null {
    const result = this.query(query);
    if (result.length) {
      return result[0];
    }
    return null;
  }

  query(query: string): Object[] {
    const commandApi = this.application.api.command;
    const exec = commandApi.getExecutionContext();

    let commandGiver: CommandGiver | undefined = undefined;
    if (exec.controllers.length) {
      if (exec.controllers[0].context) {
        commandGiver = exec.controllers[0].context.commandGiver;
      }
    }

    query = query.trim();
    if (query === "me") {
      if (commandGiver) {
        return [commandGiver];
      } else {
        return [];
      }
    } else if (query === "here") {
      if (commandGiver && hasMixin(commandGiver, Mixins.Containable)) {
        const environment = (<Containable>(<unknown>commandGiver)).environment;
        if (environment) {
          return [environment];
        } else {
          return [];
        }
      } else {
        return [];
      }
    }

    return [];
  }
}
