import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { IUser, User } from "../mud/lib/connection/User";
import { TwitchUser } from "../mud/lib/twitch/TwitchUser";

const _UserApi = ApplicationContextMixin(Object);

export class UserApi extends _UserApi {
  async getUser(_id: string): Promise<User | null> {
    const interactive = this.application.findInteractiveByUser(_id);
    let user = interactive?.avatar?.player.user;
    if (!user) {
      user = (await User.findById(_id)) as User;
    }
    return user;
  }

  userForTwitchUser(user: IUser, twitchUser: TwitchUser): void {
    const channel = this.application.twitchController.channel;
    user.username = twitchUser.displayName;
    user.email = twitchUser.email;
    user.avatar = twitchUser.profilePictureUrl;
    user.isGuest = false;
    user.isStreamer = channel?.externalId == twitchUser.externalId;
    user.isFollower = false; // TODO make this work
    user.subscriberTier = 0; // TODO make this work
  }
}
