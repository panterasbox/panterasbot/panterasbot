import { DoublyLinkedList } from "@datastructures-js/linked-list";
import { result } from "lodash";
import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { Stuff } from "../mud/lib/stuff/Stuff";

const _StuffApi = ApplicationContextMixin(Object);

export class StuffApi extends _StuffApi {
  stuffIdMap: Map<string, WeakRef<Stuff>>;
  stuffTemplateMap: Map<string, DoublyLinkedList<WeakRef<Stuff>>>;

  constructor(...args: any[]) {
    super(...args);
    this.stuffIdMap = new Map();
    this.stuffTemplateMap = new Map();
  }

  registerStuff<T extends Stuff>(stuff: T): void {
    this.stuffIdMap.set(stuff.id, new WeakRef(stuff));
    if (stuff.template?.path) {
      if (!this.stuffTemplateMap.has(stuff.template.path)) {
        this.stuffTemplateMap.set(stuff.template.path, new DoublyLinkedList());
      }
      const list = this.stuffTemplateMap.get(
        stuff.template.path
      ) as DoublyLinkedList<WeakRef<Stuff>>;
      list.insertLast(new WeakRef(stuff));
    }
  }

  findStuffById(id: string): Stuff | null {
    const stuff = this.stuffMap.get(id)?.deref() || null;
    if (!stuff && this.stuffMap.has(id)) {
      // XXX batch this?
      this.stuffIdMap.delete(id);
    }
    return stuff;
  }

  findStuffByTemplate(template: string): Stuff[] {
    const list = this.stuffTemplateMap.get(template);
    if (!list) {
      return [];
    }
    const result: Stuff[] = [];
    list.forEach((node) => {
      const stuff = node.getValue().deref();
      if (!stuff) {
        list.remove(node);
      } else {
        result.push(stuff);
      }
    });
    return result;
  }
}
