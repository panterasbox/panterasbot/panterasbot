import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";

const SPACE = " ".charCodeAt(0);
const QUOTE = "'".charCodeAt(0);
const DBLQUOTE = '"'.charCodeAt(0);

const _StringApi = ApplicationContextMixin(Object);

export class StringApi extends _StringApi {
  static StringBuffer = class {
    buffer = new Array<string>();
    index = 0;

    constructor(str?: string) {
      str && this.append(str);
      this.index = str ? 1 : 0;
    }

    append(str: string) {
      this.buffer[this.index] = str;
      this.index += 1;
      return this;
    }

    prepend(str: string) {
      this.buffer.unshift(str);
      this.index += 1;
      return this;
    }

    toString() {
      return this.buffer.join("");
    }
  };

  findNonWhitespace(str: string, start: number): number {
    for (
      let len = str.length;
      start < len && str.charCodeAt(start) == SPACE;
      start++
    );
    return start;
  }

  matchQuote(str: string, start: number, quoteChars = "\"'"): number {
    let end = undefined;

    const char = str.charAt(start);
    if (quoteChars.indexOf(char) != -1) {
      end = this.searchUnescaped(str, char, start + 1, 1);
    } else {
      end = this.searchUnescaped(str, " ", start + 1, 1);
    }
    return end < 0 ? str.length : end;
  }

  unquote(str: string): string {
    // no room for quotes
    if (str.length < 2) {
      return str;
    }

    if (
      (str.charCodeAt(0) == DBLQUOTE || str.charCodeAt(0) == QUOTE) &&
      str.charCodeAt(0) == str.charCodeAt(str.length - 1)
    ) {
      return str.length == 2 ? "" : str.substring(1, str.length - 2);
    } else {
      return str;
    }
  }

  unescape(str: string): string {
    const buffer = new StringApi.StringBuffer();

    for (let i = 0, next = 0; i < str.length; i = next + 2) {
      next = str.indexOf("\\", i);
      if (next == -1) {
        buffer.append(str.substring(i, str.length));
        return buffer.toString();
      }
      if (next == str.length - 1) {
        if (i == next) {
          return buffer.toString();
        } else {
          buffer.append(str.substring(i, str.length - 2));
          return buffer.toString();
        }
      }
      buffer.append(str.substring(i, next - 1));
      buffer.append(str.substring(next + 1, next + 1));
    }

    return buffer.toString();
  }

  isEscaped(str: string, pos: number): number {
    // It's complicated because backslashes can be escaped too.
    const arrayApi = this.application.api.array;
    const lastNonBackslash = arrayApi.antiIndexOf(
      Array.from(str),
      "\\",
      pos - 1,
      -1
    );
    return (pos - lastNonBackslash + 1) % 2;
  }

  searchUnescaped(
    str: string,
    char: string,
    pos: number,
    step: number = 1
  ): number {
    for (let len = str.length; pos < len && pos >= 0; pos += step) {
      if (str.charAt(pos) == char && !this.isEscaped(str, pos)) {
        return pos;
      }
    }
    return -1;
  }

  capitalize(str: string): string {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  buildTemplate<T extends any[]>(
    ...transforms: Function[]
  ): (parts: TemplateStringsArray, ...args: T) => string {
    return (parts: TemplateStringsArray, ...args: T): string => {
      let result = new StringApi.StringBuffer();
      for (let i = 0; i < parts.length; i++) {
        result.append(parts[i]);
        result.append(transforms[i](args[i]));
      }
      return result.toString();
    };
  }

  explodeArgs(
    arg: string,
    options: {
      preserveQuotes?: boolean;
    } = {}
  ): string[] {
    const args: string[] = [];
    if (!arg && !arg.length) {
      return args;
    }

    let pos = 0;
    while (pos < arg.length) {
      pos = this.findNonWhitespace(arg, pos);
      const newPos = this.matchQuote(arg, pos);
      const thisArg = arg.substring(pos, newPos + 1);

      if (options.preserveQuotes) {
        args.push(thisArg);
      } else {
        const unquoted = this.unquote(thisArg);
        const unescaped = this.unescape(unquoted);
        args.push(unescaped);
      }
      pos = newPos + 1;
    }

    return args;
  }
}
