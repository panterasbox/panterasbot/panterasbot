import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";

const _ArrayApi = ApplicationContextMixin(Object);

export class ArrayApi extends _ArrayApi {
  antiIndexOf(arr: any[], el: any, start = 0, step = 1): number {
    if (step > 0) {
      for (; start < arr.length && arr[start] == el; start += step);
      return start < arr.length ? start : -1;
    } else {
      for (; start >= 0 && arr[start] == el; start += step);
      return start >= 0 ? start : -1;
    }
  }
}
