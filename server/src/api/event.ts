import { EventEmitter } from "events";
import moment from "moment";
import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";

export const WellKnownEvents = {
  connect: Symbol("event.connect"),
  disconnect: Symbol("event.disconnect"),
  login: Symbol("event.login"),
  logout: Symbol("event.logout"),
  streamStatus: Symbol("event.streamStatus"),
};

export type EventType<P> = string | symbol;

export type StuffEvent<P> = {
  type: EventType<P>;
  time: number;
  payload: P;
};

export type EventCallback<P> = (event: StuffEvent<P>) => void;

const _EventApi = ApplicationContextMixin(Object);

export class EventApi extends _EventApi {
  emitter: EventEmitter = new EventEmitter();

  constructor(...args: any) {
    super(...args);
  }

  subscribe<P>(type: EventType<P>, cb: EventCallback<P>) {
    this.emitter.on(EventApi.normalizeType(type), cb);
  }

  unsubscribe<P>(type: EventType<P>, cb: EventCallback<P> | undefined) {
    if (cb) {
      this.emitter.removeListener(EventApi.normalizeType(type), cb);
    } else {
      this.emitter.removeAllListeners(EventApi.normalizeType(type));
    }
  }

  dispatch<P>(type: EventType<P>, payload: P): boolean {
    const normalizedType = EventApi.normalizeType(type);
    const event = {
      type: normalizedType,
      time: moment().valueOf(),
      payload,
    };
    return this.emitter.emit(normalizedType, event);
  }

  static normalizeType(type: EventType<any>): string {
    return type.toString();
  }
}
