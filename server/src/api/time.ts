import moment from "moment";
import { ApplicationContextMixin } from "../mud/lib/ApplicationContext";
import { StringApi } from "./string";

const _TimeApi = ApplicationContextMixin(Object);

export class TimeApi extends _TimeApi {
  formatDuration(duration: moment.Duration): string {
    let negative = false;
    if (duration.asMilliseconds() < 0) {
      duration = duration.clone().abs();
      negative = true;
    }

    const sb = new StringApi.StringBuffer(
      String(duration.seconds()).padStart(2, "0")
    );
    if (duration.minutes()) {
      sb.prepend(":");
      sb.prepend(String(duration.minutes()).padStart(2, "0"));
    } else {
      if (duration.hours()) {
        sb.prepend("00:");
      } else {
        sb.prepend("0:");
      }
    }
    if (duration.hours()) {
      sb.prepend(":");
      sb.prepend(duration.hours().toString());
    }
    if (negative) {
      sb.prepend("-");
    }
    return sb.toString();
  }

  remainingDuration(seconds: number, time: number): moment.Duration {
    time = time / 1000;
    const current = moment().valueOf() / 1000;
    return moment.duration(seconds - (current - time), "seconds");
  }
}
