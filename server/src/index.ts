import "reflect-metadata";
import * as path from "path";
import * as ngrok from "ngrok";
import { Backend } from "./backend/Backend";
import { spawnServer } from "./server/";

const clientBuildDir = path.join(__dirname, "..", "..", "client", "build");

const start = async () => {
  const backend = new Backend();

  console.debug("Initializing backend");
  await backend.initialize();

  if (process.env.NODE_ENV === "development") {
    backend.twitchWebhook.callbackHost = await ngrok.connect({
      proto: "http",
      addr: backend.getParameter("server_port"),
      region: "us",
      onStatusChange: (status) => {
        console.info(`ngrok status change`, status);
      },
      onLogEvent: (data) => {
        console.debug(`ngrok log`, data);
      },
    });
    console.log("cbhost", backend.twitchWebhook.callbackHost);
  }

  console.debug("Starting backend");
  await backend.start();
  console.debug("Spawning server");
  const server = await spawnServer(backend, clientBuildDir);
};

const distRoot = __dirname;
const srcRoot = distRoot.replace(/dist$/, "src");
export { srcRoot, distRoot };

if (require.main === module) {
  start().then(() => {
    console.debug("Started.");
  });
}
