import { Express } from "express";
import * as session from "express-session";
import passport from "passport";
import * as url from "url";
import OAuth2Strategy from "passport-oauth2";
import { Gitlab } from "@gitbeaker/node";
import { Backend } from "../backend/Backend";
import { GitlabUser } from "../mud/lib/gitlab/GitlabUser";

export const setupGitlab = (app: Express, backend: Backend) => {
  passport.use(
    "gitlab",
    new GitlabStrategy(
      backend,
      (
        accessToken: string,
        refreshToken: string,
        profile: any,
        done: OAuth2Strategy.VerifyCallback
      ) => {
        if (!profile) {
          throw new Error("Gitlab profile contains no user data");
        }

        backend
          .onGitlabAuth(profile, accessToken, refreshToken)
          .then((gitlabUser: GitlabUser) => {
            done(null, profile);
          });

        done(null, profile);
      }
    )
  );

  app.get(
    "/auth/gitlab",
    passport.authenticate("gitlab", { scope: "read_user" })
  );

  app.get(
    "/auth/gitlab/callback",
    passport.authenticate("gitlab", {
      successRedirect: "/auth/gitlab/success",
      failureRedirect: "/",
    })
  );

  app.get("/auth/gitlab/success", async (req, res) => {
    req.session.gitlabId = req.session.passport?.user.id;
    await backend.onGitlabConnection(req.session as session.SessionData);
    res.redirect("/dash");
  });
};

export class GitlabStrategy extends OAuth2Strategy {
  _baseURL: string;
  name: string;
  _profileURL: string;

  constructor(backend: Backend, verify: OAuth2Strategy.VerifyFunction) {
    const GITLAB_CLIENT_ID = backend.getParameter("gitlab_client_id");
    const GITLAB_SECRET = backend.getSecret("gitlab_client_secret");
    const CALLBACK_URL = backend.getParameter("gitlab_callback_url");

    const baseURL = "https://gitlab.com";

    const options: OAuth2Strategy.StrategyOptions = {
      authorizationURL: url.resolve(baseURL, "oauth/authorize"),
      tokenURL: url.resolve(baseURL, "oauth/token"),
      scope: "read_user",
      scopeSeparator: ",",
      clientID: GITLAB_CLIENT_ID,
      clientSecret: GITLAB_SECRET,
      callbackURL: CALLBACK_URL,
    };

    super(options, verify);

    this._baseURL = baseURL;
    this.name = "twitch";
    this._profileURL = url.resolve(baseURL, "api/v4/user");
    this._oauth2.useAuthorizationHeaderforGET(true);
  }

  userProfile(
    accessToken: string,
    done: (err?: Error | null | undefined, profile?: any) => void
  ) {
    this._oauth2.get(this._profileURL, accessToken, (err, body) => {
      if (err) {
        return done(new Error(JSON.stringify(err)));
      }

      try {
        const json = JSON.parse(body?.toString() || "");
        done(null, json);
      } catch (ex) {
        return done(ex);
      }
    });
  }
}

export const connectApp = async (backend: Backend) => {
  const GITLAB_TOKEN = backend.getSecret("gitlab_access_token");

  const api = new Gitlab({
    token: GITLAB_TOKEN,
    camelize: false, // gonna do this manually
  });

  return api;
};
