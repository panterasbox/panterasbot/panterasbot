import { Express } from "express";
import * as session from "express-session";
import passport from "passport";
import * as url from "url";
import * as crypto from "crypto";
import axios from "axios";
import OAuth2Strategy from "passport-oauth2";
import { ApiClient } from "twitch";
import {
  AccessToken,
  RefreshableAuthProvider,
  StaticAuthProvider,
} from "twitch-auth";
import { Backend } from "../backend/Backend";
import { TwitchUser } from "../mud/lib/twitch/TwitchUser";

const AUTH_BASE_URL = "https://id.twitch.tv/";

export const setupTwitch = (app: Express, backend: Backend) => {
  const twitchNotifications: Set<string> = new Set<string>();

  passport.use(
    "twitch",
    new TwitchStrategy(
      backend,
      (
        accessToken: string,
        refreshToken: string,
        profile: any,
        done: OAuth2Strategy.VerifyCallback
      ) => {
        if (!profile.data.length) {
          throw new Error("Twitch profile contains no user data");
        }

        backend
          .onTwitchAuth(profile, accessToken, refreshToken)
          .then((twitchUser?: TwitchUser) => {
            done(null, profile);
          });
      }
    )
  );

  const twitchAuth = passport.authenticate("twitch", { scope: "user_read" });
  app.get("/auth/twitch", (req, res, next) => {
    req.session.twitchReferer = req.headers.referer;
    return twitchAuth(req, res, next);
  });

  app.get(
    "/auth/twitch/callback",
    passport.authenticate("twitch", {
      successRedirect: "/auth/twitch/success",
      failureRedirect: "/",
    })
  );

  app.get("/auth/twitch/success", async (req, res) => {
    req.session.twitchId = req.session.passport?.user.data[0].id;
    await backend.onTwitchConnection(req.session as session.SessionData);
    res.redirect(req.session.twitchReferer || "/dash");
  });

  app.post("/webhook/twitch/callback", (req, res) => {
    if (
      req.body.subscription.status === "webhook_callback_verification_pending"
    ) {
      const message =
        (req.headers["twitch-eventsub-message-id"]?.toString() || "") +
        (req.headers["twitch-eventsub-message-timestamp"]?.toString() || "") +
        JSON.stringify(req.body);

      console.log("webhook", req.body);

      var hash = crypto
        .createHmac("SHA256", backend.twitchWebhook.secret || "")
        .update(message)
        .digest("hex");

      if (
        req.headers["twitch-eventsub-message-signature"] != `sha256=${hash}`
      ) {
        res.sendStatus(403);
      } else {
        res.status(202).send(req.body.challenge);
      }
    } else if (req.body.event) {
      const messageId = req.headers["twitch-eventsub-message-id"]?.toString();
      if (messageId && twitchNotifications.has(messageId)) {
        res.sendStatus(200);
      } else {
        if (
          backend.twitchWebhook.handleEvent(
            req.body.subscription.type,
            req.body.event
          )
        ) {
          messageId && twitchNotifications.add(messageId);
          res.sendStatus(200);
        } else {
          res.sendStatus(400);
        }
      }
    }
  });
};

export class TwitchStrategy extends OAuth2Strategy {
  _baseURL: string;
  name: string;
  _profileURL: string;
  _clientID: string;

  constructor(backend: Backend, verify: OAuth2Strategy.VerifyFunction) {
    const TWITCH_CLIENT_ID = backend.getParameter("twitch_client_id");
    const TWITCH_SECRET = backend.getSecret("twitch_client_secret");
    const CALLBACK_URL = backend.getParameter("twitch_callback_url");

    const options: OAuth2Strategy.StrategyOptions = {
      authorizationURL: url.resolve(AUTH_BASE_URL, "oauth2/authorize"),
      tokenURL: url.resolve(AUTH_BASE_URL, "oauth2/token"),
      clientID: TWITCH_CLIENT_ID,
      clientSecret: TWITCH_SECRET,
      callbackURL: CALLBACK_URL,
      state: true,
    };

    super(options, verify);

    this._baseURL = AUTH_BASE_URL;
    this.name = "twitch";
    this._profileURL = `${backend.getParameter("twitch_api_url")}/users`;
    this._clientID = TWITCH_CLIENT_ID;
    // this._oauth2.useAuthorizationHeaderforGET(true);
  }

  userProfile(
    accessToken: string,
    done: (err?: Error | null | undefined, profile?: any) => void
  ) {
    var options = {
      url: this._profileURL,
      headers: {
        "Client-ID": this._clientID,
        Accept: "application/vnd.twitchtv.v5+json",
        Authorization: "Bearer " + accessToken,
      },
    };

    axios.get(options.url, options).then((response) => {
      if (response && response.status == 200) {
        done(null, response.data);
      } else {
        done(response.data);
      }
    });
  }
}

export const connectApp = async (backend: Backend): Promise<ApiClient> => {
  const twitchClientId = backend.getParameter("twitch_client_id");
  const twitchSecret = backend.getSecret("twitch_client_secret");

  const authUrl = url.resolve(AUTH_BASE_URL, "oauth2/token");
  const params = {
    client_id: twitchClientId,
    client_secret: twitchSecret,
    grant_type: "client_credentials",
    scope: [].join(" "),
  };

  const response = await axios.post(authUrl, null, { params });
  if (response && response.status == 200) {
    const authProvider = new RefreshableAuthProvider(
      new StaticAuthProvider(twitchClientId, response.data.access_token),
      {
        clientSecret: twitchSecret,
        refreshToken: response.data.refresh_token,
        onRefresh: (token: AccessToken) => {
          // XXX stash session token if I need it?
        },
      }
    );
    const apiClient = new ApiClient({ authProvider });
    return apiClient;
  } else {
    throw new Error("Invalid Twitch Auth response: " + response.status);
  }
};
