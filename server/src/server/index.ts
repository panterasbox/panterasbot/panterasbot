import express from "express";
import session from "express-session";
import cors from "cors";
import referrerPolicy from "referrer-policy";
import { ApolloServer } from "apollo-server-express";
import passport from "passport";
import WebSocket from "ws";
import * as http from "http";
import * as path from "path";
import * as AWS from "aws-sdk";
import { promisify } from "util";
import { Backend } from "../backend/Backend";
import sessionStore from "../util/sessionStore";
import { setupTwitch } from "./twitch";
import { setupGitlab } from "./gitlab";
import config from "../util/config";
import { Context, buildSchema } from "../graphql";

AWS.config.update({ region: config.region });

const SESSION_COOKIE = "panterasbot.sid";

const WEBPACK_DEV_SERVER = "http://localhost:3000";

export const spawnServer = async (backend: Backend, clientDir: string) => {
  const SESSION_SECRET = backend.getSecret("session_secret");

  const app = express();

  app.use(express.json());

  if (process.env.NODE_ENV == "development") {
    app.use(
      cors({
        origin: WEBPACK_DEV_SERVER,
        credentials: true,
      })
    );

    app.use(referrerPolicy({ policy: "no-referrer-when-downgrade" }));
  }

  const sessionParser = session({
    name: SESSION_COOKIE,
    store: sessionStore,
    secret: SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
  });

  app.use(sessionParser);

  app.use(express.static(clientDir));

  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((user, done) => {
    done(null, user);
  });

  const context = async ({ req }: any): Promise<Context> => {
    return {
      sessionInteractive: backend.sessions.get(req.session.sid),
      sessionId: req.session.sid,
      application: backend.application,
    };
  };
  const schema = await buildSchema();
  const apolloServer = new ApolloServer({ schema, context });
  apolloServer.applyMiddleware({ app, cors: false });

  const server = http.createServer(app);

  setupTwitch(app, backend);
  setupGitlab(app, backend);

  app.get("/", function (req, res) {
    res.sendFile(path.join(clientDir, "index.html"));
  });

  app.get("*", (req, res) => {
    res.sendFile(path.join(clientDir, "index.html"));
  });

  const wss = new WebSocket.Server({ server, path: "/backend" });
  backend.connectWsServer(wss);

  wss.on("connection", (ws, req) => {
    try {
      getSession(req)
        .then((session) => {
          backend.onClientConnection(ws, session);
          ws.on("message", (msg: string) => {
            backend.onClientMessage(ws, msg);
          });
          ws.on("close", (code: number, reason: string) => {
            backend.onClientClose(ws, code, reason);
          });
        })
        .catch((e) => {
          console.error("Caught error handling websocket connection", e);
        });
    } catch (e) {
      console.error("Caught error handling websocket connection", e);
    }
  });

  server.listen(backend.getParameter("server_port"), () => {
    console.info(`Server started on ${server.address()} :)`);
  });

  return server;
};

const getSession = async (
  req: http.IncomingMessage
): Promise<session.SessionData> => {
  const cookies = req.headers.cookie?.split(/;\s*/) || [];
  for (let i = 0, j = cookies?.length || 0; i < j; i++) {
    const parts = cookies[i].split("=", 2);
    if (parts && parts[0] === SESSION_COOKIE) {
      const sessionKey = decodeURIComponent(parts[1]);
      const matches = sessionKey.match(/^s:(.*?)\..*/);
      if (matches) {
        const sid = matches[1];
        const getFromStore = promisify(sessionStore.get.bind(sessionStore));
        const setInStore = promisify(sessionStore.set.bind(sessionStore));
        let sessionData = await getFromStore(sid);
        if (!sessionData) {
          sessionData = {
            sid: sid,
            cookie: new session.Cookie(),
          };
        } else {
          sessionData.sid = sid;
        }
        await setInStore(sid, sessionData);
        sessionData = await getFromStore(sid);
        if (!sessionData) {
          throw new Error("error initializing session data");
        }
        return sessionData;
      }
    }
  }
  throw new Error("session cookie not found in headers");
};
