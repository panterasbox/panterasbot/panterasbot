# Pantera's Bot server source structure

## backend/

The game driver. It includes:

- Backend.ts: the backend object keeps track of connections and sessions and initiates the application; this is also where TwitchBot and TwitchWebhook are initiated
- Application.ts: the application object has a 1:1 relationship with the backend, manages event handlers but delegetes most of its responsibility to the controller classes
- \*Controller.ts: the controller objects make up the game engine that allows the rest of the domain objects to interact with each other
- Interactive.ts: the interactive object represesnts an interactive session in the game and holds a player object associated with this interactive's connection

## api/

The API layer is comprised of several classes, each wired to the Backend instance, that provide functionality to the application and to the GraphQL layer. This is the only layer that should be directly interacting with the Dynamo ORM, and most of the logic required for GraphQL query resolvers should go here.

This layer shares some kinship with the lib layer, but while lib/ provides a standard library of game objects and mixins that make up the majority of the game state, api/ provides classes that are external in nature and \[relatively\] stateless. These classes are not meant to be inherited, and are each essentially final singletons.

## lib/

This code makes up the game's standard library. Like the API layer, it is organized by concept; however these classes are meant to be inherited, extended, combined, and instantiated without prejudice. These classes don't have to be stateful, though most of the game's state will reside in the classes defined here.

It should be noted that the library contains foundational classes for both diegetic and non-diegetic objects. That is to say, both for tangible people, places and things within the game world, and ideas that exist outside the immediate narrative (e.g. a weather or calendar controller).

Some of the classes you find here will have cousins in the model/ layer, in cases where they have state that needs to be persisted to the database.

## domain/

The D in MUD. This is where it all happens -- where all the different locations, NPCs, trinkets and events play out. It is organized by the games topology. Zones are collections of rooms and other child zones. Classes of NPCs and game objects are generally associated with a particular zone, and are also organized as such.

Note: we still need to figure out what relationship domain logic has to domain content. it could be the case that domain/ is just the backing classes but the actual landscape of the game is stored away in a document database that gets mapped onto domain instances.

## graphql/

The GraphQL layer contains the query/mutation resolvers for graphql queries. The classes here should be very thin as most of the heavy lifting should be done by the API layer. However, there may be some data sanitization needed to be done that's specific to the externally exposed APIs.

## model/

The ORM model. These are simply data objects annotated to be stored and fetched from the DynamoDB store.

They implement interfaces that are also implemented (and augmented) by their API counterparts, so you should always be able to go from a model class to an standard library class without losing anything. It should be noted, though, that not every property in the lib/ objects will exist on its model/ counterpart -- only those props which are persistable.

## server/

The app server. It's a single process, but it's broken up into a few modules:

- index.ts: main express server setup
- session.ts: setup session middleware
- websocket.ts: setup Websocket server
- graphql.ts: setup Apollo server
- gitlab.ts: setup OAuth2 exchange for GitLab login
- twitch.ts: setup OAuth2 exchange for Twitch login
- client.ts: setup static routes to serve the React client

## util/

Some utility modules. May phase this directory out.
