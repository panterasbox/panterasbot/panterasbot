import { ObjectType, Field } from "type-graphql";
import { Timestamped } from "..";
import { StuffTemplateModel } from "../../../model/StuffTemplate";
import { Constructor } from "../../../util/mixin";
import { Persistent, PersistableMixin } from "../Persistable";
import { Idea, IIdea } from "../stuff/Idea";

export interface IStuffTemplate
  extends IIdea,
    Timestamped,
    Persistent<IStuffTemplate> {
  name: string;
  domain: string;
  path: string;
  type: string;
  schemaPath?: string;
  template: any;
}

@ObjectType()
class _StuffTemplate extends Idea implements IStuffTemplate {
  constructor(...args: ConstructorParameters<typeof Idea>) {
    super(...args);
  }

  @Field({ nullable: true })
  _id?: string;

  @Field()
  name!: string;

  @Field()
  domain!: string;

  @Field()
  path!: string;

  @Field()
  type!: string;

  @Field({ nullable: true })
  schemaPath?: string;

  // @Field()
  template!: any;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: IStuffTemplate) {
    super.copyFrom(from);
    this.name = from.name;
    this.domain = from.domain;
    this.path = from.path;
    this.type = from.type;
    this.schemaPath = from.schemaPath;
    this.template = from.template;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableStuffTemplate = PersistableMixin<
  IStuffTemplate,
  Constructor<_StuffTemplate>,
  typeof StuffTemplateModel
>(_StuffTemplate, StuffTemplateModel);

@ObjectType()
export class StuffTemplate extends PersistableStuffTemplate {
  static async findByPath(path: string) {
    return (await StuffTemplate.findOne({ path })) as StuffTemplate;
  }
}

export type StuffTemplateType = InstanceType<typeof StuffTemplate>;
