import {
  CommandModel,
  ModelValue,
  MultiModelValue,
} from "../../../api/command";
import { Field, Validation } from "../command/CommandDefinition";

export type ValidationModel = Record<string, MultiModelValue<ModelValue>>;

export type FieldValidationFunction<M extends ModelValue, T = any> = (
  value: M,
  params: T
) => boolean;

export type ModelValidationFunction<M extends CommandModel, T = any> = (
  value: M,
  params: T
) => boolean;

export type ValidationFunction<
  M extends CommandModel,
  F extends ModelValue,
  T = any
> = FieldValidationFunction<F, T> | ModelValidationFunction<M, T>;

export class ValidationContext {
  fields: Record<string, Field>;
  valid: boolean = true;
  model: ValidationModel;
  invalid: Map<Validation, MultiModelValue<ModelValue> | null>;

  constructor(fields: Record<string, Field>) {
    this.fields = fields;
    this.invalid = new Map();
    this.model = {};
  }

  fail<T extends ModelValue>(
    validation: Validation,
    value: T | null,
    multiIndex: number = -1
  ) {
    const fieldRef = validation.field;
    if (validation.field.length) {
      if (!this.invalid.has(validation)) {
        this.invalid.set(validation, []);
      }
      const failures = this.invalid.get(validation) as T[];
      const model = this.model[fieldRef] as T[];
      const field = this.fields[fieldRef];

      if (field.type === "objects") {
        const objFailures = failures as Object[][];
        const objModel = model as Object[][];

        while (objFailures.length <= multiIndex) {
          objFailures.push([]);
        }
        objFailures[multiIndex].push(value as T);
        if (multiIndex in objModel) {
          objModel[multiIndex] = objModel[multiIndex].filter((v) => v != value);
        }
      } else {
        failures.push(value as T);
        if (model) {
          this.model[fieldRef] = model.filter((v) => v != value);
        }
      }
    } else {
      if (!this.invalid.has(validation)) {
        this.invalid.set(validation, null);
      }
    }
  }

  pass<T extends ModelValue>(
    validation: Validation,
    value: T | null,
    multiIndex: number = -1
  ) {
    const fieldRef = validation.field;
    const field = this.fields[fieldRef];
    if (validation.field.length) {
      if (!(fieldRef in this.model)) {
        this.model[fieldRef] = [];
      }
      const model = this.model[fieldRef] as T[];

      if (field.type === "objects") {
        const objModel = model as Object[][];
        while (objModel.length <= multiIndex) {
          objModel.push([]);
        }
        objModel[multiIndex].push(value as T);
      } else {
        model.push(value as T);
      }
    }
  }

  invalidate() {
    this.valid = false;
  }
}
