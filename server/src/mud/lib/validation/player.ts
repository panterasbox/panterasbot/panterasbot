import { Application } from "../../../backend/Application";
import { Avatar, AvatarType } from "../../obj/Avatar";

export function isPlayer(ob: Object): boolean {
  return ob instanceof Avatar;
}

export function isCommandGiver(ob: Object): boolean {
  const application = Application.fromContext();
  const context = application.api.command.getExecutionContext();
  return ob === context.controllers[0].context.commandGiver;
}

export interface NameParams {
  firstName?: string;
  lastName?: string;
}

export function isFirstName(avatar: AvatarType, params: NameParams): boolean {
  return avatar.player.firstName == params.firstName;
}

export function isLastName(avatar: AvatarType, params: NameParams): boolean {
  return avatar.player.lastName == params.lastName;
}

export function validName(name: string): boolean {
  return name.match(/^\w*$/) != null;
}

export function isPronoun(str: string): boolean {
  const playerApi = Application.fromContext().api.player;
  const pronouns = playerApi.toPronouns(str);
  return pronouns != null;
}
