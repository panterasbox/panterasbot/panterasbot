export interface LengthParams {
  min?: number;
  max?: number;
}

export function validLength(str: string, params: LengthParams): boolean {
  const len = str.length;
  if (params.max !== undefined && len > params.max) {
    return false;
  }
  if (params.min !== undefined && len < params.min) {
    return false;
  }
  return true;
}

export function isUrl(url: string): boolean {
  try {
    new URL(url);
    return true;
  } catch (e) {
    return false;
  }
}
