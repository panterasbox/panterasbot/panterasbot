import { StreamStatus } from "../../../api/stream";

export function isStatus(str: string): boolean {
  return Object.values(StreamStatus).includes(str as StreamStatus);
}
