import { Model, EnforceDocument, FilterQuery } from "mongoose";
import { ObjectTypeOptions, ObjectType } from "type-graphql";
import { Constructor, Mixin, Mixins } from "../../util/mixin";
import { Application } from "../../backend/Application";

export interface Persistent<I> {
  _id?: string;
  copyFrom(from: I): void;
}

export type PersistableConstraint = Persistent<any>;

function PersistableType<T extends Constructor<PersistableConstraint>>(
  PersistentType: T,
  options?: ObjectTypeOptions
) {
  const typeName = PersistentType.name;

  return (target: Function) => {
    ObjectType(`Persistable${typeName}`, options)(target);
  };
}

export interface Persistable<I> {
  save(): Promise<this>;
  restore(): Promise<this>;
}

export function PersistableMixin<
  I,
  T extends Constructor<Persistent<I>>,
  M extends Model<I>
>(Base: T, Model: M) {
  @PersistableType(Base)
  @Mixin(Mixins.Persistable)
  class BasePersistable extends Base implements Persistable<I> {
    static model = Model;
    doc?: EnforceDocument<I, {}> | null;

    static async findById(_id: string | undefined) {
      const doc = await BasePersistable.model.findById(_id).exec();
      if (!doc) {
        return null;
      }
      const application = Application.fromContext();
      const persistable = new this(application, doc);
      persistable.doc = doc;
      persistable._id = doc._id;
      return persistable;
    }

    static async findOne(filter?: FilterQuery<I>) {
      const doc = await BasePersistable.model.findOne(filter).exec();
      if (!doc) {
        return null;
      }
      const application = Application.fromContext();
      const persistable = new this(application, doc);
      persistable.doc = doc;
      persistable._id = doc._id;
      return persistable;
    }

    async save(): Promise<this> {
      if (!this.doc) {
        this.doc = new BasePersistable.model(this);
        const res = await this.doc.save();
      } else {
        this.doc.overwrite(this);
        await this.doc.save();
      }
      this._id = this.doc._id;
      return this;
    }

    async restore(): Promise<this> {
      const doc = await BasePersistable.model.findById(this._id).exec();
      if (!doc) {
        throw new Error(
          `No ${BasePersistable.model.name} found with _id ${this._id}`
        );
      }
      this.doc = doc;
      if (!this._id) {
        this._id = this.doc._id;
      } else if (this.doc._id != this._id) {
        throw new Error(
          `Won't overwrite object with different record, ` + this.doc._id
        );
      }
      this.copyFrom(this.doc);
      return this;
    }
  }

  return BasePersistable;
}
