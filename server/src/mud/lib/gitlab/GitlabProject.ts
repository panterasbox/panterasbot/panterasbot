import { ObjectType, Field } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { GitlabApiResult, GitlabId } from "./GitlabController";
import { camelize, checkedDate } from "../../../model";
import { GitlabProjectModel } from "../../../model/gitlab/GitlabProject";
import { Persistent, PersistableMixin } from "../Persistable";
import { Constructor } from "../../../util/mixin";

export interface IGitlabProject
  extends IIdea,
    Timestamped,
    Persistent<IGitlabProject> {
  externalId: GitlabId;
  description?: string;
  name?: string;
  nameWithNamespace?: string;
  path?: string;
  pathWithNamespace?: string;
  projectCreatedAt?: Date;
  defaultBranch?: string;
  tagList?: string[];
  sshUrlToRepo?: string;
  httpUrlToRepo?: string;
  webUrl?: string;
  readmeUrl?: string;
  avatarUrl?: string;
  forksCount?: number;
  starCount?: number;
  lastActivityAt?: Date;
  namespaceId: number;
  namespaceKind: string;
  _linksJson: string;
  visibility: string;
  creatorId: number;
  openIssuesCount: number;
}

@ObjectType()
export class _GitlabProject extends Idea implements IGitlabProject {
  @Field({ nullable: true })
  _id?: string;

  @Field()
  externalId!: GitlabId;

  @Field({ nullable: true })
  description?: string;

  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  nameWithNamespace?: string;

  @Field({ nullable: true })
  path?: string;

  @Field({ nullable: true })
  pathWithNamespace?: string;

  @Field({ nullable: true })
  projectCreatedAt?: Date;

  @Field({ nullable: true })
  defaultBranch?: string;

  @Field((type) => [String], { nullable: true })
  tagList?: string[];

  @Field({ nullable: true })
  sshUrlToRepo?: string;

  @Field({ nullable: true })
  httpUrlToRepo?: string;

  @Field({ nullable: true })
  webUrl?: string;

  @Field({ nullable: true })
  readmeUrl?: string;

  @Field({ nullable: true })
  avatarUrl?: string;

  @Field({ nullable: true })
  forksCount?: number;

  @Field({ nullable: true })
  starCount?: number;

  @Field({ nullable: true })
  lastActivityAt?: Date;

  @Field()
  namespaceId!: number;

  @Field()
  namespaceKind!: string;

  @Field()
  _linksJson!: string;

  @Field()
  visibility!: string;

  @Field()
  creatorId!: number;

  @Field()
  openIssuesCount!: number;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: IGitlabProject) {
    super.copyFrom(from);
    this.externalId = from.externalId;
    this.description = from.description;
    this.name = from.name;
    this.nameWithNamespace = from.nameWithNamespace;
    this.path = from.path;
    this.pathWithNamespace = from.pathWithNamespace;
    this.projectCreatedAt = from.projectCreatedAt;
    this.defaultBranch = from.defaultBranch;
    this.tagList = from.tagList;
    this.sshUrlToRepo = from.sshUrlToRepo;
    this.httpUrlToRepo = from.httpUrlToRepo;
    this.webUrl = from.webUrl;
    this.readmeUrl = from.readmeUrl;
    this.avatarUrl = from.avatarUrl;
    this.forksCount = from.forksCount;
    this.starCount = from.starCount;
    this.lastActivityAt = from.lastActivityAt;
    this.namespaceId = from.namespaceId;
    this.namespaceKind = from.namespaceKind;
    this._linksJson = from._linksJson;
    this.visibility = from.visibility;
    this.creatorId = from.creatorId;
    this.openIssuesCount = from.openIssuesCount;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableGitlabProject = PersistableMixin<
  IGitlabProject,
  Constructor<_GitlabProject>,
  typeof GitlabProjectModel
>(_GitlabProject, GitlabProjectModel);

@ObjectType()
export class GitlabProject extends PersistableGitlabProject {
  static async fromApi(result: GitlabApiResult): Promise<GitlabProject> {
    result = camelize(result);

    const props: Partial<IGitlabProject> = {
      externalId: result.id,
      description: result.description,
      name: result.name,
      nameWithNamespace: result.nameWithNamespace,
      path: result.path,
      pathWithNamespace: result.pathWithNamespace,
      projectCreatedAt: checkedDate(result.createdAt),
      defaultBranch: result.defaultBranch,
      tagList: result.tagList,
      sshUrlToRepo: result.sshUrlToRepo,
      httpUrlToRepo: result.httpUrlToRepo,
      webUrl: result.webUrl,
      readmeUrl: result.readmeUrl,
      avatarUrl: result.avatarUrl,
      forksCount: result.forksCount,
      starCount: result.starCount,
      lastActivityAt: checkedDate(result.lastActivityAt),
      namespaceId: result.namespace.id,
      namespaceKind: result.namespace.kind,
      _linksJson: JSON.stringify(result._links),
      visibility: result.visibility,
      creatorId: result.creatorId,
      openIssuesCount: result.openIssuesCount,
    };

    let gitlabProject = (await GitlabProject.findOne({
      externalId: result.id,
    })) as GitlabProject;
    if (!gitlabProject) {
      gitlabProject = new GitlabProject(props);
    } else {
      Object.assign(gitlabProject, props);
    }
    return gitlabProject;
  }
}

export type GitlabProjectType = InstanceType<typeof GitlabProject>;
