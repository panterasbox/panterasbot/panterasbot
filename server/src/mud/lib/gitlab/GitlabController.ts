import { Gitlab } from "@gitbeaker/node";
import { Application } from "../../../backend/Application";
import { ApplicationContextMixin } from "../ApplicationContext";
import { GitlabGroup } from "./GitlabGroup";
import { GitlabProject } from "./GitlabProject";

export type GitlabId = number;

export type GitlabApiResult = {
  id: GitlabId;
  [key: string]: any;
};

export interface GitlabProfile {
  apiResult: GitlabApiResult;
  accessToken: any;
  refreshToken: any;
}

const _GitlabController = ApplicationContextMixin(Object);

export class GitlabController extends _GitlabController {
  api: InstanceType<typeof Gitlab> | undefined;
  active: boolean = false;
  rootGroupId?: number;
  groups: Map<number, GitlabGroup>;
  projects: Map<number, GitlabProject>;
  activeGroupId?: number;
  activeProjectId?: number;

  constructor(application: Application) {
    super(application);
    this.groups = new Map();
    this.projects = new Map();
  }

  start(gitlabApi: InstanceType<typeof Gitlab> | undefined) {
    this.api = gitlabApi;
  }

  async setRootGroup(groupId: number): Promise<GitlabGroup | undefined> {
    this.rootGroupId = groupId;
    await this.refreshGroups();
    await this.refreshProjects();
    const result = this.groups.get(groupId);
    this.active = typeof result !== "undefined";
    return result;
  }

  async refreshGroups() {
    if (!this.rootGroupId) {
      throw new Error("Root group not set");
    }
    if (!this.api) {
      throw new Error("Gitlab API not available");
    }

    const rootGroup = await this.api.Groups.show(this.rootGroupId);
    if (!rootGroup) {
      throw new Error("Root group not found: " + this.rootGroupId);
    }

    let groups = await this.api.Groups.search(rootGroup.path);
    groups = groups ? (Array.isArray(groups) ? groups : [groups]) : [];
    const gitlabGroups: GitlabGroup[] = await Promise.all(
      groups.map(
        (group): Promise<GitlabGroup> => {
          return GitlabGroup.fromApi(group as GitlabApiResult);
        }
      )
    );
    gitlabGroups.forEach((gitlabGroup: GitlabGroup) => {
      this.groups.set(gitlabGroup.externalId, gitlabGroup);
    });

    await Promise.all(gitlabGroups.map((gg) => gg.save()));

    if (!this.activeGroupId) {
      this.activeGroupId = this.rootGroupId;
    }
  }

  async refreshProjects() {
    let projects = await Promise.all(
      Array.from(this.groups, ([groupId, gitlabGroup]) => {
        return this.api?.Groups.projects(gitlabGroup.externalId);
      })
    );
    if (!projects) {
      throw new Error("No projects found: " + this.rootGroupId);
    }

    const gitlabProjects: GitlabProject[] = await Promise.all(
      projects
        .flat()
        .filter((x) => x)
        .map(
          (project): Promise<GitlabProject> => {
            return GitlabProject.fromApi(project as GitlabApiResult);
          }
        )
    );
    gitlabProjects.forEach((gitlabProject: GitlabProject) => {
      this.projects.set(gitlabProject.externalId, gitlabProject);
    });

    await Promise.all(gitlabProjects.map((gp) => gp.save()));
  }
}
