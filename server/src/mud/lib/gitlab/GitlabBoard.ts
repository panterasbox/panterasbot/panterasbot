import { ObjectType, Field } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { GitlabApiResult, GitlabId } from "./GitlabController";
import { camelize } from "../../../model";
import { GitlabBoardModel } from "../../../model/gitlab/GitlabBoard";
import { Persistent, PersistableMixin } from "../Persistable";
import { Constructor } from "../../../util/mixin";

export interface IGitlabBoard
  extends IIdea,
    Timestamped,
    Persistent<IGitlabBoard> {
  externalId: GitlabId;
}

@ObjectType()
export class _GitlabBoard extends Idea implements IGitlabBoard {
  @Field({ nullable: true })
  _id?: string;

  @Field()
  externalId!: GitlabId;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: IGitlabBoard) {
    super.copyFrom(from);
    this.externalId = from.externalId;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableGitlabBoard = PersistableMixin<
  IGitlabBoard,
  Constructor<_GitlabBoard>,
  typeof GitlabBoardModel
>(_GitlabBoard, GitlabBoardModel);

@ObjectType()
export class GitlabBoard extends PersistableGitlabBoard {
  static async fromApi(result: GitlabApiResult): Promise<GitlabBoard> {
    result = camelize(result);

    const props: Partial<IGitlabBoard> = {
      externalId: result.id,
    };

    let gitlabBoard = (await GitlabBoard.findOne({
      externalId: result.id,
    })) as GitlabBoard;
    if (!gitlabBoard) {
      gitlabBoard = new GitlabBoard(props);
    } else {
      Object.assign(gitlabBoard, props);
    }
    return gitlabBoard;
  }
}

export type GitlabBoardType = InstanceType<typeof GitlabBoard>;
