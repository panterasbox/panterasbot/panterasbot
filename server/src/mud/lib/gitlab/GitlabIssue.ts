import { ObjectType, Field } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { GitlabApiResult, GitlabId } from "./GitlabController";
import { camelize } from "../../../model";
import { GitlabIssueModel } from "../../../model/gitlab/GitlabIssue";
import { Persistent, PersistableMixin } from "../Persistable";
import { Constructor } from "../../../util/mixin";

export interface IGitlabIssue
  extends IIdea,
    Timestamped,
    Persistent<IGitlabIssue> {
  externalId: GitlabId;
}

@ObjectType()
export class _GitlabIssue extends Idea implements IGitlabIssue {
  @Field({ nullable: true })
  _id?: string;

  @Field()
  externalId!: GitlabId;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: IGitlabIssue) {
    super.copyFrom(from);
    this.externalId = from.externalId;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableGitlabIssue = PersistableMixin<
  IGitlabIssue,
  Constructor<_GitlabIssue>,
  typeof GitlabIssueModel
>(_GitlabIssue, GitlabIssueModel);

@ObjectType()
export class GitlabIssue extends PersistableGitlabIssue {
  static async fromApi(result: GitlabApiResult): Promise<GitlabIssue> {
    result = camelize(result);

    const props: Partial<IGitlabIssue> = {
      externalId: result.id,
    };

    let gitlabIssue = (await GitlabIssue.findOne({
      externalId: result.id,
    })) as GitlabIssue;
    if (!gitlabIssue) {
      gitlabIssue = new GitlabIssue(props);
    } else {
      Object.assign(gitlabIssue, props);
    }
    return gitlabIssue;
  }
}

export type GitlabIssueType = InstanceType<typeof GitlabIssue>;
