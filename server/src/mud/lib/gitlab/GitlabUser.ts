import { ObjectType, Field } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { GitlabApiResult, GitlabId } from "./GitlabController";
import { camelize } from "../../../model";
import { GitlabUserModel } from "../../../model/gitlab/GitlabUser";
import { Persistent, PersistableMixin } from "../Persistable";
import { Constructor } from "../../../util/mixin";

export interface IGitlabUser
  extends IIdea,
    Timestamped,
    Persistent<IGitlabUser> {
  externalId: GitlabId;
  name: String;
  username?: String;
  accountCreatedAt?: Date;
  location?: String;
  publicEmail?: String;
  linkedin?: String;
  twitter?: String;
  websiteUrl?: String;
  email?: String;
  twoFactorEnabled?: Boolean;
  privateProfile?: Boolean;
  accessToken?: String;
  refreshToken?: String;
  authenticatedAt: Date;
}

@ObjectType()
export class _GitlabUser extends Idea implements IGitlabUser {
  @Field({ nullable: true })
  _id?: string;

  @Field()
  externalId!: GitlabId;

  @Field()
  name!: String;

  @Field({ nullable: true })
  username?: String;

  @Field({ nullable: true })
  accountCreatedAt?: Date;

  @Field({ nullable: true })
  location?: String;

  @Field({ nullable: true })
  publicEmail?: String;

  @Field({ nullable: true })
  linkedin?: String;

  @Field({ nullable: true })
  twitter?: String;

  @Field({ nullable: true })
  websiteUrl?: String;

  @Field({ nullable: true })
  email?: String;

  @Field({ nullable: true })
  twoFactorEnabled?: Boolean;

  @Field({ nullable: true })
  privateProfile?: Boolean;

  @Field({ nullable: true })
  accessToken?: String;

  @Field({ nullable: true })
  refreshToken?: String;

  @Field()
  authenticatedAt!: Date;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: IGitlabUser) {
    super.copyFrom(from);
    this.externalId = from.externalId;
    this.name = from.name;
    this.username = from.username;
    this.accountCreatedAt = from.accountCreatedAt;
    this.location = from.location;
    this.publicEmail = from.publicEmail;
    this.linkedin = from.linkedin;
    this.twitter = from.twitter;
    this.websiteUrl = from.websiteUrl;
    this.email = from.email;
    this.twoFactorEnabled = from.twoFactorEnabled;
    this.privateProfile = from.privateProfile;
    this.accessToken = from.accessToken;
    this.refreshToken = from.refreshToken;
    this.authenticatedAt = from.authenticatedAt;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableGitlabUser = PersistableMixin<
  IGitlabUser,
  Constructor<_GitlabUser>,
  typeof GitlabUserModel
>(_GitlabUser, GitlabUserModel);

@ObjectType()
export class GitlabUser extends PersistableGitlabUser {
  static async findByExternalId(externalId: GitlabId) {
    return (await GitlabUser.findOne({ externalId })) as GitlabUser;
  }

  static async fromApi(result: GitlabApiResult): Promise<GitlabUser> {
    result = camelize(result);

    const props: Partial<IGitlabUser> = {
      externalId: result.id,
      name: result.name,
      username: result.username,
      accountCreatedAt: result.accountCreatedAt,
      location: result.location,
      publicEmail: result.publicEmail,
      linkedin: result.linkedin,
      twitter: result.twitter,
      websiteUrl: result.websiteUrl,
      email: result.email,
      twoFactorEnabled: result.twoFactorEnabled,
      privateProfile: result.privateProfile,
    };

    let gitlabUser = await GitlabUser.findByExternalId(result.id);
    if (!gitlabUser) {
      gitlabUser = new GitlabUser(props);
    } else {
      Object.assign(gitlabUser, props);
    }
    return gitlabUser;
  }
}

export type GitlabUserType = InstanceType<typeof GitlabUser>;
