import { ObjectType, Field, Int } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { GitlabApiResult, GitlabId } from "./GitlabController";
import { camelize, checkedDate } from "../../../model";
import { GitlabGroupModel } from "../../../model/gitlab/GitlabGroup";
import { Persistent, PersistableMixin } from "../Persistable";
import { Constructor } from "../../../util/mixin";

export interface IGitlabGroup
  extends IIdea,
    Timestamped,
    Persistent<IGitlabGroup> {
  externalId: GitlabId;
  webUrl?: string;
  name?: string;
  path?: string;
  description?: string;
  visibility?: string;
  projectCreationLevel?: string;
  subgroupCreationLevel?: string;
  avatarUrl?: string;
  fullName?: string;
  fullPath?: string;
  groupCreatedAt?: Date;
  parentId: GitlabId | null;
}

@ObjectType()
export class _GitlabGroup extends Idea implements IGitlabGroup {
  @Field({ nullable: true })
  _id?: string;

  @Field()
  externalId!: GitlabId;

  @Field({ nullable: true })
  webUrl?: string;

  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  path?: string;

  @Field({ nullable: true })
  description?: string;

  @Field({ nullable: true })
  visibility?: string;

  @Field({ nullable: true })
  projectCreationLevel?: string;

  @Field({ nullable: true })
  subgroupCreationLevel?: string;

  @Field({ nullable: true })
  avatarUrl?: string;

  @Field({ nullable: true })
  fullName?: string;

  @Field({ nullable: true })
  fullPath?: string;

  @Field({ nullable: true })
  groupCreatedAt?: Date;

  @Field((type) => Int, { nullable: true })
  parentId: GitlabId | null = null;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: IGitlabGroup) {
    super.copyFrom(from);
    this.externalId = from.externalId;
    this.webUrl = from.webUrl;
    this.name = from.name;
    this.path = from.path;
    this.description = from.description;
    this.visibility = from.visibility;
    this.projectCreationLevel = from.projectCreationLevel;
    this.subgroupCreationLevel = from.subgroupCreationLevel;
    this.avatarUrl = from.avatarUrl;
    this.fullName = from.fullName;
    this.fullPath = from.fullPath;
    this.groupCreatedAt = from.groupCreatedAt;
    this.parentId = from.parentId;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableGitlabGroup = PersistableMixin<
  IGitlabGroup,
  Constructor<_GitlabGroup>,
  typeof GitlabGroupModel
>(_GitlabGroup, GitlabGroupModel);

@ObjectType()
export class GitlabGroup extends PersistableGitlabGroup {
  static async fromApi(result: GitlabApiResult): Promise<GitlabGroup> {
    result = camelize(result);
    const parentId = result.parentId ? result.parentId : null;

    const props: Partial<IGitlabGroup> = {
      externalId: result.id,
      webUrl: result.webUrl,
      name: result.name,
      path: result.path,
      description: result.description,
      visibility: result.visibility,
      projectCreationLevel: result.projectCreationLevel,
      subgroupCreationLevel: result.subgroupCreationLevel,
      avatarUrl: result.avatarUrl,
      fullName: result.fullName,
      fullPath: result.fullPath,
      groupCreatedAt: checkedDate(result.groupCreatedAt),
      parentId: parentId,
    };

    let gitlabGroup = (await GitlabGroup.findOne({
      externalId: result.id,
    })) as GitlabGroup;
    if (!gitlabGroup) {
      gitlabGroup = new GitlabGroup(props);
    } else {
      Object.assign(gitlabGroup, props);
    }
    return gitlabGroup;
  }
}

export type GitlabGroupType = InstanceType<typeof GitlabGroup>;
