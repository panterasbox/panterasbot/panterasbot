import callsites from "callsites";
import { ObjectTypeOptions, ObjectType } from "type-graphql";
import { Application } from "../../backend/Application";
import { Constructor, Mixin, Mixins } from "../../util/mixin";

export interface ApplicationContext {
  application: Application;
}

function ApplicationContextType<T extends Constructor<any>>(
  ContextualType: T,
  options?: ObjectTypeOptions
) {
  const typeName = ContextualType.name;

  return (target: Function) => {
    ObjectType(`ApplicationContext${typeName}`, options)(target);
  };
}

export function ApplicationContextMixin<T extends Constructor<any>>(Base: T) {
  @ApplicationContextType(Base)
  @Mixin(Mixins.ApplicationContext)
  abstract class BaseApplicationContext
    extends Base
    implements ApplicationContext {
    application: Application;

    constructor(...args: any[]) {
      let app = undefined;
      if (args[0] instanceof Application) {
        app = args[0];
        args = args.slice(1);
      }
      super(...args);

      if (!app) {
        app = Application.fromContext();
      }
      this.application = app;
      this.initApplicationContext();
    }

    protected initApplicationContext(): void {
      // nothing to do
    }
  }

  return BaseApplicationContext;
}
