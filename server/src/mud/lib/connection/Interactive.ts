import WebSocket from "ws";
import { SessionData } from "express-session";
import { AvatarType } from "../../obj/Avatar";
import { GitlabUser } from "../gitlab/GitlabUser";
import { TwitchUser } from "../twitch/TwitchUser";
import { PayloadAction } from "../../../backend/Backend";
import { CommandPayload } from "../../../api/command";
import { MessagePayload } from "../../../api/message";
import { Idea } from "../stuff/Idea";
import { saveSession } from "../../../util/sessionStore";
import { UserAgentPayload } from "../../../api/client";
import { WellKnownEvents } from "../../../api/event";
import { ConnectionEvent } from "../../obj/notify/ConnectionController";

export class Interactive extends Idea {
  websocket: WebSocket | undefined;
  session: SessionData;
  avatar?: AvatarType;
  userAgent?: string;
  twitchUser: TwitchUser | undefined;
  gitlabUser: GitlabUser | undefined;

  constructor(ws: WebSocket, session: SessionData) {
    super({});
    this.websocket = ws;
    this.session = session;
  }

  onConnect() {
    const messageApi = this.application.api.message;
    this.sendAction({
      type: "websocket/message",
      payload: {
        topic: "system.connect",
        message: messageApi.composeMessage(`Connected to Pantera's Bot`),
        params: {
          interactiveId: this.id,
        },
      },
    });
  }

  onDisconnect() {
    const messageApi = this.application.api.message;
    this.application.api.event.dispatch<ConnectionEvent>(
      WellKnownEvents.disconnect,
      {
        interactive: this,
        linkdead: true,
        event: WellKnownEvents.disconnect,
      }
    );
    if (this.avatar?.environment) {
      messageApi.messageContainer(
        this.avatar.environment,
        {
          topic: "sensor.connection",
          message: `${this.fullName} has disconnected.`,
        },
        [this.avatar]
      );
    }
  }

  async login(avatar: AvatarType) {
    const messageApi = this.application.api.message;

    let linkdead = this.avatar == avatar;
    if (this.avatar && this.avatar != avatar) {
      this.avatar.interactives.delete(this);
    }
    if (!this.avatar || this.avatar != avatar) {
      this.avatar = avatar;
      this.avatar.interactives.add(this);
    }

    if (!linkdead) {
      const result = await this.avatar.moveToStartLocation();
      if (result && this.avatar.environment) {
        messageApi.messageContainer(
          this.avatar.environment,
          {
            topic: "sensor.connection",
            message: `${this.fullName} has entered the game.`,
          },
          [this.avatar]
        );
      }
    } else {
      if (this.avatar.environment) {
        messageApi.messageContainer(
          this.avatar.environment,
          {
            topic: "sensor.connection",
            message: `${this.fullName} has reconnected.`,
          },
          [this.avatar]
        );
      }
    }

    this.application.api.event.dispatch<ConnectionEvent>(
      WellKnownEvents.connect,
      {
        interactive: this,
        linkdead,
        event: WellKnownEvents.connect,
      }
    );

    this.application.api.message.prompt(avatar);
  }

  logout() {
    const messageApi = this.application.api.message;

    if (this.avatar) {
      this.application.api.event.dispatch<ConnectionEvent>(
        WellKnownEvents.disconnect,
        {
          interactive: this,
          linkdead: false,
          event: WellKnownEvents.disconnect,
        }
      );

      this.avatar.interactives.delete(this);
      // XXX dest avatar

      if (this.avatar?.environment) {
        messageApi.messageContainer(
          this.avatar.environment,
          {
            topic: "sensor.connection",
            message: `${this.fullName} has logged out.`,
          },
          [this.avatar]
        );
      }
    }
    this.session.twitchId = undefined;
    saveSession(this.session);
    const guest = this.application.newGuestAvatar();
    this.login(guest);
  }

  onAction(action: PayloadAction<any>) {
    switch (action.type) {
      case "backend/doCommand":
        this.onCommandAction(action);
        break;
      case "client/userAgent":
        this.onUserAgent(action);
        break;
    }
  }

  onUserAgent(action: PayloadAction<UserAgentPayload>) {
    this.userAgent = action.payload;
  }

  async onCommandAction(action: PayloadAction<CommandPayload>) {
    if (!this.avatar) {
      throw new Error("Interactives cannot execute commands without avatar");
    }
    if (action.payload.command == "logout") {
      this.logout();
    } else {
      try {
        await this.avatar.doCommand(action.payload);
      } catch (e) {
        console.error("Caught exception running command", e);
      }
    }
  }

  async sendAction(action: PayloadAction<any>): Promise<boolean> {
    if (this.websocket) {
      return this.application.sendMessage(this.websocket, action);
    }
    return false;
  }

  async sendMessageAction(
    action: PayloadAction<MessagePayload>
  ): Promise<boolean> {
    return await this.sendAction(action);
  }
}
