import { ObjectType, Field } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { Persistent, PersistableMixin } from "../Persistable";
import { PlayerModel } from "../../../model/Player";
import { Constructor } from "../../../util/mixin";
import { User } from "./User";
import { Pronouns } from "../character/Gendered";
import { PropValue } from "../Propertied";

export interface IPlayer extends IIdea, Timestamped, Persistent<IPlayer> {
  userId: string;
  firstName?: string;
  lastName?: string;
  pronouns: Pronouns;
  avatar?: string;
  user: User;
  savedProps: Record<string, PropValue>;
}

@ObjectType()
class _Player extends Idea implements IPlayer {
  constructor(...args: ConstructorParameters<typeof Idea>) {
    super(...args);
    this.savedProps = {};
  }

  @Field({ nullable: true })
  _id?: string;

  @Field()
  userId!: string;

  @Field({ nullable: true })
  firstName?: string;

  @Field({ nullable: true })
  lastName?: string;

  @Field((type) => Pronouns)
  pronouns: Pronouns = Pronouns.They;

  @Field({ nullable: true })
  avatar?: string;

  @Field((type) => User)
  user!: User;

  savedProps: Record<string, PropValue>;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: IPlayer) {
    super.copyFrom(from);
    this.user = from.user;
    this.userId = from.userId;
    this.firstName = from.firstName;
    this.lastName = from.lastName;
    this.pronouns = from.pronouns;
    this.avatar = from.avatar;
    this.savedProps = from.savedProps;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistablePlayer = PersistableMixin<
  IPlayer,
  Constructor<_Player>,
  typeof PlayerModel
>(_Player, PlayerModel);

@ObjectType()
export class Player extends PersistablePlayer {
  static async findByUser(userId: string) {
    return (await Player.findOne({ userId })) as Player;
  }
}
export type PlayerType = InstanceType<typeof Player>;
