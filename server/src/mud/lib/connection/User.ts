import { ObjectType, Field } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { Persistent, PersistableMixin } from "../Persistable";
import { UserModel } from "../../../model/User";
import { Constructor } from "../../../util/mixin";

export interface IUser extends IIdea, Timestamped, Persistent<IUser> {
  twitchUserId: string;
  gitlabUserId?: string;
  username?: string;
  email?: string;
  avatar?: string;
  isGuest: boolean;
  isStreamer: boolean;
  isFollower: boolean;
  subscriberTier: number;
}

@ObjectType()
export class _User extends Idea implements IUser {
  @Field({ nullable: true })
  _id?: string;

  @Field()
  twitchUserId!: string;

  @Field({ nullable: true })
  gitlabUserId?: string;

  @Field({ nullable: true })
  username?: string;

  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  avatar?: string;

  @Field()
  isGuest!: boolean;

  @Field()
  isStreamer!: boolean;

  @Field()
  isFollower!: boolean;

  @Field()
  subscriberTier!: number;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: IUser) {
    super.copyFrom(from);
    this.twitchUserId = from.twitchUserId;
    this.gitlabUserId = from.gitlabUserId;
    this.username = from.username;
    this.email = from.email;
    this.avatar = from.avatar;
    this.isGuest = from.isGuest;
    this.isStreamer = from.isStreamer;
    this.isFollower = from.isFollower;
    this.subscriberTier = from.subscriberTier;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableUser = PersistableMixin<
  IUser,
  Constructor<_User>,
  typeof UserModel
>(_User, UserModel);

@ObjectType()
export class User extends PersistableUser {
  static async findTwitchUser(twitchUserId: string) {
    return (await User.findOne({ twitchUserId })) as User;
  }
}

export type UserType = InstanceType<typeof User>;
