import { MessagePayload } from "../../../api/message";
import { Constructor, Mixin, Mixins } from "../../../util/mixin";
// import { _Avatar } from "../../obj/Avatar";

export interface CommandShell {
  onEcho(command: string, prompt: string): void;
  onPrompt(prompt: string): void;
  prompt: string | (() => string);
}

export type CommandShellConstraint = any;

export function CommandShellMixin<
  T extends Constructor<CommandShellConstraint>
>(Base: T) {
  @Mixin(Mixins.CommandShell)
  class BaseCommandShell extends Base implements CommandShell {
    prompt: string | (() => string) = ">";

    constructor(...args: any[]) {
      super(...args);
    }

    onEcho(command: string, prompt: string): void {
      for (const interactive of this.interactives) {
        try {
          interactive.sendAction({
            type: "websocket/echo",
            payload: `${prompt} ${command}`,
          });
        } catch (e) {
          console.warn("Caught error sending local echo", e);
        }
      }
    }

    onPrompt(prompt: string): void {
      for (const interactive of this.interactives) {
        try {
          interactive.sendAction({
            type: "websocket/prompt",
            payload: prompt,
          });
        } catch (e) {
          console.warn("Caught error sending prompt", e);
        }
      }
    }
  }

  return BaseCommandShell;
}
