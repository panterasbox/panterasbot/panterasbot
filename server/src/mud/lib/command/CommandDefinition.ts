import { Field, ObjectType } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { PersistableMixin, Persistent } from "../Persistable";
import { Constructor } from "../../../util/mixin";
import { CommandDefinitionModel } from "../../../model/CommandDefinition";

export type BaseCommand = {
  validation: Validation[];
  fields?: Record<string, Field>;
  opts?: Option[];
  syntax: Syntax[];
};

export type Command = BaseCommand & {
  verbs: string[];
  controller: string;
  subcommands?: Record<string, Subcommand>;
};

export type Validation<T = any> = {
  field: string;
  method: string;
  pass: "any" | "all" | "none";
  fail: string;
  params: T;
};

export type Field = {
  id: string;
  type: "boolean" | "number" | "string" | "object" | "objects";
  required: boolean;
  default?: string;
};

export type Subcommand = BaseCommand & {
  verb: string;
  controller?: string;
};

export type Syntax = {
  minArgs?: number;
  maxArgs?: number;
  opts?: (Option | OptionRef)[];
  args?: Arg[];
};

export type Option = {
  id?: string;
  opt?: string;
  longopt?: string;
  param: "none" | "single" | "multi";
  fieldRef: string;
};

export type OptionRef = string;

export type Arg = {
  fieldRef: string;
  multi: boolean;
};

class CommandImpl implements Command {
  validation: Validation<any>[] = [];
  fields?: Record<string, Field>;
  opts?: Option[];
  syntax!: Syntax[];
  verbs!: string[];
  controller!: string;
  subcommands?: Record<string, Subcommand>;
}

export interface ICommandDefinition
  extends IIdea,
    Persistent<ICommandDefinition> {
  id: string;
  path: string;
  command: Command;
  createdAt: Date;
  updatedAt: Date;
}

@ObjectType("CommandDefinition")
export class _CommandDefinition extends Idea implements ICommandDefinition {
  @Field()
  path!: string;

  @Field((type) => CommandImpl)
  command!: Command;

  @Field()
  createdAt!: Date;

  @Field()
  updatedAt!: Date;

  copyFrom(from: ICommandDefinition) {
    super.copyFrom(from);
    this.path = from.path;
    this.command = from.command;
  }
}

const PersistableCommandDefinition = PersistableMixin<
  ICommandDefinition,
  Constructor<_CommandDefinition>,
  typeof CommandDefinitionModel
>(_CommandDefinition, CommandDefinitionModel);

@ObjectType()
export class CommandDefinition extends PersistableCommandDefinition {
  static async findByPath(path: string) {
    return (await CommandDefinition.findOne({ path })) as CommandDefinition;
  }
}

export type CommandDefinitionType = InstanceType<typeof CommandDefinition>;
