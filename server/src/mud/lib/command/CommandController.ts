import {
  CommandContext,
  CommandModel,
  ModelValue,
  MultiModelValue,
} from "../../../api/command";
import { ParserContext } from "../../../api/command-line";
import { ApplicationContextMixin } from "../ApplicationContext";
import {
  FieldValidationFunction,
  ModelValidationFunction,
  ValidationContext,
  ValidationModel,
} from "../validation";
import { Command, CommandDefinition } from "./CommandDefinition";

const _CommandController = ApplicationContextMixin(Object);

export abstract class CommandController<
  I extends CommandModel,
  O extends CommandModel
> extends _CommandController {
  context: CommandContext<I, O>;

  constructor(...args: any[]) {
    super(...args);
    this.context = {
      commandGiver: args[1],
      terminate: false,
    };
  }

  parseCommand(
    def: CommandDefinition,
    verb: string,
    arg: string
  ): CommandContext<I, O> {
    const commandLineApi = this.application.api.commandLine;
    const parser = commandLineApi.parseCommand(def.command, verb, arg);

    this.context = {
      ...this.context,
      commandDef: def.path,
      command: def.command,
      verb,
      arg,
      parser,
      subcommand: parser.subcommandIndex,
    };
    return this.context;
  }

  async doCommand(
    context: CommandContext<I, O>
  ): Promise<CommandContext<I, O>> {
    this.context = context;
    try {
      return await this._doCommand();
    } catch (e) {
      this.application.api.mudlog.error(`Caught error running command: ${e}`, {
        error: e,
      });
      throw e;
    }
  }

  async _doCommand(): Promise<CommandContext<I, O>> {
    if (!this.context.parser) {
      throw new Error("Command not parsed");
    }

    this.context.validation = await this.doValidate(this.context);

    if (!this.context.validation.valid) {
      this.context.terminate = await this.doFail(this.context);
    } else {
      this.context.in = await this.doModel(this.context.validation.model);
      this.context.out = await this.execute(
        this.context.in,
        this.context.subcommand
      );
      this.context.terminate = true;
    }
    return this.context;
  }

  async doValidate(context: CommandContext): Promise<ValidationContext> {
    const commandApi = this.application.api.command;

    const model = commandApi.resolveModel<I>(context);

    const fields = commandApi.resolveFields(
      context.command as Command,
      context.parser as ParserContext
    );
    const validations = commandApi.resolveValidations(context);
    const validationCtx = new ValidationContext(fields);

    const validated = new Set<string>();
    for (const validation of validations) {
      const fieldRef = validation.field;
      const field = fields[fieldRef];
      validated.add(fieldRef);

      if (fieldRef.length) {
        await (async <T extends ModelValue, P>() => {
          const validate = (await commandApi.resolveValidation<I, P>(
            validation
          )) as FieldValidationFunction<T, P>;

          const values = (context.parser?.multi[fieldRef]
            ? model[fieldRef as keyof I]
            : model[fieldRef as keyof I] !== undefined
            ? [model[fieldRef as keyof I]]
            : []) as MultiModelValue<T>;

          const params = commandApi.resolveParams<P, I>(
            validation.params,
            model
          );

          const _doValidate = async (
            value: T,
            multiIndex: number
          ): Promise<void> => {
            const result = await Promise.resolve(validate(value, params));
            switch (validation.pass) {
              case "none":
                if (result) {
                  validationCtx.fail(validation, value, multiIndex);
                  validationCtx.invalidate();
                } else {
                  validationCtx.pass(validation, value, multiIndex);
                }
                break;
              case "any":
                if (!result) {
                  validationCtx.fail(validation, value, multiIndex);
                } else {
                  validationCtx.pass(validation, value, multiIndex);
                }
                break;
              case "all":
              default:
                if (!result) {
                  validationCtx.fail(validation, value, multiIndex);
                  validationCtx.invalidate();
                } else {
                  validationCtx.pass(validation, value, multiIndex);
                }
                break;
            }
          };

          // XXX we can do this in parallel
          for (let multiIndex = 0; multiIndex < values.length; multiIndex++) {
            if (field.type === "objects") {
              const valueList = (values as Object[][])[multiIndex];
              for (const value of valueList) {
                await _doValidate(value as T, multiIndex);
              }
            } else {
              const value = values[multiIndex];
              await _doValidate(value, multiIndex);
            }
          }
        })();
      } else {
        await (async <P>() => {
          const validate = (await commandApi.resolveValidation<I, P>(
            validation
          )) as ModelValidationFunction<I, P>;

          const params = commandApi.resolveParams<P, I>(
            validation.params,
            model
          );

          const result = await Promise.resolve(validate(model, params));

          if (!result) {
            validationCtx.fail(validation, null);
            validationCtx.invalidate();
          }
        })();
      }
    }

    // add implicitly valid fields to model
    for (const fieldRef of Object.keys(fields)) {
      if (!(fieldRef in validationCtx.model) && fieldRef in model) {
        const values = (context.parser?.multi[fieldRef]
          ? model[fieldRef as keyof I]
          : model[fieldRef as keyof I] !== undefined
          ? [model[fieldRef as keyof I]]
          : []) as MultiModelValue<ModelValue>;
        validationCtx.model[fieldRef] = values;
      }
    }

    return validationCtx;
  }

  async doModel(model: ValidationModel): Promise<I> {
    const commandApi = this.application.api.command;
    return commandApi.collapseModel<I>(model, this.context);
  }

  async doFail(context: CommandContext): Promise<boolean> {
    if (context.validation?.invalid) {
      for (const validation of context.validation.invalid.keys()) {
        const value = context.validation.invalid.get(validation);
        this.application.api.mudlog.error(validation.fail, {
          field: validation.field,
          value,
        });
      }
    }
    return true;
  }

  abstract execute(model: I, subcommand: string | undefined): Promise<O>;
}
