import YAML from "yaml";
import { CommandProvider } from "..";
import { CommandContext, CommandPayload } from "../../../api/command";
import { Application } from "../../../backend/Application";
import { Constructor, hasMixin, Mixin, Mixins } from "../../../util/mixin";
import { CommandShell } from "../connection/CommandShell";
import { Agent } from "../stuff/Agent";
import { CommandController } from "./CommandController";
import { CommandDefinition } from "./CommandDefinition";

export const CONTROLLER_PATH = "/obj/command/";
export const COMMAND_DEF_PATH = "/cmd/";

export enum CommandSource {
  Colocated = "colocated",
  Inventory = "inventory",
  Environment = "environment",
  Self = "self",
}
export type CommandDefMap = Map<string, CommandDefinition>;
export type WeakProviderMap = WeakMap<
  CommandProvider,
  WeakRef<CommandProvider>
>;
export type ProviderMap = Map<WeakRef<CommandProvider>, CommandDefMap>;
export type CommandInfo = [string, CommandDefinition];

export interface CommandGiver {
  addCommand(
    commandSource: CommandSource,
    commandProvider: CommandProvider,
    path: string
  ): Promise<boolean>;
  removeCommand(
    commandSource: CommandSource,
    commandProvider: CommandProvider,
    path: string
  ): boolean;
  removeAllCommands(
    commandSource: CommandSource,
    commandProvider: CommandProvider
  ): string[];
  doCommand(payload: CommandPayload): Promise<CommandContext | undefined>;
}

export type CommandGiverConstraint = CommandProvider & Agent;

export function CommandGiverMixin<
  T extends Constructor<CommandGiverConstraint>
>(Base: T) {
  @Mixin(Mixins.CommandGiver)
  class BaseCommandGiver extends Base implements CommandGiver {
    commandDefs: Map<CommandSource, [WeakProviderMap, ProviderMap]> = new Map();
    initPromise: Promise<void>;

    constructor(...args: any[]) {
      super(...args);
      this.initPromise = this.loadSelfCommands();
    }

    async addCommand(
      commandSource: CommandSource,
      commandProvider: CommandProvider,
      path: string
    ): Promise<boolean> {
      if (!this.commandDefs.has(commandSource)) {
        this.commandDefs.set(commandSource, [new WeakMap(), new Map()]);
      }
      const [providerWeakMap, weakProviderMap] = this.commandDefs.get(
        commandSource
      ) as [WeakProviderMap, ProviderMap];
      try {
        const [commandPath, commandDef] = await this.loadCommandDef(path);
        if (!providerWeakMap.has(commandProvider)) {
          const providerRef = new WeakRef<CommandProvider>(commandProvider);
          providerWeakMap.set(commandProvider, providerRef);
          weakProviderMap.set(providerRef, new Map());
        }
        const providerRef = providerWeakMap.get(
          commandProvider
        ) as WeakRef<CommandProvider>;
        const commandDefMap = weakProviderMap.get(providerRef) as CommandDefMap;
        commandDefMap.set(commandPath, commandDef);
        return true;
      } catch (err) {
        console.warn("Caught error loading command def", path);
        return false;
      }
    }

    removeCommand(
      commandSource: CommandSource,
      commandProvider: CommandProvider,
      path: string
    ): boolean {
      const [providerWeakMap, weakProviderMap] =
        this.commandDefs.get(commandSource) || [];
      if (!providerWeakMap || !weakProviderMap) {
        return false;
      }
      if (!providerWeakMap.has(commandProvider)) {
        return false;
      }
      const providerRef = providerWeakMap.get(
        commandProvider
      ) as WeakRef<CommandProvider>;
      const commandDefMap = weakProviderMap.get(providerRef);
      if (!commandDefMap) {
        return false;
      }
      if (!commandDefMap.has(path)) {
        return false;
      }
      commandDefMap.delete(path);
      return true;
    }

    removeAllCommands(
      commandSource: CommandSource,
      commandProvider: CommandProvider
    ): string[] {
      const [providerWeakMap, weakProviderMap] =
        this.commandDefs.get(commandSource) || [];
      if (!providerWeakMap || !weakProviderMap) {
        return [];
      }
      if (!providerWeakMap.has(commandProvider)) {
        return [];
      }
      const providerRef = providerWeakMap.get(
        commandProvider
      ) as WeakRef<CommandProvider>;
      const commandDefMap = weakProviderMap.get(providerRef);
      const result = Array.from(commandDefMap?.keys() || []);
      weakProviderMap.delete(providerRef);
      providerWeakMap.delete(commandProvider);
      return result;
    }

    async loadSelfCommands(): Promise<void> {
      const defs = await Promise.all(
        this.exportSelfCommands(this).map((spec) => {
          return this.addCommand(CommandSource.Self, this, spec);
        })
      );
    }

    async doCommand(
      payload: CommandPayload
    ): Promise<CommandContext | undefined> {
      let fragments: { [index: number]: string } = {}; // len to fragment
      let matched: { [index: string]: string | undefined } = {}; // verb to arg
      let result: CommandContext | undefined = undefined;

      await this.initPromise;

      for (const commandSource of Object.values(CommandSource)) {
        const [providerWeakMap, weakProviderMap] =
          this.commandDefs.get(commandSource) || [];
        if (!providerWeakMap || !weakProviderMap) {
          continue;
        }
        for (const [providerRef, commandDefMap] of weakProviderMap) {
          for (const [path, commandDef] of commandDefMap) {
            for (const verb of commandDef.command.verbs) {
              if (verb in matched) {
                if (matched[verb]) {
                  result = await this._doCommand(
                    commandDef,
                    verb,
                    matched[verb] as string,
                    payload.noEcho || false
                  );
                  if (result?.terminate) {
                    break;
                  }
                } else {
                  continue;
                }
              }
              if (verb.length > payload.command.length) {
                continue;
              }
              if (!(verb.length in fragments)) {
                fragments[verb.length] = payload.command.substring(
                  0,
                  verb.length
                );
              }
              if (fragments[verb.length] == verb) {
                if (verb.length == payload.command.length) {
                  matched[verb] = "";
                } else if (
                  payload.command.substring(verb.length, verb.length + 1) == " "
                ) {
                  // trim leading spaces
                  let i = verb.length;
                  while (payload.command.substring(i, i + 1) == " ") {
                    i++;
                  }
                  // set the match
                  if (i < payload.command.length) {
                    matched[verb] = payload.command.substring(i);
                  } else {
                    matched[verb] = "";
                  }
                } else {
                  matched[verb] = undefined;
                }
              } else {
                matched[verb] = undefined;
              }

              if (verb in matched && typeof matched[verb] === "string") {
                result = await this._doCommand(
                  commandDef,
                  verb,
                  matched[verb] as string,
                  payload.noEcho || false
                );
                if (result?.terminate) {
                  break;
                }
              }
            }
            if (result?.terminate) {
              break;
            }
          }
          if (result?.terminate) {
            break;
          }
        }
        if (result?.terminate) {
          break;
        }
      }

      const application = Application.fromContext();

      if (!result) {
        if (!payload.noEcho) {
          this.doEcho(payload.command);
        }
        application.api.mudlog.info("Huh?", {}, result, this);
      }

      if (hasMixin(this, Mixins.CommandShell)) {
        application.api.message.prompt((this as unknown) as CommandShell);
      }
      return result;
    }

    async _doCommand(
      def: CommandDefinition,
      verb: string,
      arg: string,
      noEcho: boolean = false
    ): Promise<CommandContext | undefined> {
      if (!noEcho) {
        this.doEcho(`${verb} ${arg}`);
      }

      const controller = await this.loadController(def.command.controller);
      if (controller) {
        const context = controller.parseCommand(def, verb, arg);
        let subController = controller;
        if (
          context.subcommand &&
          def.command?.subcommands &&
          def.command.subcommands[context.subcommand].controller
        ) {
          subController = await this.loadController(
            def.command.subcommands[context.subcommand].controller as string
          );
        }

        return await subController.doCommand(context);
      }
      return undefined;
    }

    async loadController(
      controllerPath: string
    ): Promise<CommandController<any, any>> {
      const application = Application.fromContext();
      const fileApi = application.api.file;
      const moduleApi = application.api.module;
      const objectApi = application.api.object;

      controllerPath = fileApi.resolvePath(controllerPath, CONTROLLER_PATH);
      controllerPath = moduleApi.resolve(controllerPath);
      const controller = await objectApi.clone(
        controllerPath,
        application,
        this // XXX implicit cast to Stuff, should check first?
      );
      return controller as CommandController<any, any>;
    }

    async loadCommandDef(path: string): Promise<[string, CommandDefinition]> {
      const fileApi = Application.fromContext().api.file;
      path = fileApi.resolvePath(path, COMMAND_DEF_PATH);
      const file = fileApi.readFileSync(`${path}.yaml`);
      let commandDef = await CommandDefinition.findByPath(path);
      // TODO validate yaml against schema
      if (commandDef) {
        commandDef.command = YAML.parse(file);
      } else {
        commandDef = new CommandDefinition({
          path: path,
          command: YAML.parse(file),
        });
      }
      await commandDef.save();
      return [path, commandDef];
    }

    doEcho(command: string): boolean {
      if (hasMixin(this, Mixins.CommandShell)) {
        const application = Application.fromContext();
        application.api.message.echo(
          (this as unknown) as CommandShell,
          command
        );
        return true;
      }
      return false;
    }
  }

  return BaseCommandGiver;
}
