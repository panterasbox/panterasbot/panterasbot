import { HelixGame } from "twitch/lib";
import { ObjectType, Field } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { TwitchId } from "./TwitchController";
import { Persistent, PersistableMixin } from "../Persistable";
import { TwitchGameModel } from "../../../model/twitch/TwitchGame";
import { Constructor } from "../../../util/mixin";

export interface ITwitchGame
  extends IIdea,
    Timestamped,
    Persistent<ITwitchGame> {
  externalId: TwitchId;
  name: string;
  boxArtUrl?: string;
}

@ObjectType()
export class _TwitchGame extends Idea implements ITwitchGame {
  @Field({ nullable: true })
  _id?: string;

  @Field()
  externalId!: TwitchId;

  @Field()
  name!: string;

  @Field({ nullable: true })
  boxArtUrl?: string;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: ITwitchGame) {
    super.copyFrom(from);
    this.externalId = from.externalId;
    this.name = from.name;
    this.boxArtUrl = from.boxArtUrl;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableTwitchGame = PersistableMixin<
  ITwitchGame,
  Constructor<_TwitchGame>,
  typeof TwitchGameModel
>(_TwitchGame, TwitchGameModel);

@ObjectType()
export class TwitchGame extends PersistableTwitchGame {
  static async fromApi(result: HelixGame): Promise<TwitchGameType> {
    const props: Partial<ITwitchGame> = {
      externalId: result.id,
      name: result.name,
      boxArtUrl: result.boxArtUrl,
    };

    let twitchGame = (await TwitchGame.findOne({
      externalId: result.id,
    })) as TwitchGame;
    if (!twitchGame) {
      twitchGame = new TwitchGame(props);
    } else {
      Object.assign(twitchGame, props);
    }
    return twitchGame;
  }
}

export type TwitchGameType = InstanceType<typeof TwitchGame>;
