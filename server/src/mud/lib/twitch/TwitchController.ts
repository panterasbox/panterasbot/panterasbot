import { ApiClient } from "twitch/lib";
import { Application } from "../../../backend/Application";
import { ApplicationContextMixin } from "../ApplicationContext";
import { TwitchChannel } from "./TwitchChannel";
import { TwitchGame } from "./TwitchGame";
import { TwitchStream } from "./TwitchStream";
import { TwitchUser } from "./TwitchUser";

export type TwitchId = string;

export type TwitchApiResult = {
  id: TwitchId;
  [key: string]: any;
};

export interface TwitchProfile {
  id: TwitchId;
  email: string;
  accessToken: any;
  refreshToken: any;
}

const _TwitchController = ApplicationContextMixin(Object);

export class TwitchController extends _TwitchController {
  api: ApiClient | undefined;
  active: boolean = false;
  user?: TwitchUser;
  channel?: TwitchChannel;
  stream?: TwitchStream;
  game?: TwitchGame;
  streaming: NodeJS.Timeout | undefined;

  constructor(application: Application) {
    super(application);
  }

  start(twitchApi: ApiClient | undefined) {
    this.api = twitchApi;
  }

  async setChannel(channelName: string): Promise<TwitchChannel> {
    const api = this.api?.helix;
    if (!api) {
      throw new Error("Twitch API not available");
    }

    const user = await api.users.getUserByName(channelName);
    if (!user) {
      throw new Error("User not found: " + channelName);
    }
    const twitchUser = await TwitchUser.fromApi(user);
    await twitchUser.save();
    this.user = twitchUser;

    const channel = await api.channels.getChannelInfo(twitchUser.externalId);
    if (!channel) {
      throw new Error("Channel not found: " + twitchUser.externalId);
    }

    const twitchChannel = await TwitchChannel.fromApi(channel);
    await twitchChannel.save();
    this.channel = twitchChannel;

    this.active = typeof this.channel !== "undefined";

    if (twitchChannel) {
      const game = await api.games.getGameById(twitchChannel.gameId);
      if (!game) {
        throw new Error("Game not found: " + twitchChannel.gameId);
      }
      const twitchGame = await TwitchGame.fromApi(game);
      await twitchGame.save();
      this.game = twitchGame;
    }

    return twitchChannel;
  }

  isStreamer(user: TwitchUser) {
    return this.channel?.externalId == user.externalId;
  }

  setStreaming(streaming: boolean) {
    if (streaming) {
      this.streaming = setInterval(() => {
        this.refreshStream().catch((e) => {
          console.warn("Caught error refreshing stream", e);
        });
      }, 15 * 1000);
    } else {
      this.streaming && clearInterval(this.streaming);
      this.streaming = undefined;
    }
  }

  isStreaming(): boolean {
    return this.streaming !== undefined;
  }

  async refreshStream(): Promise<TwitchStream | undefined> {
    const api = this.api?.helix;
    if (!api) {
      throw new Error("Twitch API not available");
    }

    if (!this.channel) {
      throw new Error("Twitch channel not set");
    }

    const stream = await api.streams.getStreamByUserId(this.channel.externalId);
    if (!stream) {
      this.stream = undefined;
      return undefined;
    }

    const twitchStream = await TwitchStream.fromApi(stream);
    await twitchStream.save();

    const viewers = this.stream?.viewerCount;
    this.stream = twitchStream;
    if (
      typeof this.stream.viewerCount === "number" &&
      viewers !== this.stream.viewerCount
    ) {
      this.application.api.message.messageAll(
        this.application.api.player.findAvatars(),
        {
          topic: "system.viewerChange",
          message: `Viewer count changed from ${viewers} to ${this.stream.viewerCount}`,
          params: {
            viewers: this.stream.viewerCount,
            previous: viewers,
          },
        }
      );
    }

    return twitchStream;
  }
}
