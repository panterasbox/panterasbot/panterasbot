import { HelixUser } from "twitch/lib";
import { ObjectType, Field } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { TwitchId } from "./TwitchController";
import { Persistent, PersistableMixin } from "../Persistable";
import { TwitchUserModel } from "../../../model/twitch/TwitchUser";
import { Constructor } from "../../../util/mixin";

export interface ITwitchUser
  extends IIdea,
    Timestamped,
    Persistent<ITwitchUser> {
  externalId: TwitchId;
  name: string;
  displayName: string;
  type?: string;
  broadcasterType?: string;
  description?: string;
  profilePictureUrl?: string;
  offlinePlaceholderUrl?: string;
  views?: number;
  email?: string;
  accessToken?: string;
  refreshToken?: string;
  authenticatedAt?: Date;
}

@ObjectType()
export class _TwitchUser extends Idea implements ITwitchUser {
  @Field({ nullable: true })
  _id?: string;

  @Field()
  externalId!: TwitchId;

  @Field()
  name!: string;

  @Field()
  displayName!: string;

  @Field({ nullable: true })
  type?: string;

  @Field({ nullable: true })
  broadcasterType?: string;

  @Field({ nullable: true })
  description?: string;

  @Field({ nullable: true })
  profilePictureUrl?: string;

  @Field({ nullable: true })
  offlinePlaceholderUrl?: string;

  @Field({ nullable: true })
  views?: number;

  @Field({ nullable: true })
  email?: string;

  @Field({ nullable: true })
  accessToken?: string;

  @Field({ nullable: true })
  refreshToken?: string;

  @Field()
  authenticatedAt?: Date;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: ITwitchUser) {
    super.copyFrom(from);
    this.externalId = from.externalId;
    this.name = from.name;
    this.displayName = from.displayName;
    this.type = from.type;
    this.broadcasterType = from.broadcasterType;
    this.description = from.description;
    this.profilePictureUrl = from.profilePictureUrl;
    this.offlinePlaceholderUrl = from.offlinePlaceholderUrl;
    this.views = from.views;
    this.email = from.email;
    this.accessToken = from.accessToken;
    this.refreshToken = from.refreshToken;
    this.authenticatedAt = from.authenticatedAt;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableTwitchUser = PersistableMixin<
  ITwitchUser,
  Constructor<_TwitchUser>,
  typeof TwitchUserModel
>(_TwitchUser, TwitchUserModel);

@ObjectType()
export class TwitchUser extends PersistableTwitchUser {
  static async findByExternalId(externalId: string) {
    return (await TwitchUser.findOne({ externalId })) as TwitchUser;
  }

  static async fromApi(result: HelixUser): Promise<TwitchUser> {
    const props: Partial<ITwitchUser> = {
      externalId: result.id,
      name: result.name,
      displayName: result.displayName,
      type: result.type,
      broadcasterType: result.broadcasterType,
      description: result.description,
      profilePictureUrl: result.profilePictureUrl,
      offlinePlaceholderUrl: result.offlinePlaceholderUrl,
      views: result.views,
    };

    let twitchUser = await TwitchUser.findByExternalId(result.id);

    if (!twitchUser) {
      twitchUser = new TwitchUser(props);
    } else {
      Object.assign(twitchUser, props);
    }
    return twitchUser;
  }
}

export type TwitchUserType = InstanceType<typeof TwitchUser>;
