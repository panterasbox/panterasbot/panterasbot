import { HelixStream } from "twitch/lib";
import { ObjectType, Field } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { TwitchId } from "./TwitchController";
import { Persistent, PersistableMixin } from "../Persistable";
import { TwitchStreamModel } from "../../../model/twitch/TwitchStream";
import { Constructor } from "../../../util/mixin";

export interface ITwitchStream
  extends IIdea,
    Timestamped,
    Persistent<ITwitchStream> {
  externalId: TwitchId;
  gameId: string;
  language?: string;
  title?: string;
  type?: string;
  thumbnailUrl?: string;
  startedAt: Date;
  userId: string;
  userName: string;
  viewerCount: number;
  tagIds: string[];
}

@ObjectType()
export class _TwitchStream extends Idea implements ITwitchStream {
  @Field({ nullable: true })
  _id?: string;

  @Field()
  externalId!: TwitchId;

  @Field()
  gameId!: string;

  @Field({ nullable: true })
  language?: string;

  @Field({ nullable: true })
  title?: string;

  @Field({ nullable: true })
  type?: string;

  @Field({ nullable: true })
  thumbnailUrl?: string;

  @Field()
  startedAt!: Date;

  @Field()
  userId!: string;

  @Field()
  userName!: string;

  @Field()
  viewerCount!: number;

  @Field((type) => [String])
  tagIds!: string[];

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: ITwitchStream) {
    super.copyFrom(from);
    this.externalId = from.externalId;
    this.gameId = from.gameId;
    this.language = from.language;
    this.title = from.title;
    this.type = from.type;
    this.thumbnailUrl = from.thumbnailUrl;
    this.startedAt = from.startedAt;
    this.userId = from.userId;
    this.userName = from.userName;
    this.viewerCount = from.viewerCount;
    this.tagIds = from.tagIds;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableTwitchStream = PersistableMixin<
  ITwitchStream,
  Constructor<_TwitchStream>,
  typeof TwitchStreamModel
>(_TwitchStream, TwitchStreamModel);

@ObjectType()
export class TwitchStream extends PersistableTwitchStream {
  static async fromApi(result: HelixStream): Promise<TwitchStream> {
    const props: Partial<ITwitchStream> = {
      externalId: result.id,
      userId: result.userId,
      userName: result.userDisplayName,
      gameId: result.gameId,
      type: result.type,
      title: result.title,
      startedAt: result.startDate,
      language: result.language,
      thumbnailUrl: result.thumbnailUrl,
      viewerCount: result.viewers,
      tagIds: result.tagIds,
    };

    let twitchStream = (await TwitchStream.findOne({
      externalId: result.id,
    })) as TwitchStream;
    if (!twitchStream) {
      twitchStream = new TwitchStream(props);
    } else {
      Object.assign(twitchStream, props);
    }
    return twitchStream;
  }
}

export type TwitchStreamType = InstanceType<typeof TwitchStream>;
