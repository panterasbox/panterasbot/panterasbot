import { HelixChannel } from "twitch/lib";
import { ObjectType, Field } from "type-graphql";
import { Idea, IIdea } from "../stuff/Idea";
import { Timestamped } from "..";
import { TwitchId } from "./TwitchController";
import { Persistent, PersistableMixin } from "../Persistable";
import { TwitchChannelModel } from "../../../model/twitch/TwitchChannel";
import { Constructor } from "../../../util/mixin";

export interface ITwitchChannel
  extends IIdea,
    Timestamped,
    Persistent<ITwitchChannel> {
  externalId: TwitchId;
  displayName: string;
  gameId: string;
  gameName?: string;
  language?: string;
  title?: string;
}

@ObjectType()
export class _TwitchChannel extends Idea implements ITwitchChannel {
  @Field({ nullable: true })
  _id?: string;

  @Field()
  externalId!: TwitchId;

  @Field()
  displayName!: string;

  @Field()
  gameId!: string;

  @Field({ nullable: true })
  gameName?: string;

  @Field({ nullable: true })
  language?: string;

  @Field({ nullable: true })
  title?: string;

  @Field()
  createdAt?: Date;

  @Field()
  updatedAt?: Date;

  copyFrom(from: ITwitchChannel) {
    super.copyFrom(from);
    this.externalId = from.externalId;
    this.displayName = from.displayName;
    this.gameId = from.gameId;
    this.gameName = from.gameName;
    this.language = from.language;
    this.title = from.title;
    this.createdAt = from.createdAt;
    this.updatedAt = from.updatedAt;
  }
}

const PersistableTwitchChannel = PersistableMixin<
  ITwitchChannel,
  Constructor<_TwitchChannel>,
  typeof TwitchChannelModel
>(_TwitchChannel, TwitchChannelModel);

@ObjectType()
export class TwitchChannel extends PersistableTwitchChannel {
  static async fromApi(result: HelixChannel): Promise<TwitchChannel> {
    const props: Partial<ITwitchChannel> = {
      externalId: result.id,
      displayName: result.displayName,
      gameId: result.gameId,
      gameName: result.gameName,
      language: result.language,
      title: result.title,
    };

    let twitchChannel = (await TwitchChannel.findOne({
      externalId: result.id,
    })) as TwitchChannel;
    if (!twitchChannel) {
      twitchChannel = new TwitchChannel(props);
    } else {
      Object.assign(twitchChannel, props);
    }
    return twitchChannel;
  }
}

export type TwitchChannelType = InstanceType<typeof TwitchChannel>;
