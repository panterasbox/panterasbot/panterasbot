import { CommandGiver } from "./command/CommandGiver";
import { StuffTemplate } from "./domain/StuffTemplate";

export interface Timestamped {
  createdAt?: Date;
  updatedAt?: Date;
}

export interface Templatized {
  applyTemplate(template: StuffTemplate): void;
}

export interface CommandProvider {
  exportInventoryCommands(commandGiver: CommandGiver): string[];
  exportEnvironmentCommands(commandGiver: CommandGiver): string[];
  exportSelfCommands(commandGiver: CommandGiver): string[];
  exportColocatedCommands(commandGiver: CommandGiver): string[];
  isCommandProvider: true;
}
