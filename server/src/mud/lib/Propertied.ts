import shortuuid from "short-uuid";
import { Templatized } from ".";
import { Constructor, Mixin, Mixins } from "../../util/mixin";
import { StuffTemplate } from "./domain/StuffTemplate";

export class Property<T extends PropValue = PropValue> extends String {}

export type PropValue =
  | object
  | string
  | number
  | boolean
  | (object | string | number | boolean)[]
  | null;

namespace Operations {
  export const Configure = "configure";
  export const Set = "set";
  export const Get = "get";
  export const Remove = "remove";
  export const Mask = "mask";
  export const Unmask = "unmask";
}

export const PropOperations: Record<string, PropOperation> = {
  Configure: Operations.Configure,
  Set: Operations.Set,
  Get: Operations.Get,
  Remove: Operations.Remove,
  Mask: Operations.Mask,
  Unmask: Operations.Unmask,
};

export type PropOperation =
  | typeof Operations.Configure
  | typeof Operations.Set
  | typeof Operations.Get
  | typeof Operations.Remove
  | typeof Operations.Mask
  | typeof Operations.Unmask;

export type PropMask<T extends PropValue> = (
  prop: Property<T>,
  op: PropOperation,
  special: any
) => boolean;

export interface PropOptions<T extends PropValue> {
  transient: boolean;
  checkAccess: PropMask<T>;
}

export interface Propertied extends Templatized {
  props: Readonly<Record<string, PropValue>>;
  initProp: <T extends PropValue>(
    prop: Property<T>,
    options: PropOptions<T>
  ) => boolean;
  configureProp: <T extends PropValue>(
    prop: Property<T>,
    options: PropOptions<T>
  ) => boolean;
  setProp: <T extends PropValue>(prop: Property<T>, value: T) => boolean;
  removeProp: <T extends PropValue>(prop: Property<T>) => boolean;
  getProp: <T extends PropValue>(prop: Property<T>) => T | null;
  maskProp: <T extends PropValue>(
    prop: Property<T>,
    mask: PropMask<T>
  ) => boolean;
  unmaskProp: <T extends PropValue>(prop: Property<T>, owner: any) => boolean;
  isMaskingProp: <T extends PropValue>(
    prop: Property<T>,
    owner: any
  ) => boolean;
  checkProp: <T extends PropValue>(prop: Property<T>) => PropOptions<T> | null;
  getAllPropNames: () => Property<PropValue>[];
  generateUniquePropName: <T extends PropValue>(seed: string) => Property<T>;
  defaultPropAccess: PropMask<PropValue>;
}

export type PropertiedConstraint = Templatized;

export function PropertiedMixin<T extends Constructor<PropertiedConstraint>>(
  Base: T
) {
  @Mixin(Mixins.Propertied)
  abstract class BasePropertied extends Base implements Propertied {
    abstract savedProps?: Record<string, PropValue>;
    transientProps: Record<string, PropValue>;
    propOptions: Record<string, PropOptions<PropValue>>;

    constructor(...args: any[]) {
      super(...args);
      this.transientProps = {};
      this.propOptions = {};
    }

    get props(): Readonly<Record<string, PropValue>> {
      return { ...this.savedProps, ...this.transientProps };
    }

    checkAccess<T extends PropValue>(
      property: Property<T>,
      op: PropOperation,
      special?: any
    ): boolean {
      const prop = property.toString();
      if (!(prop in this.propOptions)) {
        return false;
      }
      const options = this.propOptions[prop];
      return options.checkAccess(prop, op, special);
    }

    checkProp<T extends PropValue>(
      property: Property<T>
    ): PropOptions<T> | null {
      const prop = property.toString();
      if (!this.checkAccess(prop, PropOperations.Get)) {
        return null;
      }
      return this.propOptions[prop];
    }

    initProp<T extends PropValue>(
      property: Property<T>,
      options: Partial<PropOptions<T>> = {}
    ): boolean {
      const prop = property.toString();
      if (this.checkProp(prop) != null) {
        return false;
      }
      options = {
        transient: true,
        checkAccess: this.defaultPropAccess.bind(this),
        ...options,
      };

      this.propOptions[prop] = options as PropOptions<T>;
      if (options.transient) {
        this.transientProps[prop] = null;
      } else {
        if (!this.savedProps) {
          return false;
        }
        this.savedProps[prop] = null;
      }
      return true;
    }

    configureProp<T extends PropValue>(
      property: Property<T>,
      options: Partial<PropOptions<T>>
    ): boolean {
      const prop = property.toString();
      if (this.checkProp(prop) == null) {
        return false;
      }

      if (!this.checkAccess(prop, PropOperations.Configure, options)) {
        return false;
      }

      if ("transient" in options) {
        if (options.transient && !this.propOptions[prop].transient) {
          if (!this.savedProps) {
            return false;
          }
          this.transientProps[prop] = this.savedProps[prop];
          delete this.savedProps[prop];
        } else if (!options.transient && this.propOptions[prop].transient) {
          if (!this.savedProps) {
            return false;
          }
          this.savedProps[prop] = this.transientProps[prop];
          delete this.transientProps[prop];
        }
      }

      options = {
        ...this.propOptions[prop],
        ...options,
      };
      this.propOptions[prop] = options as PropOptions<T>;

      return true;
    }

    setProp<T extends PropValue>(property: Property<T>, value: T): boolean {
      const prop = property.toString();
      let options = this.checkProp(prop);
      if (options == null && !this.initProp(prop)) {
        return false;
      } else {
        options = this.checkProp(prop);
      }

      if (!this.checkAccess(prop, PropOperations.Set, value)) {
        return false;
      }

      if ((<PropOptions<T>>options).transient) {
        this.transientProps[prop] = value;
      } else {
        if (!this.savedProps) {
          return false;
        }
        this.savedProps[prop] = value;
      }

      return true;
    }

    removeProp<T extends PropValue>(property: Property<T>): boolean {
      const prop = property.toString();
      const options = this.checkProp(prop);
      if (options == null) {
        return false;
      }

      if (!this.checkAccess(prop, PropOperations.Remove)) {
        return false;
      }

      if (options.transient) {
        delete this.transientProps[prop];
      } else {
        if (!this.savedProps) {
          return false;
        }
        delete this.savedProps[prop];
      }
      delete this.propOptions[prop];

      return true;
    }

    getProp<T extends PropValue>(property: Property<T>): T | null {
      const prop = property.toString();
      const options = this.checkProp(prop);
      if (options == null) {
        return null;
      }

      if (!this.checkAccess(prop, PropOperations.Get)) {
        return null;
      }

      if (options.transient) {
        return this.transientProps[prop] as T;
      } else {
        if (!this.savedProps) {
          return null;
        }
        return this.savedProps[prop] as T;
      }
    }

    maskProp<T extends PropValue>(
      property: Property<T>,
      mask: PropMask<T>
    ): boolean {
      throw new Error("Not yet implemented");
    }

    unmaskProp<T extends PropValue>(
      property: Property<T>,
      owner: any
    ): boolean {
      throw new Error("Not yet implemented");
    }

    isMaskingProp<T extends PropValue>(
      property: Property<T>,
      owner: any
    ): boolean {
      throw new Error("Not yet implemented");
    }

    getAllPropNames(): Property<PropValue>[] {
      return Object.keys(this.propOptions);
    }

    generateUniquePropName<T extends PropValue>(seed?: string): Property<T> {
      if (!seed) {
        seed = "anonymous";
      }
      return `${seed}.${shortuuid.generate()}`;
    }

    applyTemplate(template: StuffTemplate): void {
      super.applyTemplate(template);
      const tmpl = template.template;

      if ("props" in tmpl) {
      }
    }

    abstract defaultPropAccess(
      property: Property<PropValue>,
      op: PropOperation,
      special: any
    ): boolean;
  }

  return BasePropertied;
}
