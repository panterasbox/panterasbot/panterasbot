import { Field, ID, ObjectType } from "type-graphql";
import { generateId } from "../../../model";
import { ApplicationContextMixin } from "../ApplicationContext";
import { StuffTemplate } from "../domain/StuffTemplate";
import { CommandProvider, Templatized } from "..";
import { CommandGiver } from "../command/CommandGiver";

export interface IStuff extends Templatized, CommandProvider {
  id: string;
  template: TemplateInfo | undefined;
}

@ObjectType()
export class TemplateInfo {
  @Field()
  path: string;

  @Field({ nullable: true })
  id?: string;

  constructor(path: string, id?: string) {
    this.path = path;
    this.id = id;
  }
}

@ObjectType()
export class _Stuff implements IStuff {
  @Field((type) => ID)
  id!: string;

  @Field((type) => TemplateInfo, { nullable: true })
  template: TemplateInfo | undefined;

  isCommandProvider = true as true;

  constructor();
  constructor(from: Partial<IStuff>);
  constructor(...args: any[]) {
    if (args[0]) {
      this.copyFrom(args[0]);
    }
    if (!this.id) {
      this.id = generateId(this.constructor.name);
    }
  }

  copyFrom(from: IStuff) {
    this.id = from.id;
  }

  toString() {
    return `Stuff: ${this.id}`;
  }

  applyTemplate(template: StuffTemplate): void {
    this.template = new TemplateInfo(template.path, template._id);
  }

  exportInventoryCommands(commandGiver: CommandGiver): string[] {
    return [];
  }

  exportEnvironmentCommands(commandGiver: CommandGiver): string[] {
    return [];
  }

  exportSelfCommands(commandGiver: CommandGiver): string[] {
    return [];
  }

  exportColocatedCommands(commandGiver: CommandGiver): string[] {
    return [];
  }
}

const ContextualStuff = ApplicationContextMixin(_Stuff);

@ObjectType()
export class Stuff extends ContextualStuff {
  protected initApplicationContext(): void {
    this.application.api.stuff.registerStuff(this);
  }
}

export type StuffType = InstanceType<typeof Stuff>;
