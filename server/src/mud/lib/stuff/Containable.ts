import { Constructor, hasMixin, Mixin, Mixins } from "../../../util/mixin";
import { Container } from "./Container";
import { CommandProvider } from "../";
import { CommandGiver, CommandSource } from "../command/CommandGiver";
import { Stuff } from "./Stuff";

export interface Containable {
  environment: Container | null;

  move: (container: Container & Stuff) => Promise<boolean>;
}

export function ContainableMixin<T extends Constructor>(Base: T) {
  @Mixin(Mixins.Containable)
  class BaseContainable extends Base implements Containable {
    environment: (Container & Stuff) | null = null;

    constructor(...args: any[]) {
      super(...args);
    }

    async move(container: Container & Stuff): Promise<boolean> {
      const isCommandGiver = hasMixin(this, Mixins.CommandGiver);
      const isCommandProvider = (<CommandProvider>(<unknown>this))
        .isCommandProvider;

      // Remove exported commands
      if (this.environment) {
        // remove commands from containable added from environment
        if ((<CommandProvider>(<unknown>this.environment)).isCommandProvider) {
          const environment = (this.environment as unknown) as CommandProvider;
          if (isCommandGiver) {
            const commandGiver = (this as unknown) as CommandGiver;
            commandGiver.removeAllCommands(
              CommandSource.Environment,
              environment
            );
          }
        }

        // remove commands from environment added by containable
        if (hasMixin(this.environment, Mixins.CommandGiver)) {
          const environment = (this.environment as unknown) as CommandGiver;
          if (isCommandProvider) {
            const commandProvider = (this as unknown) as CommandProvider;
            environment.removeAllCommands(
              CommandSource.Inventory,
              commandProvider
            );
          }
        }

        for (const containable of this.environment.inventory) {
          if (containable == this) {
            continue;
          }

          // remove commands from containable added by colocated
          if ((<CommandProvider>(<unknown>containable)).isCommandProvider) {
            const colocated = (containable as unknown) as CommandProvider;
            if (isCommandGiver) {
              const commandGiver = (this as unknown) as CommandGiver;
              commandGiver.removeAllCommands(
                CommandSource.Colocated,
                colocated
              );
            }
          }

          // remove commands from colocated added by containable
          if (hasMixin(containable, Mixins.CommandGiver)) {
            const colocated = (containable as unknown) as CommandGiver;
            if (isCommandProvider) {
              const commandProvider = (this as unknown) as CommandProvider;
              colocated.removeAllCommands(
                CommandSource.Colocated,
                commandProvider
              );
            }
          }
        }
      }

      // Execute move
      this.environment?.inventory.delete(this);
      this.environment = container;
      container.inventory.add(this);

      // Add exported commands
      if (this.environment) {
        // add commands to containable from environment
        if ((<CommandProvider>(<unknown>this.environment)).isCommandProvider) {
          const environment = (this.environment as unknown) as CommandProvider;
          if (isCommandGiver) {
            const commandGiver = (this as unknown) as CommandGiver;
            await Promise.all(
              environment.exportInventoryCommands(commandGiver).map((path) => {
                return commandGiver.addCommand(
                  CommandSource.Environment,
                  environment,
                  path
                );
              })
            );
          }
        }

        // add commands to environment from containable
        if (hasMixin(this.environment, Mixins.CommandGiver)) {
          const environment = (this.environment as unknown) as CommandGiver;
          if (isCommandProvider) {
            const commandProvider = (this as unknown) as CommandProvider;
            await Promise.all(
              commandProvider
                .exportEnvironmentCommands(environment)
                .map((path) => {
                  return environment.addCommand(
                    CommandSource.Inventory,
                    commandProvider,
                    path
                  );
                })
            );
          }
        }

        for (const containable of this.environment.inventory) {
          if (containable == this) {
            continue;
          }

          // add commands to containable from colocated
          if ((<CommandProvider>(<unknown>containable)).isCommandProvider) {
            const colocated = (containable as unknown) as CommandProvider;
            if (isCommandGiver) {
              const commandGiver = (this as unknown) as CommandGiver;
              await Promise.all(
                colocated.exportColocatedCommands(commandGiver).map((path) => {
                  return commandGiver.addCommand(
                    CommandSource.Colocated,
                    colocated,
                    path
                  );
                })
              );
            }
          }

          // add commands to colocated from containable
          if (hasMixin(containable, Mixins.CommandGiver)) {
            const colocated = (containable as unknown) as CommandGiver;
            if (isCommandProvider) {
              const commandProvider = (this as unknown) as CommandProvider;
              await Promise.all(
                commandProvider
                  .exportColocatedCommands(colocated)
                  .map((path) => {
                    return colocated.addCommand(
                      CommandSource.Colocated,
                      commandProvider,
                      path
                    );
                  })
              );
            }
          }
        }
      }

      return true;
    }
  }

  return BaseContainable;
}
