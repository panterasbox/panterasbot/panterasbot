import { ObjectType } from "type-graphql";
import { IStuff, Stuff } from "./Stuff";

export interface IIdea extends IStuff {}

@ObjectType()
export class Idea extends Stuff implements IIdea {}
