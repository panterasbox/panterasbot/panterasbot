import { ObjectType } from "type-graphql";
import { IStuff, Stuff } from "./Stuff";
import { Containable, ContainableMixin } from "./Containable";
import { Sensor, SensorMixin } from "../character/Sensor";

export interface IAgent extends IStuff, Containable, Sensor {}

class _Agent extends Stuff {}

const ContainableAgent = ContainableMixin(_Agent);
const SensorAgent = SensorMixin(ContainableAgent);

@ObjectType()
export class Agent extends SensorAgent implements IAgent {}
