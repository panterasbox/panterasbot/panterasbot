import { ObjectType } from "type-graphql";
import { IStuff, Stuff } from "./Stuff";
import { ContainableMixin } from "./Containable";
import { Container } from "./Container";

export interface IThing extends IStuff {
  environment: Container | null;
}

class _Thing extends Stuff {}

const ContainableThing = ContainableMixin(_Thing);

@ObjectType()
export class Thing extends ContainableThing implements IThing {}
