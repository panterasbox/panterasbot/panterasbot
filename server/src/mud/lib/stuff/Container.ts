import { Constructor, Mixin, Mixins } from "../../../util/mixin";
import { Containable } from "./Containable";

export interface Container {
  inventory: Set<Containable>;
}

export function ContainerMixin<T extends Constructor>(Base: T) {
  @Mixin(Mixins.Container)
  class BaseContainer extends Base implements Container {
    inventory: Set<Containable> = new Set();

    constructor(...args: any[]) {
      super(...args);
    }
  }

  return BaseContainer;
}
