import { ObjectType } from "type-graphql";
import { Stuff } from "./Stuff";
import { Containable, ContainableMixin } from "./Containable";
import { Container, ContainerMixin } from "./Container";
import { IThing } from "./Thing";
import { Detailed, DetailedMixin } from "./Detailed";
import { Visible, VisibleMixin } from "./Visible";
import {
  Propertied,
  PropertiedMixin,
  Property,
  PropOperation,
  PropValue,
} from "../Propertied";

export const DEFAULT_SHORT = "a nondescript receptacle";
export const DEFAULT_LONG = "It lacks any features whatsoever.";

export const OUT_DIRECTION = "out";

export interface IReceptacle
  extends IThing,
    Propertied,
    Containable,
    Container,
    Visible,
    Detailed {}

class _Receptacle extends Stuff {}

const PropertiedReceptacle = PropertiedMixin(_Receptacle);
const ContainableReceptacle = ContainableMixin(PropertiedReceptacle);
const ContainerReceptacle = ContainerMixin(ContainableReceptacle);
const VisibleReceptacle = VisibleMixin(ContainerReceptacle);
const DetailedReceptacle = DetailedMixin(VisibleReceptacle);

@ObjectType()
export class Receptacle extends DetailedReceptacle implements IReceptacle {
  savedProps: undefined;
  longView: string = DEFAULT_LONG;
  shortView: string = DEFAULT_SHORT;

  constructor(...args: ConstructorParameters<typeof DetailedReceptacle>) {
    super(...args);
  }

  defaultPropAccess(
    property: Property<PropValue>,
    op: PropOperation,
    special: any
  ): boolean {
    return true;
  }
}
