import { CommandProvider, Templatized } from "..";
import { Constructor, Mixin, Mixins } from "../../../util/mixin";
import { CommandGiver } from "../command/CommandGiver";
import { StuffTemplate } from "../domain/StuffTemplate";
import { IStuff } from "./Stuff";

export interface Visible extends Templatized, CommandProvider {
  longView: string;
  shortView: string;
  invisible: boolean;
}

export type VisibleConstraint = IStuff & Templatized & CommandProvider;

export function VisibleMixin<T extends Constructor<VisibleConstraint>>(
  Base: T
) {
  @Mixin(Mixins.Visible)
  abstract class BaseVisible extends Base implements Visible {
    abstract longView: string;
    abstract shortView: string;
    invisible: boolean = false;

    constructor(...args: any[]) {
      super(...args);
    }

    applyTemplate(template: StuffTemplate): void {
      super.applyTemplate(template);
      const tmpl = template.template;

      if ("shortView" in tmpl) {
        this.shortView = tmpl.shortView;
      }
      if ("longView" in tmpl) {
        this.longView = tmpl.longView;
      }
    }

    exportInventoryCommands(commandGiver: CommandGiver): string[] {
      const result = super.exportInventoryCommands(commandGiver);
      result.push("look");
      return result;
    }

    exportEnvironmentCommands(commandGiver: CommandGiver): string[] {
      const result = super.exportEnvironmentCommands(commandGiver);
      result.push("look");
      return result;
    }

    exportSelfCommands(commandGiver: CommandGiver): string[] {
      const result = super.exportSelfCommands(commandGiver);
      result.push("look");
      return result;
    }

    exportColocatedCommands(commandGiver: CommandGiver): string[] {
      const result = super.exportColocatedCommands(commandGiver);
      result.push("look");
      return result;
    }
  }

  return BaseVisible;
}
