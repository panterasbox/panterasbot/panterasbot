import { WellKnownEvents } from "../../../api/event";
import { Application } from "../../../backend/Application";
import { Avatar } from "../../obj/Avatar";
import { ConnectionEvent } from "../../obj/notify/ConnectionController";
import { ApplicationContextMixin } from "../ApplicationContext";
import { Interactive } from "../connection/Interactive";

const _NotifyController = ApplicationContextMixin(Object);

export abstract class NotifyController extends _NotifyController {
  subscribers: Set<Avatar> = new Set();

  constructor(application: Application) {
    super(application);
    this.initSubscriptions();
  }

  initSubscriptions() {
    const eventApi = this.application.api.event;
    const playerApi = this.application.api.player;

    eventApi.subscribe<ConnectionEvent>(WellKnownEvents.connect, (event) => {
      if (this.checkSubscription(event.payload.interactive)) {
        this.subscribers.add(event.payload.interactive.avatar as Avatar);
      }
    });

    eventApi.subscribe<ConnectionEvent>(WellKnownEvents.disconnect, (event) => {
      if (this.subscribers.has(event.payload.interactive.avatar as Avatar)) {
        this.subscribers.delete(event.payload.interactive.avatar as Avatar);
      }
    });

    playerApi.findAvatars().forEach((avatar) => {
      for (const interactive of avatar.interactives) {
        if (this.checkSubscription(interactive)) {
          this.subscribers.add(avatar);
          break;
        }
      }
    });
  }

  abstract checkSubscription(interactive: Interactive): boolean;
}
