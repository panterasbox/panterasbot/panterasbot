import { Constructor, Mixin, Mixins } from "../../../util/mixin";
import { Stuff } from "../stuff/Stuff";
import { CommandProvider } from "..";
import { CommandGiver } from "../command/CommandGiver";

export interface Vocal extends CommandProvider {}

export type VocalConstraint = Stuff & CommandProvider;

export function VocalMixin<T extends Constructor<VocalConstraint>>(Base: T) {
  @Mixin(Mixins.Vocal)
  class BaseVocal extends Base implements Vocal {
    constructor(...args: any[]) {
      super(...args);
    }

    exportSelfCommands(commandGiver: CommandGiver): string[] {
      const result = super.exportSelfCommands(commandGiver);
      result.push("say");
      return result;
    }
  }

  return BaseVocal;
}
