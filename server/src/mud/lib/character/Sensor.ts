import { MessagePayload } from "../../../api/message";
import { Constructor, Mixin, Mixins } from "../../../util/mixin";
import { Stuff } from "../stuff/Stuff";

export interface Sensor {
  interceptMessage(message: MessagePayload): void;
  onMessage(message: MessagePayload): void;
}

export type SensorConstraint = Stuff;

export function SensorMixin<T extends Constructor<SensorConstraint>>(Base: T) {
  @Mixin(Mixins.Sensor)
  class BaseSensor extends Base implements Sensor {
    constructor(...args: any[]) {
      super(...args);
    }

    interceptMessage(message: MessagePayload): void {
      return;
    }

    onMessage(message: MessagePayload): void {
      return;
    }
  }

  return BaseSensor;
}
