import { Application } from "../../../backend/Application";
import { Constructor, Mixin, Mixins } from "../../../util/mixin";

export interface Named {
  firstName?: string;
  lastName?: string;
  fullName: string;
  displayName: string;
  isAnonymous: boolean;
}

export function NamedMixin<T extends Constructor>(Base: T) {
  @Mixin(Mixins.Named)
  abstract class BaseNamed extends Base implements Named {
    abstract firstName?: string;
    abstract lastName?: string;

    constructor(...args: any[]) {
      super(...args);
    }

    get isAnonymous(): boolean {
      return !(this.firstName || this.lastName);
    }

    get fullName(): string {
      const application = Application.fromContext();
      const stringApi = application.api.string;

      let result = "";
      if (this.firstName) {
        result += stringApi.capitalize(this.firstName);
        if (this.lastName) {
          result += " ";
        }
      }
      if (this.lastName) {
        result += stringApi.capitalize(this.lastName);
      }
      if (!result.length) {
        return "Anonymous";
      }
      return result;
    }

    get displayName(): string {
      const application = Application.fromContext();
      const stringApi = application.api.string;
      if (this.firstName) {
        return stringApi.capitalize(this.firstName);
      }
      // XXX check also for last name or nick name?
      return "Anonymous";
    }
  }

  return BaseNamed;
}
