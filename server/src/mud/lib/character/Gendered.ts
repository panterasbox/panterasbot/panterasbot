import { Constructor, Mixin, Mixins } from "../../../util/mixin";

export enum Pronouns {
  He = "he/him",
  She = "she/her",
  They = "they/them",
  It = "it/it",
}

export interface Gendered {
  pronouns: Pronouns;
}

export function GenderedMixin<T extends Constructor>(Base: T) {
  @Mixin(Mixins.Gendered)
  abstract class BaseGendered extends Base implements Gendered {
    abstract pronouns: Pronouns;

    constructor(...args: any[]) {
      super(...args);
    }
  }

  return BaseGendered;
}
