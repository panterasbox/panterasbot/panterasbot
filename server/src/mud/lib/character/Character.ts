import { CommandGiverMixin } from "../command/CommandGiver";
import { PropertiedMixin } from "../Propertied";
import { Agent } from "../stuff/Agent";
import { GenderedMixin } from "./Gendered";
import { NamedMixin } from "./Named";
import { VocalMixin } from "./Vocal";

class _Character extends Agent {}

const PropertiedCharacter = PropertiedMixin(_Character);
const GenderedCharacter = GenderedMixin(PropertiedCharacter);
const NamedCharacter = NamedMixin(GenderedCharacter);
const CommandGiverCharacter = CommandGiverMixin(NamedCharacter);
const VocalCharacter = VocalMixin(CommandGiverCharacter);

export abstract class Character extends VocalCharacter {}

export type CharacterType = InstanceType<typeof Character>;
