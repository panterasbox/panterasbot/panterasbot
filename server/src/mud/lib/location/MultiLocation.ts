import { Constructor, Mixin, Mixins } from "../../../util/mixin";
import { Containable } from "../stuff/Containable";
import { Stuff } from "../stuff/Stuff";
import { Location } from "./Location";

export interface MultiLocation {
  routeEntrance(stuff: Containable & Stuff): Location & MultiLocation;
}

export type MultiLocationConstraint = Location;

export function MultiLocationMixin<
  T extends Constructor<MultiLocationConstraint>
>(Base: T) {
  @Mixin(Mixins.MultiLocation)
  abstract class BaseMultiLocation extends Base implements MultiLocation {
    constructor(...args: any[]) {
      super(...args);
    }

    abstract routeEntrance(
      stuff: Containable & Stuff
    ): Location & MultiLocation;
  }

  return BaseMultiLocation;
}
