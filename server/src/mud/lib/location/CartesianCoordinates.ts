import { Coordinates } from ".";
import { Constructor, Mixin, Mixins } from "../../../util/mixin";

export interface CartesianCoordinates extends Coordinates {
  setXCoordinate: (x: number) => boolean;
  getXCoordinate: () => number;
  setYCoordinate: (y: number) => boolean;
  getYCoordinate: () => number;
  setZCoordinate: (z: number) => boolean;
  getZCoordinate: () => number;
}

export function CartesianCoordinatesMixin<T extends Constructor>(Base: T) {
  @Mixin(Mixins.CartesianCoordinates)
  class BaseCartesianCoordinates extends Base implements CartesianCoordinates {
    coordinates: [number, number, number];

    constructor(...args: any[]) {
      super(...args);
      this.coordinates = [0, 0, 0];
    }

    setXCoordinate(x: number): boolean {
      this.coordinates[0] = x;
      return true;
    }

    getXCoordinate(): number {
      return this.coordinates[0];
    }

    setYCoordinate(y: number): boolean {
      this.coordinates[1] = y;
      return true;
    }

    getYCoordinate(): number {
      return this.coordinates[1];
    }

    setZCoordinate(z: number): boolean {
      this.coordinates[2] = z;
      return true;
    }

    getZCoordinate(): number {
      return this.coordinates[2];
    }
  }

  return BaseCartesianCoordinates;
}
