import { Door } from "./Door";
import { Location } from "./Location";

export class Exit {
  direction: string;
  path: string;
  door: Door | null;
  destination?: Location;
  hidden: boolean;
  blocked: boolean;
  muffled: boolean;
  noFollow: boolean;
  // pedestrian: boolean;
  messageIn: string | null;
  messageOut: string | null;

  constructor(opts: {
    direction: string;
    path: string;
    door: Door | null | undefined;
    hidden: boolean | undefined;
    blocked: boolean | undefined;
    muffled: boolean | undefined;
    noFollow: boolean | undefined;
    // pedestrian: boolean | undefined;
    messageIn: string | null | undefined;
    messageOut: string | null | undefined;
  }) {
    this.direction = opts.direction;
    this.path = opts.path;
    this.door = opts.door || null;
    this.hidden = opts.hidden || false;
    this.blocked = opts.blocked || false;
    this.muffled = opts.muffled || false;
    this.noFollow = opts.noFollow || false;
    // this.pedestrian = opts.pedestrian || false;
    this.messageIn = opts.messageIn || null;
    this.messageOut = opts.messageOut || null;
  }
}
