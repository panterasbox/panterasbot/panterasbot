import { CommandProvider, Templatized } from "..";
import { Constructor, Mixin, Mixins } from "../../../util/mixin";
import { StuffTemplate } from "../domain/StuffTemplate";
import { Exit } from "./Exit";

export type Direction = string;

export interface Exitable extends Templatized, CommandProvider {
  teleportMessageOut: string | null;
  teleportMessageIn: string | null;

  setExits: (exits: Map<Direction, Exit>) => boolean;
  removeExit: (direction: Direction) => boolean;
  getExit: (direction: Direction) => Exit | undefined;
  getExits: () => Map<Direction, Exit>;
  addExit: (exit: Exit) => boolean;
}

export type ExitableConstraint = CommandProvider & Templatized;

export function ExitableMixin<T extends Constructor<ExitableConstraint>>(
  Base: T
) {
  @Mixin(Mixins.Exitable)
  class BaseExitable extends Base implements Exitable {
    exits: Map<Direction, Exit> = new Map();
    teleportMessageOut: string | null = null;
    teleportMessageIn: string | null = null;

    constructor(...args: any[]) {
      super(...args);
    }

    setExits(exits: Map<Direction, Exit>): boolean {
      this.exits = exits;
      return true;
    }

    removeExit(direction: Direction): boolean {
      this.exits.delete(direction);
      return true;
    }

    getExit(direction: Direction): Exit | undefined {
      return this.exits.get(direction);
    }

    addExit(exit: Exit): boolean {
      if (this.exits.has(exit.direction)) {
        this.removeExit(exit.direction);
      }
      this.exits.set(exit.direction, exit);
      return true;
    }

    getExits(): Map<string, Exit> {
      return this.exits;
    }

    applyTemplate(template: StuffTemplate): void {
      super.applyTemplate(template);
      const tmpl = template.template;

      if ("exits" in tmpl) {
        const exits = tmpl.exits as Record<string, any>;
        for (const [dir, exit] of Object.entries(exits)) {
          this.addExit(
            new Exit({
              direction: dir,
              path: exit.destination as string,
              hidden: (exit.hidden as boolean) || false,
              blocked: (exit.blocked as boolean) || false,
              muffled: (exit.muffled as boolean) || false,
              noFollow: (exit.noFollow as boolean) || false,
              messageIn: (exit.messageIn as string) || null,
              messageOut: (exit.messageOut as string) || null,
              door: null,
            })
          );
        }
      }
    }
  }

  return BaseExitable;
}
