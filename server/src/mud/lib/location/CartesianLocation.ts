import { StuffTemplate } from "../domain/StuffTemplate";
import { CartesianCoordinatesMixin } from "./CartesianCoordinates";
import { Location } from "./Location";

const _CartesianLocation = CartesianCoordinatesMixin(Location);

export class CartesianLocation extends _CartesianLocation {
  applyTemplate(template: StuffTemplate): void {
    super.applyTemplate(template);
    const tmpl = template.template;

    if ("coordinates" in tmpl) {
      this.coordinates = tmpl.coordinates;
    }
  }
}
