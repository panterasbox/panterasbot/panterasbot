import { ObjectType } from "type-graphql";
import {
  Propertied,
  PropertiedMixin,
  Property,
  PropOperation,
  PropValue,
} from "../Propertied";
import { IStuff, Stuff } from "../stuff/Stuff";
import { Container, ContainerMixin } from "../stuff/Container";
import { Detailed, DetailedMixin } from "../stuff/Detailed";
import { Visible, VisibleMixin } from "../stuff/Visible";
import { Exitable, ExitableMixin } from "./Exitable";
import { StuffTemplate } from "../domain/StuffTemplate";

export enum Biome {
  Urban = "urban",
}

export const DEFAULT_SHORT = "An Unnamed Location";
export const DEFAULT_LONG = "You see nothing in all directions.";
export const DEFAULT_BIOME = Biome.Urban;

export interface ILocation
  extends IStuff,
    Propertied,
    Container,
    Exitable,
    Visible,
    Detailed {}

class _Location extends Stuff {}

const PropertiedLocation = PropertiedMixin(_Location);
const ContainerLocation = ContainerMixin(PropertiedLocation);
const ExitableLocation = ExitableMixin(ContainerLocation);
const VisibleLocation = VisibleMixin(ExitableLocation);
const DetailedLocation = DetailedMixin(VisibleLocation);

@ObjectType()
export class Location extends DetailedLocation implements ILocation {
  savedProps: undefined;
  longView: string = DEFAULT_LONG;
  shortView: string = DEFAULT_SHORT;
  biome: Biome = DEFAULT_BIOME;
  indoors: boolean = false;

  defaultPropAccess(
    property: Property<PropValue>,
    op: PropOperation,
    special: any
  ): boolean {
    return true;
  }

  applyTemplate(template: StuffTemplate) {
    super.applyTemplate(template);
    const tmpl = template.template;

    if ("biome" in tmpl) {
      this.biome = tmpl.biome;
    }
    if ("indoors" in tmpl) {
      this.indoors = tmpl.indoors;
    }
  }
}
