import { Coordinates } from ".";
import { Constructor, Mixin, Mixins } from "../../../util/mixin";

export interface SphericalCoordinates extends Coordinates {
  setRhoCoordinate: (rho: number) => boolean;
  getRhoCoordinate: () => number;
  setThetaCoordinate: (theta: number) => boolean;
  getThetaCoordinate: () => number;
  setPhiCoordinate: (phi: number) => boolean;
  getPhiCoordinate: () => number;
}

export function SphericalCoordinatesMixin<T extends Constructor>(Base: T) {
  @Mixin(Mixins.SphericalCoordinates)
  class BaseSphericalCoordinates extends Base implements SphericalCoordinates {
    coordinates: [number, number, number];

    constructor(...args: any[]) {
      super(...args);
      this.coordinates = [0, 0, 0];
    }

    setRhoCoordinate(rho: number) {
      this.coordinates[0] = rho;
      return true;
    }

    getRhoCoordinate(): number {
      return this.coordinates[0];
    }

    setThetaCoordinate(theta: number) {
      this.coordinates[1] = theta;
      return true;
    }

    getThetaCoordinate(): number {
      return this.coordinates[1];
    }

    setPhiCoordinate(phi: number) {
      this.coordinates[2] = phi;
      return true;
    }

    getPhiCoordinate(): number {
      return this.coordinates[2];
    }
  }

  return BaseSphericalCoordinates;
}
