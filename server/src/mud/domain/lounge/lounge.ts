import { ObjectType } from "type-graphql";
import { CartesianLocation } from "../../lib/location/CartesianLocation";

@ObjectType()
export default class Lounge extends CartesianLocation {}
