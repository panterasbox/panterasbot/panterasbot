import { CommandModel } from "../../../api/command";
import { MessagePayload } from "../../../api/message";
import { hasMixin, Mixins } from "../../../util/mixin";
import { CommandController } from "../../lib/command/CommandController";
import { Exitable } from "../../lib/location/Exitable";
import { Visible } from "../../lib/stuff/Visible";
import { Location } from "../../lib/location/Location";
import esc from "xml-escape";
import { Stuff } from "../../lib/stuff/Stuff";

interface LookModel extends CommandModel {
  target: Visible[];
}

interface DoorParam {}

interface ExitParam {
  direction: string;
  path: string;
  door?: DoorParam;
  destination?: string;
  hidden: boolean;
  blocked: boolean;
  muffled: boolean;
  noFollow: boolean;
}

export default class LookController extends CommandController<LookModel, {}> {
  async execute(model: LookModel): Promise<{}> {
    const messageApi = this.application.api.message;
    const mudlogApi = this.application.api.mudlog;

    if (!model.target.length) {
      mudlogApi.info("There isn't anything like that here.");
      return {};
    }

    model.target.forEach((visible) => {
      if (visible instanceof Location) {
        const message = this.locationMessage(visible);
        messageApi.message(this.context.commandGiver, message);
      }
    });
    return {};
  }

  locationMessage(location: Location & Visible): MessagePayload {
    let exitMessage = "";
    let exits: Record<string, ExitParam> | undefined;
    if (hasMixin(location, Mixins.Exitable)) {
      const exitable = (location as unknown) as Exitable;
      exitMessage += "<flex><inline>―――&#160;</inline>";
      exits = {};
      let exitList = "";
      for (const [direction, exit] of exitable.getExits()) {
        if (exit.hidden) {
          continue;
        }
        exitList += `<exit dir="${esc(direction)}">${esc(direction)}`;
        if (exit.blocked) {
          exitList += "&#160;(<blocked/>)";
        }
        exitList += `</exit>`;

        let door: {} | undefined = undefined;
        if (exit.door) {
          door = {};
        }
        exits[direction] = {
          direction: exit.direction,
          path: exit.path,
          door,
          hidden: exit.hidden,
          blocked: exit.blocked,
          muffled: exit.muffled,
          noFollow: exit.noFollow,
          destination: exit.destination?.id,
        };
      }
      if (exitList.length) {
        exitMessage += `<inline>Obvious exits are: <exits>${exitList}</exits>.</inline></flex>`;
      } else {
        exitMessage += "<inline>There are no obvious exits.</inline></flex>";
      }
    }

    let inventoryMessage = "";
    const inventory = [];
    for (const containable of location.inventory) {
      if (containable === this.context.commandGiver) {
        continue;
      }
      if (hasMixin(containable, Mixins.Visible)) {
        const visible = (containable as unknown) as Visible;
        let tag = "stuff";
        // if (visible instanceof Location) {
        //   tag = 'location';
        // } else if (visible instanceof Agent) {
        //   tag = 'agent';
        // } else if (visible instanceof Thing) {
        //   tag = 'thing';
        // } else if (visible instanceof Receptacle) {
        //   tag = 'receptacle';
        // }
        inventoryMessage += `<${tag}>${esc(visible.shortView)}</${tag}>.`;
        const stuff = (visible as unknown) as Stuff;
        inventory.push({
          id: stuff.id,
          template: stuff.template?.path,
        });
      }
      inventoryMessage = `<inventory>${inventoryMessage}</inventory>`;
    }

    const message = `
      <location>${esc(location.shortView)}</location>
      <p indent="3em">${esc(location.longView)}</p>
      ${exitMessage}
      ${inventoryMessage}
    `;
    const params = {
      target: {
        type: "location",
        id: location.id,
        template: location.template?.path,
      },
      exits,
      inventory,
    };
    return {
      topic: `vision.${location.id}`,
      message,
      params,
    };
  }
}
