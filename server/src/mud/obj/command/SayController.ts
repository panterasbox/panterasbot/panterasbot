import { CommandModel } from "../../../api/command";
import { hasMixin, Mixins } from "../../../util/mixin";
import { Named } from "../../lib/character/Named";
import { CommandController } from "../../lib/command/CommandController";
import { Visible } from "../../lib/stuff/Visible";
import esc from "xml-escape";

interface SayModel extends CommandModel {
  message: string;
}

export default class SayController extends CommandController<SayModel, {}> {
  async execute(model: SayModel): Promise<{}> {
    const messageApi = this.application.api.message;
    const mudlogApi = this.application.api.mudlog;
    const commandGiver = this.context.commandGiver;

    if (!commandGiver.environment) {
      mudlogApi.warn("You can't speak without an environment.");
      return {};
    }

    let subject: string = "An unknown voice";
    if (hasMixin(commandGiver, Mixins.Named)) {
      subject = ((commandGiver as unknown) as Named).displayName;
    } else if (hasMixin(commandGiver, Mixins.Visible)) {
      subject = ((commandGiver as unknown) as Visible).shortView;
    }
    // TODO Audible Mixin?

    const params = {
      subject: {
        id: commandGiver.id,
        template: commandGiver.template?.path,
      },
    };

    const topic = `voice.say.${commandGiver.environment.id}`;
    messageApi.messageContainer(
      commandGiver.environment,
      {
        topic,
        message: `
          <voice><flex>
            <prefix><subject>${subject}</subject>&#160;says:</prefix>
            <inline>${esc(model.message)}</inline>
          </flex></voice>
        `,
        params,
      },
      [commandGiver]
    );

    messageApi.message(commandGiver, {
      topic,
      message: `
        <voice><flex>
          <prefix><subject>You</subject>&#160;say:</prefix>
          <inline>${esc(model.message)}</inline>
        </flex></voice>
      `,
      params,
    });

    return {};
  }
}
