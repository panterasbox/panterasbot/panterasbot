import moment from "moment";
import { CommandModel } from "../../../api/command";
import { CommandController } from "../../lib/command/CommandController";
import {
  StreamStatus,
  StreamStatusController,
} from "../notify/StreamStatusController";

interface StreamStatusModel extends CommandModel {
  status?: string;
  transition?: string;
  time: number;
}

export default class StandbyController extends CommandController<
  StreamStatusModel,
  {}
> {
  async execute(model: StreamStatusModel): Promise<{}> {
    const objectApi = this.application.api.object;
    const mudlogApi = this.application.api.mudlog;
    const timeApi = this.application.api.time;

    let streamStatusController;
    try {
      streamStatusController = await objectApi.singleton(
        StreamStatusController
      );
    } catch (e) {
      console.warn(`Could not load stream status controller`, e);
      mudlogApi.error(`Could not load stream status controller`);
      return {};
    }

    if (model.status) {
      streamStatusController.updateState(
        model.status as StreamStatus,
        model.time,
        model.transition ? (model.transition as StreamStatus) : undefined
      );

      const duration = timeApi.formatDuration(
        moment.duration(model.time, "seconds")
      );

      mudlogApi.info(
        `Stream status set to '${model.status}'${
          model.time ? ` for ${duration}` : ""
        }.`
      );
    } else if (model.transition) {
      streamStatusController.scheduleState(
        model.transition as StreamStatus,
        model.time
      );

      const duration = timeApi.formatDuration(
        moment.duration(model.time, "seconds")
      );

      mudlogApi.info(
        `Stream status will be set to '${model.transition}'${
          model.time ? ` in ${duration}` : ""
        }.`
      );
    } else {
      const duration = timeApi.remainingDuration(
        streamStatusController.state.duration || 0,
        streamStatusController.state.time
      );

      if (duration.asMilliseconds() > 0) {
        mudlogApi.info(
          `Stream status is '${streamStatusController.state.status}'${
            streamStatusController.state.duration
              ? ` for ${timeApi.formatDuration(duration)}`
              : ""
          }.`
        );
      } else {
        mudlogApi.info(
          `Stream status is currently '${streamStatusController.state.status}'.`
        );
      }
    }

    return {};
  }
}
