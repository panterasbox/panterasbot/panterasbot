import { CommandModel } from "../../../api/command";
import { Character } from "../../lib/character/Character";
import { CommandController } from "../../lib/command/CommandController";
import { AvatarType } from "../Avatar";

interface PlayerModel extends CommandModel {
  player: AvatarType;
}

interface PlayerNameModel extends PlayerModel {
  firstName?: string;
  lastName?: string;
}

interface PlayerAvatarModel extends PlayerModel {
  imageUrl?: string;
}

interface PlayerPronounsModel extends PlayerModel {
  pronouns?: string;
}

export default class PlayerController extends CommandController<
  PlayerNameModel,
  {}
> {
  async execute(model: PlayerModel, subcommand?: string): Promise<{}> {
    switch (subcommand) {
      case "name":
        return await this.executeName(model as PlayerNameModel);
      case "pronouns":
        return await this.executePronouns(model as PlayerPronounsModel);
      case "avatar":
        return await this.executeAvatar(model as PlayerAvatarModel);
      default:
        throw Error("Not yet implemented");
    }
  }

  async executeName(model: PlayerNameModel): Promise<{}> {
    const assertApi = this.application.api.assert;
    const grammarApi = this.application.api.grammar;
    const stringApi = this.application.api.string;

    assertApi.isInstanceOf(this.context.commandGiver, Character);
    const commandGiver = this.context.commandGiver as Character;
    const subject = grammarApi.possessive(model.player, commandGiver);

    const previous = {
      first: model.player.firstName,
      last: model.player.lastName,
      full: model.player.fullName,
    };
    let save = false;

    if (model.firstName) {
      model.player.firstName = model.firstName.toLowerCase();
      save = true;
    }
    if (model.lastName) {
      model.player.lastName = model.lastName.toLowerCase();
      save = true;
    }
    if (save) {
      await model.player.player.save();
    }

    const params = {
      name: {
        first: model.player.firstName,
        last: model.player.lastName,
        full: model.player.fullName,
      },
      previous: previous,
    };

    if (save) {
      if (model.player.fullName) {
        this.application.api.mudlog.info(
          `Changed ${subject} full name to ${model.player.fullName}.`,
          params
        );
      } else {
        const fmt = stringApi.buildTemplate<[string, Character]>(
          (subject: string) => subject,
          (character: Character) =>
            stringApi.capitalize(grammarApi.pronounBe(character, commandGiver))
        );
        this.application.api.mudlog.info(
          fmt`Cleared ${subject} name. ${model.player} now anonymous.`,
          params
        );
      }
    } else {
      if (model.player.fullName) {
        this.application.api.mudlog.info(
          `${subject} full name is ${model.player.fullName}.`,
          params
        );
      } else {
        const fmt = stringApi.buildTemplate<[Character, Character]>(
          (character: Character) =>
            stringApi.capitalize(
              grammarApi.properHave(character, commandGiver)
            ),
          (character: Character) =>
            stringApi.capitalize(grammarApi.pronounBe(character, commandGiver))
        );
        this.application.api.mudlog.info(
          fmt`${model.player} has no name set. ${model.player} anonymous.`,
          params
        );
      }
    }

    return {};
  }

  async executePronouns(model: PlayerPronounsModel): Promise<{}> {
    const assertApi = this.application.api.assert;
    const grammarApi = this.application.api.grammar;
    const stringApi = this.application.api.string;

    assertApi.isInstanceOf(this.context.commandGiver, Character);
    const commandGiver = this.context.commandGiver as Character;
    const subject = grammarApi.possessive(model.player, commandGiver);

    let save = false;
    const previous = model.player.pronouns;
    if (model.pronouns) {
      const pronouns = this.application.api.player.toPronouns(model.pronouns);
      if (pronouns) {
        model.player.pronouns = pronouns;
        save = true;
      }
    }
    if (save) {
      await model.player.player.save();
    }

    const params = {
      pronouns: model.player.pronouns,
      previous: previous,
    };

    if (save) {
      this.application.api.mudlog.info(
        `Changed ${subject} pronouns to ${model.player.pronouns}.`,
        params
      );
    } else {
      this.application.api.mudlog.info(
        `${stringApi.capitalize(subject)} prounouns are ${
          model.player.pronouns
        }.`,
        params
      );
    }

    return {};
  }

  async executeAvatar(model: PlayerAvatarModel): Promise<{}> {
    const assertApi = this.application.api.assert;
    const grammarApi = this.application.api.grammar;
    const stringApi = this.application.api.string;

    assertApi.isInstanceOf(this.context.commandGiver, Character);
    const commandGiver = this.context.commandGiver as Character;
    const subject = grammarApi.possessive(model.player, commandGiver);

    let save = false;
    const previous = model.player.avatar;
    if (model.imageUrl) {
      model.player.avatar = model.imageUrl;
      save = true;
    }
    if (save) {
      await model.player.player.save();
    }

    const params = {
      avatar: model.player.avatar,
      previous: previous,
    };

    if (save) {
      this.application.api.mudlog.info(
        `Changed ${subject} avatar to ${model.player.avatar}.`,
        params
      );
    } else {
      this.application.api.mudlog.info(
        `${stringApi.capitalize(subject)} avatar is ${model.player.avatar}.`,
        params
      );
    }

    return {};
  }
}
