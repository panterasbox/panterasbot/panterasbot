import { CommandController } from "../../lib/command/CommandController";
import { Agent } from "../../lib/stuff/Agent";

export default class PingController extends CommandController<{}, {}> {
  async execute(model: {}): Promise<{}> {
    const messageApi = this.application.api.message;
    if (this.context.commandGiver instanceof Agent) {
      messageApi.messageAll([this.context.commandGiver as Agent], {
        topic: "system.ping",
        message: "Pong",
      });
    }
    return {};
  }
}
