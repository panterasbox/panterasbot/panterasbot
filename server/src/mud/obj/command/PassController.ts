import { CommandModel } from "../../../api/command";
import { CommandController } from "../../lib/command/CommandController";

interface PassModel extends CommandModel {}

export default class PassController extends CommandController<PassModel, {}> {
  async execute(model: PassModel): Promise<{}> {
    this.context.terminate = false;
    return {};
  }
}
