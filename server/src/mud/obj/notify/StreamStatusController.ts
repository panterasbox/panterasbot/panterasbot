import moment from "moment";
import { WellKnownEvents } from "../../../api/event";
import { MessagePayload } from "../../../api/message";
import { Application } from "../../../backend/Application";
import { Interactive } from "../../lib/connection/Interactive";
import { NotifyController } from "../../lib/notify/NotifyController";
import { Property } from "../../lib/Propertied";
import { Avatar } from "../Avatar";
import { ConnectionEvent } from "./ConnectionController";

export const NotifyProp = new Property<boolean>("notify.streamStatus");
export const NotifyTopic = "notify.streamStatus";

export enum StreamStatus {
  Starting = "starting",
  Standby = "standby",
  Live = "live",
  Ending = "ending",
}

export interface StreamState {
  status: StreamStatus;
  duration: number | undefined;
  time: number;
}

export class StreamStatusController extends NotifyController {
  state: StreamState;
  timer?: NodeJS.Timeout;

  constructor(application: Application) {
    super(application);
    this.state = {
      status: StreamStatus.Standby,
      time: moment().valueOf(),
      duration: undefined,
    };

    const eventApi = this.application.api.event;

    eventApi.subscribe<ConnectionEvent>(WellKnownEvents.connect, (event) => {
      if (this.checkSubscription(event.payload.interactive)) {
        this.application.api.message.message(
          event.payload.interactive.avatar as Avatar,
          this.notifyMessage()
        );
      }
    });

    this.notifySubscribers();
  }

  updateState(
    status: StreamStatus,
    duration?: number,
    transition?: StreamStatus
  ) {
    this.state = {
      status,
      duration,
      time: moment().valueOf(),
    };
    this.notifySubscribers();
    if (transition && duration) {
      this.scheduleState(transition, duration);
    } else {
      if (this.timer) {
        clearTimeout(this.timer);
        this.timer = undefined;
      }
    }
  }

  scheduleState(transition: StreamStatus, duration: number) {
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = undefined;
    }
    if (transition && duration) {
      this.timer = setTimeout(() => {
        this.updateState(transition);
      }, duration * 1000);
    }
  }

  notifySubscribers() {
    this.application.api.message.messageAll(
      Array.from(this.subscribers),
      this.notifyMessage()
    );
  }

  notifyMessage(): MessagePayload {
    const timeApi = this.application.api.time;
    const remaining = timeApi.remainingDuration(
      this.state.duration || 0,
      this.state.time
    );
    const duration = timeApi.formatDuration(remaining);
    return {
      topic: NotifyTopic,
      message: `Stream status is '${this.state.status}'${
        remaining.asMilliseconds() > 0 ? ` for ${duration}` : ""
      }.`,
      params: this.state,
    };
  }

  checkSubscription(interactive: Interactive): boolean {
    const avatar = interactive.avatar;
    if (!avatar) {
      return false;
    }
    // OBS clients automatically get status updates
    if (
      typeof interactive.userAgent === "string" &&
      interactive.userAgent.match(/\bOBS\b/) != null
    ) {
      return true;
    }
    return avatar.getProp(NotifyProp) || true;
  }
}
