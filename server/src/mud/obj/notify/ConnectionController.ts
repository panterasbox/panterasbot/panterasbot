import { WellKnownEvents } from "../../../api/event";
import { Application } from "../../../backend/Application";
import { Interactive } from "../../lib/connection/Interactive";
import { NotifyController } from "../../lib/notify/NotifyController";
import { Property } from "../../lib/Propertied";
import { Avatar } from "../Avatar";

export const NotifyProp = new Property<boolean>("notify.connection");
export const NotifyTopic = "notify.connection";

export interface ConnectionEvent {
  interactive: Interactive;
  linkdead: boolean;
  event: symbol;
}

export class ConnectionController extends NotifyController {
  subscribers: Set<Avatar> = new Set();

  constructor(application: Application) {
    super(application);
    const eventApi = this.application.api.event;

    eventApi.subscribe<ConnectionEvent>(WellKnownEvents.connect, (event) => {
      this.onConnect(event.payload, event.time);
    });

    eventApi.subscribe<ConnectionEvent>(WellKnownEvents.disconnect, (event) => {
      this.onDisconnect(event.payload, event.time);
    });
  }

  onConnect(event: ConnectionEvent, time: number) {
    const interactive = event.interactive;
    const avatar = interactive.avatar;
    if (!avatar) {
      console.warn("onLogin() called without avatar", interactive);
      return;
    }
    this.notifySubscribers(event, time);
    this.application.api.event.dispatch(WellKnownEvents.login, avatar);
  }

  onDisconnect(event: ConnectionEvent, time: number) {
    const interactive = event.interactive;
    const avatar = interactive.avatar;
    if (!avatar) {
      console.warn("onLogin() called without avatar", interactive);
      return;
    }
    this.application.api.event.dispatch(WellKnownEvents.logout, avatar);
    this.notifySubscribers(event, time);
  }

  notifySubscribers(event: ConnectionEvent, time: number) {
    this.application.api.message.messageAll(
      Array.from(this.subscribers),
      this.notifyMessage(event, time)
    );
  }

  notifyMessage(event: ConnectionEvent, time: number) {
    const interactive = event.interactive;
    const avatar = interactive.avatar;
    const getEventMessage = () => {
      if (event.linkdead) {
        if (event.event == WellKnownEvents.connect) {
          return "reconnected";
        } else {
          return "disconnected";
        }
      } else {
        if (event.event == WellKnownEvents.connect) {
          return "logged in";
        } else {
          return "logged out";
        }
      }
    };
    return {
      topic: NotifyTopic,
      message: `${
        avatar?.fullName ? avatar.fullName : "Nobody"
      } just ${getEventMessage()}.`,
      params: {
        event: event.event.description,
        player: {
          id: avatar?.player.id,
          _id: avatar?.player._id,
          firstName: avatar?.firstName,
          lastName: avatar?.lastName,
          avatar: avatar?.player.avatar,
          user: {
            id: avatar?.player.user.id,
            _id: avatar?.player.user._id,
            avatar: avatar?.player.user.avatar,
            isGuest: avatar?.player.user.isGuest,
          },
        },
        interactiveId: interactive.id,
        linkdead: event.linkdead,
        time,
      },
    };
  }

  checkSubscription(interactive: Interactive): boolean {
    const avatar = interactive.avatar;
    if (!avatar) {
      return false;
    }
    return avatar.getProp(NotifyProp) || true;
  }
}
