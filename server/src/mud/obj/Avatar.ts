import { CommandGiver } from "../lib/command/CommandGiver";
import { PlayerType } from "../lib/connection/Player";
import { Interactive } from "../lib/connection/Interactive";
import { Character } from "../lib/character/Character";
import { Pronouns } from "../lib/character/Gendered";
import { Property, PropOperation, PropValue } from "../lib/Propertied";
import { Location } from "../lib/location/Location";
import { CommandProvider } from "../lib";
import { CommandShellMixin } from "../lib/connection/CommandShell";
import { MessagePayload } from "../../api/message";
import { VisibleMixin } from "../lib/stuff/Visible";

export const StartLocationProp = new Property<string>("avatar.startLocation");
export const DEFAULT_START_LOCATION = "/domain/lounge/lounge";

export class _Avatar extends Character implements CommandProvider {
  player: PlayerType;
  interactives: Set<Interactive> = new Set();

  constructor(player: PlayerType) {
    super();
    this.player = player;
  }

  set firstName(firstName: string | undefined) {
    this.player.firstName = firstName;
  }

  get firstName(): string | undefined {
    return this.player.firstName;
  }

  set lastName(lastName: string | undefined) {
    this.player.lastName = lastName;
  }

  get lastName(): string | undefined {
    return this.player.lastName;
  }

  set pronouns(pronouns: Pronouns) {
    this.player.pronouns = pronouns;
  }

  get pronouns() {
    return this.player.pronouns;
  }

  set savedProps(props: Record<string, PropValue>) {
    this.player.props = new Map<string, PropValue>(Object.entries(props));
  }

  get savedProps() {
    return Object.fromEntries(this.player.props.entries());
  }

  async resolveStartLocation(): Promise<[string, Location | undefined]> {
    const templateApi = this.application.api.template;

    if (!this.checkProp(StartLocationProp)) {
      this.setProp(StartLocationProp, DEFAULT_START_LOCATION);
    }
    const path = this.getProp(StartLocationProp) || DEFAULT_START_LOCATION;
    let location = await templateApi.routeLocationTemplate(path, this);

    if (!location && path != DEFAULT_START_LOCATION) {
      location = await templateApi.routeLocationTemplate(
        DEFAULT_START_LOCATION,
        this
      );
    }

    return [path, location];
  }

  async moveToStartLocation(): Promise<boolean> {
    const [path, location] = await this.resolveStartLocation();

    if (location) {
      const result = await this.move(location);
      if (result) {
        this.application.api.command.command(this, {
          command: "look",
          params: {},
          noEcho: true,
        });
      }
      return result;
    } else {
      console.warn("Couldn't move player to start location: ", this);
      this.application.api.mudlog.warn(
        `Error moving avatar to start location`,
        { path },
        undefined,
        this
      );
      return false;
    }
  }

  defaultPropAccess(prop: Property, op: PropOperation, special: any): boolean {
    return true;
  }

  exportSelfCommands(commandGiver: CommandGiver): string[] {
    const result = super.exportSelfCommands(commandGiver);
    result.push("player", "stream", "ping");
    return result;
  }

  onMessage(message: MessagePayload): void {
    for (const interactive of this.interactives) {
      try {
        interactive.sendAction({
          type: "websocket/message",
          payload: message,
        });
      } catch (e) {
        console.warn("Caught error sending action", e);
      }
    }
  }
}

const VisibleAvatar = VisibleMixin(_Avatar);
const CommandShellAvatar = CommandShellMixin(VisibleAvatar);

export class Avatar extends CommandShellAvatar {
  longView: string = "Default long";

  get shortView(): string {
    return this.fullName;
  }
}

export type AvatarType = InstanceType<typeof Avatar>;
