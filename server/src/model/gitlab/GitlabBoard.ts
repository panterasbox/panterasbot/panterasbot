import { model, Schema } from "mongoose";
import { IGitlabBoard } from "../../mud/lib/gitlab/GitlabBoard";

const schema = new Schema<Partial<IGitlabBoard>>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    externalId: {
      type: String,
      required: true,
      index: true,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

export const GitlabBoardModel = model<IGitlabBoard>("GitlabBoard", schema);
