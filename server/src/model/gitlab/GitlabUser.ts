import { model, Schema } from "mongoose";
import { IGitlabUser } from "../../mud/lib/gitlab/GitlabUser";

const schema = new Schema<IGitlabUser>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    externalId: {
      type: String,
      required: true,
      index: true,
    },
    name: {
      type: String,
    },
    username: {
      type: String,
    },
    accountCreatedAt: {
      type: Date,
    },
    location: {
      type: String,
    },
    publicEmail: {
      type: String,
    },
    linkedin: {
      type: String,
    },
    twitter: {
      type: String,
    },
    websiteUrl: {
      type: String,
    },
    email: {
      type: String,
    },
    twoFactorEnabled: {
      type: Boolean,
    },
    privateProfile: {
      type: Boolean,
    },
    accessToken: {
      type: String,
    },
    refreshToken: {
      type: String,
    },
    authenticatedAt: {
      type: Date,
      required: true,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

export const GitlabUserModel = model<IGitlabUser>("GitlabUser", schema);
