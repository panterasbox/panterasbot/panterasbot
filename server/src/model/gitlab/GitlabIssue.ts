import { model, Schema } from "mongoose";
import { IGitlabIssue } from "../../mud/lib/gitlab/GitlabIssue";

const schema = new Schema<IGitlabIssue>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    externalId: {
      type: Number,
      required: true,
      index: true,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

export const GitlabIssueModel = model<IGitlabIssue>("GitlabIssue", schema);
