import { model, Schema } from "mongoose";
import { IGitlabProject } from "../../mud/lib/gitlab/GitlabProject";

const schema = new Schema<IGitlabProject>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    externalId: {
      type: Number,
      required: true,
      index: true,
    },
    description: {
      type: String,
    },
    name: {
      type: String,
    },
    nameWithNamespace: {
      type: String,
    },
    path: {
      type: String,
    },
    pathWithNamespace: {
      type: String,
    },
    projectCreatedAt: {
      type: Date,
    },
    defaultBranch: {
      type: String,
    },
    tagList: {
      type: [String],
    },
    sshUrlToRepo: {
      type: String,
    },
    httpUrlToRepo: {
      type: String,
    },
    webUrl: {
      type: String,
    },
    readmeUrl: {
      type: String,
    },
    avatarUrl: {
      type: String,
    },
    forksCount: {
      type: Number,
    },
    starCount: {
      type: Number,
    },
    lastActivityAt: {
      type: Date,
    },
    namespaceId: {
      type: Number,
      required: true,
    },
    namespaceKind: {
      type: String,
      required: true,
    },
    _links: {
      type: Object,
    },
    visibility: {
      type: String,
    },
    creatorId: {
      type: Number,
    },
    openIssuesCount: {
      type: Number,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

export const GitlabProjectModel = model<IGitlabProject>(
  "GitlabProject",
  schema
);
