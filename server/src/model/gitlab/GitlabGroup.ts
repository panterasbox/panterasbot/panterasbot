import { model, Schema } from "mongoose";
import { IGitlabGroup } from "../../mud/lib/gitlab/GitlabGroup";

const schema = new Schema<IGitlabGroup>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    externalId: {
      type: String,
      required: true,
      index: true,
    },
    webUrl: {
      type: String,
    },
    name: {
      type: String,
    },
    path: {
      type: String,
    },
    description: {
      type: String,
    },
    visibility: {
      type: String,
    },
    projectCreationLevel: {
      type: String,
    },
    subgroupCreationLevel: {
      type: String,
    },
    avatarUrl: {
      type: String,
    },
    fullName: {
      type: String,
    },
    fullPath: {
      type: String,
    },
    groupCreatedAt: {
      type: Date,
    },
    parentId: {
      type: Number,
      index: true,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

export const GitlabGroupModel = model<IGitlabGroup>("GitlabGroup", schema);
