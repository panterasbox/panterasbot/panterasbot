import { model, Schema } from "mongoose";
import { IPlayer } from "../mud/lib/connection/Player";

const schema = new Schema<IPlayer>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    userId: {
      type: Schema.Types.ObjectId,
      required: true,
      index: true,
      ref: "User",
    },
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    pronouns: {
      type: String,
      required: true,
    },
    avatar: {
      type: String,
    },
    props: {
      type: Schema.Types.Map,
      required: true,
      of: Schema.Types.Mixed,
    },
  },
  { timestamps: true }
);

export const PlayerModel = model<IPlayer>("Player", schema);
