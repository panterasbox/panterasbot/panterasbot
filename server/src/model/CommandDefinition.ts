import { model, Schema } from "mongoose";
import { ICommandDefinition } from "../mud/lib/command/CommandDefinition";
import * as commandSchema from "../mud/template/command.schema.json";
import { JsonSchema } from ".";

const schema = new JsonSchema<ICommandDefinition>(
  "command",
  commandSchema,
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    path: {
      type: Schema.Types.String,
      required: true,
      index: { unique: true },
    },
  },
  { timestamps: true }
);

export const CommandDefinitionModel = model<ICommandDefinition>(
  "CommandDefinition",
  schema
);
