import { model, Schema } from "mongoose";
import { IStuffTemplate } from "../mud/lib/domain/StuffTemplate";

const schema = new Schema<IStuffTemplate>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    name: {
      type: Schema.Types.String,
      required: true,
      index: true,
    },
    domain: {
      type: Schema.Types.String,
      required: true,
      index: true,
    },
    path: {
      type: String,
      required: true,
      index: { unique: true },
    },
    type: {
      type: String,
      required: true,
    },
    schemaPath: {
      type: String,
      index: true,
    },
    template: {
      type: Schema.Types.Mixed,
      required: true,
    },
  },
  { timestamps: true }
);

schema.index({ type: 1, schema: 1 });

export const StuffTemplateModel = model<IStuffTemplate>(
  "StuffTemplate",
  schema
);
