import { model, Schema } from "mongoose";
import { IUser } from "../mud/lib/connection/User";

const schema = new Schema<IUser>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    twitchUserId: {
      type: Schema.Types.ObjectId,
      required: true,
      index: true,
    },
    gitlabUserId: {
      type: Schema.Types.ObjectId,
      index: true,
    },
    username: {
      type: String,
    },
    email: {
      type: String,
    },
    avatar: {
      type: String,
    },
    isGuest: {
      type: Boolean,
      required: true,
    },
    isStreamer: {
      type: Boolean,
      required: true,
    },
    isFollower: {
      type: Boolean,
      required: true,
    },
    subscriberTier: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true }
);

export const UserModel = model<IUser>("User", schema);
