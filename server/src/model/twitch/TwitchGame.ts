import { model, Schema } from "mongoose";
import { ITwitchGame } from "../../mud/lib/twitch/TwitchGame";

const schema = new Schema<ITwitchGame>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    externalId: {
      type: String,
      required: true,
      index: true,
    },
    name: {
      type: String,
      required: true,
    },
    boxArtUrl: {
      type: String,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

export const TwitchGameModel = model<ITwitchGame>("TwitchGame", schema);
