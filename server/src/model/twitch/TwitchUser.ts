import { model, Schema } from "mongoose";
import { ITwitchUser } from "../../mud/lib/twitch/TwitchUser";

const schema = new Schema<ITwitchUser>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    externalId: {
      type: String,
      required: true,
      index: true,
    },
    name: {
      type: String,
      required: true,
    },
    displayName: {
      type: String,
      required: true,
    },
    type: {
      type: String,
    },
    broadcasterType: {
      type: String,
    },
    description: {
      type: String,
    },
    profilePictureUrl: {
      type: String,
    },
    offlinePlaceholderUrl: {
      type: String,
    },
    views: {
      type: Number,
    },
    email: {
      type: String,
    },
    accessToken: {
      type: String,
    },
    refreshToken: {
      type: String,
    },
    authenticatedAt: {
      type: Date,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

export const TwitchUserModel = model<ITwitchUser>("TwitchUser", schema);
