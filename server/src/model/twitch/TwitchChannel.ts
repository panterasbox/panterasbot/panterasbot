import { model, Schema } from "mongoose";
import { ITwitchChannel } from "../../mud/lib/twitch/TwitchChannel";

const schema = new Schema<ITwitchChannel>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    externalId: {
      type: String,
      required: true,
      index: true,
    },
    displayName: {
      type: String,
      required: true,
    },
    gameId: {
      type: String,
      required: true,
    },
    gameName: {
      type: String,
    },
    language: {
      type: String,
    },
    title: {
      type: String,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

export const TwitchChannelModel = model<ITwitchChannel>(
  "TwitchChannel",
  schema
);
