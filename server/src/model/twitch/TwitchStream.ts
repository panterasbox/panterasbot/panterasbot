import { model, Schema } from "mongoose";
import { ITwitchStream } from "../../mud/lib/twitch/TwitchStream";

const schema = new Schema<ITwitchStream>(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
      auto: true,
    },
    externalId: {
      type: String,
      required: true,
      index: true,
    },
    gameId: {
      type: String,
      required: true,
    },
    language: {
      type: String,
    },
    title: {
      type: String,
    },
    type: {
      type: String,
    },
    thumbnailUrl: {
      type: String,
    },
    startedAt: {
      type: Date,
    },
    userId: {
      type: String,
    },
    userName: {
      type: String,
    },
    viewerCount: {
      type: Number,
    },
    tagIds: {
      type: [String],
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  },
  { timestamps: true }
);

export const TwitchStreamModel = model<ITwitchStream>("TwitchStream", schema);
