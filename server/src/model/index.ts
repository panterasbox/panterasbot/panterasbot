import * as AWS from "aws-sdk";
import shortuuid from "short-uuid";
import {
  connect,
  Schema,
  Document,
  Model,
  SchemaOptions,
  SchemaDefinition,
  SchemaDefinitionProperty,
  _AllowStringsForIds,
  LeanDocument,
} from "mongoose";
import config from "../util/config";
import deepmerge from "deepmerge";
const _camelize = require("camelize");
const deref = require("json-schema-deref-sync");

AWS.config.update({ region: config.region });

const MONGO_URI = `mongodb+srv://${encodeURIComponent(
  AWS.config.credentials?.accessKeyId || ""
)}:${encodeURIComponent(
  AWS.config.credentials?.secretAccessKey || ""
)}@sandbox.vtt5n.mongodb.net/test?authSource=%24external&authMechanism=MONGODB-AWS&retryWrites=true&w=majority`;

export const initialize = async () => {
  const result = await Promise.all([]);

  await connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  return result;
};

export const generateId = (table: string): string => {
  return shortuuid.generate();
};

export const checkedDate = (dateStr: string | undefined): Date | undefined => {
  return dateStr ? new Date(dateStr) : undefined;
};

export const camelize = <T>(obj: T): T => {
  return _camelize(obj);
};

export class JsonSchema<
  DocType = Document<any, any>,
  M extends Model<DocType, any, any> = Model<any, any, any>,
  SchemaDefinitionType = undefined
> extends Schema<DocType, M, SchemaDefinitionType> {
  static convertSchema(
    name: string,
    schema: any,
    isList: boolean = false
  ): SchemaDefinition<undefined> {
    if ("anyOf" in schema) {
      return {
        [name]: {
          type: Schema.Types.Mixed,
        },
      };
    } else if ("allOf" in schema) {
      let all = {};
      for (const el of schema["allOf"]) {
        all = deepmerge(all, el);
      }
      schema = all;
    }

    let attribute: SchemaDefinitionProperty = {};
    switch (schema["type"]) {
      case "object":
      case "objects":
        if (schema["properties"]) {
          const propSchema = Object.entries(schema["properties"])
            .map(([propName, prop]) => {
              return this.convertSchema(propName, prop, false);
            })
            .reduce((prev: any, curr: any, index: number) => {
              return { ...prev, ...curr };
            });
          attribute = {
            type: isList ? [new Schema(propSchema)] : new Schema(propSchema),
          };
        } else {
          if (schema["patternProperties"]) {
            const propName = Object.keys(schema["patternProperties"])[0];
            const prop = schema["patternProperties"][propName];
            const propSchema = this.convertSchema(propName, prop, false);
            attribute = {
              type: isList ? [Schema.Types.Map] : Schema.Types.Map,
              of: propSchema[propName],
            };
          } else {
            attribute = {
              type: isList ? [Schema.Types.Map] : Schema.Types.Map,
            };
          }
        }
        break;
      case "string":
        attribute = {
          type: isList ? [Schema.Types.String] : Schema.Types.String,
        };
        break;
      case "boolean":
        attribute = {
          type: isList ? [Schema.Types.Boolean] : Schema.Types.Boolean,
        };
        break;
      case "number":
        attribute = {
          type: isList ? [Schema.Types.Number] : Schema.Types.Number,
        };
        break;
      case "array":
        return JsonSchema.convertSchema(name, schema["items"], true);
      default:
        if (schema["type"] && "enum" in schema["type"]) {
          attribute = {
            type: isList ? [Schema.Types.String] : Schema.Types.String,
          };
        } else {
          throw new Error(`Unknown schema type: ${name} ${schema["type"]}`);
        }
    }
    // TODO set "required" props as required
    if ("default" in schema) {
      attribute.default = schema["default"];
    }

    return { [name]: attribute };
  }

  constructor(
    name: string,
    jsonSchema: {},
    definition?: SchemaDefinition<
      _AllowStringsForIds<LeanDocument<SchemaDefinitionType>>
    >,
    options?: SchemaOptions
  ) {
    const schema = {
      ...JsonSchema.convertSchema(name, deref(jsonSchema), false),
      ...definition,
    } as SchemaDefinition<
      _AllowStringsForIds<LeanDocument<SchemaDefinitionType>>
    >;
    super(schema, options);
  }
}
