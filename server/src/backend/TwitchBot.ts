import * as tmi from "tmi.js";
import { Backend } from "./Backend";

export class TwitchBot {
  backend: Backend;
  client?: tmi.Client;

  constructor(backend: Backend) {
    this.backend = backend;
  }

  async start() {
    if (!this.backend.application.twitchController.channel) {
      throw new Error("Application not connected to Twitch channel");
    }

    this.client = new tmi.client({
      options: { debug: true, messagesLogLevel: "info" },
      connection: {
        reconnect: true,
        secure: true,
      },
      identity: {
        username: this.backend.getParameter("twitch_bot_name"),
        password: this.backend.getSecret("bot_oauth_token"),
      },
      channels: [this.backend.application.twitchController.channel.displayName],
    });

    this.client.on("message", (target, context, msg, self) => {
      const streamer = this.backend.application.twitchController.user;
      if (self) return;
      if (msg.startsWith("!")) {
        if (msg.startsWith("!!")) {
          if (context["user-id"] == streamer?.externalId) {
            this.doCommand(msg.trim().substr(2), target, context);
          }
        } else {
          this.doCommand(msg.trim().substr(1), target, context);
        }
      }
    });

    this.client.on("connected", (addr, port) => {
      console.debug(`TwitchBot connected at ${addr}:${port}`);
    });

    try {
      const res = await this.client.connect();
    } catch (e) {
      console.error("Caught", e);
    }
  }

  async doCommand(command: string, target: string, context: tmi.ChatUserstate) {
    switch (command) {
      case "help":
        await this.sendMessage(
          target,
          "Sorry, this feature is not yet implemented."
        );
        break;
      case "discord":
        if (this.backend.getParameter("discord_invite")) {
          await this.sendMessage(
            target,
            `Join the Discord server today! 
             ${this.backend.getParameter("discord_invite")}`
          );
        }
        break;
      case "project":
        await this.sendMessage(
          target,
          `Check out the Pantera's Bot project page on GitLab:
          https://gitlab.com/panterasbox/panterasbot/panterasbot`
        );
        break;
    }
  }

  async sendMessage(target: string, message: string) {
    if (!this.client) {
      throw new Error(
        "Unable to send message: " + "TwitchBot client not initialized"
      );
    }
    const result = await this.client.say(target, message);
    console.log("sayResult", result);
    return result;
  }
}
