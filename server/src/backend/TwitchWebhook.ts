import axios from "axios";
import { Backend } from "./Backend";
import * as crypto from "crypto";
import * as Discord from "discord.js";
import { getS3Url, s3 } from "../util/aws";

const TWITCH_SUBSCRIPTIONS = "/eventsub/subscriptions";
const API_URL_PARAM = "twitch_api_url";
const S3_BUCKET_PARAM = "s3_bucket";
const STREAM_IMAGES = "stream-images";
const WAIT_STREAM_ONLINE = 300 * 1000;

export class TwitchWebhook {
  backend: Backend;
  secret?: string;
  callbackHost?: string;

  constructor(backend: Backend) {
    this.backend = backend;
  }

  async initialize() {
    if (process.env.NODE_ENV === "production") {
      this.callbackHost = `https://${this.backend.getParameter("server_host")}`;
    }
    if (!this.secret) {
      this.secret = (await crypto.randomBytes(48)).toString("base64");
    }
  }

  async subscribe(eventType: string) {
    const token = await this.backend.twitchApi?.getAccessToken();

    if (!this.callbackHost) {
      if (process.env.NODE_ENV !== "production") {
        console.warn("No development callback active for Twitch webhook");
        return;
      }
      throw new Error("No callback host set for webhook subscription");
    }

    const user = this.backend.application.twitchController.user;
    if (!user) {
      throw new Error("No user connected for webhook subscription");
    }

    const subscribeUrl = `${this.backend.getParameter(
      API_URL_PARAM
    )}${TWITCH_SUBSCRIPTIONS}`;
    console.log("subscribeUrl", subscribeUrl);
    try {
      const response = await axios.post(
        subscribeUrl,
        {
          type: eventType,
          version: "1",
          condition: {
            broadcaster_user_id: user.externalId,
          },
          transport: {
            method: "webhook",
            callback: `${this.callbackHost}/webhook/twitch/callback`,
            secret: this.secret,
          },
        },
        {
          url: subscribeUrl,
          headers: {
            "Client-ID": this.backend.twitchApi?.clientId,
            Accept: "application/vnd.twitchtv.v5+json",
            Authorization: "Bearer " + token?.accessToken,
          },
        }
      );

      const data = JSON.parse(response.config.data);
      console.log("sub", data);
      return data;
    } catch (e) {
      console.warn("Caught error during webhook subscription", e);
    }
  }

  async getAllSubscriptions() {
    const token = await this.backend.twitchApi?.getAccessToken();

    const subscribeUrl = `${this.backend.getParameter(
      API_URL_PARAM
    )}${TWITCH_SUBSCRIPTIONS}`;

    try {
      const subs = await axios.get(subscribeUrl, {
        url: subscribeUrl,
        headers: {
          "Client-ID": this.backend.twitchApi?.clientId,
          Authorization: "Bearer " + token?.accessToken,
        },
      });
      return subs.data.data;
    } catch (e) {
      console.warn("Caught error during webhook unsubscribe", e);
    }
  }

  async unsubscribe(sub: Record<string, any>) {
    const token = await this.backend.twitchApi?.getAccessToken();

    if (!this.callbackHost) {
      if (process.env.NODE_ENV !== "production") {
        console.warn("No development callback active for Twitch webhook");
        return;
      }
      throw new Error("No callback host set for webhook subscription");
    }

    const subscribeUrl = `${this.backend.getParameter(
      API_URL_PARAM
    )}${TWITCH_SUBSCRIPTIONS}`;

    try {
      const deleted = await axios.delete(`${subscribeUrl}?id=${sub.id}`, {
        url: subscribeUrl,
        headers: {
          "Client-ID": this.backend.twitchApi?.clientId,
          Authorization: "Bearer " + token?.accessToken,
        },
      });
      console.log("deleted", deleted.statusText, sub);
      return deleted;
    } catch (e) {
      console.warn("Caught error during webhook unsubscribe", e);
    }
  }

  handleEvent(type: string, event: Record<string, any>): boolean {
    console.log("event", type, event);
    switch (type) {
      case "stream.online":
        this.streamOnline(event);
        return true;
      case "stream.offline":
        this.streamOffline(event);
        return true;
      case "channel.update":
        break;
      default:
        break;
    }
    return false;
  }

  async streamOnline(event: Record<string, any>): Promise<boolean> {
    this.backend.application.twitchController.setStreaming(true);
    setTimeout(() => {
      this.notifyDiscord(event);
    }, WAIT_STREAM_ONLINE);
    return true;
  }

  async streamOffline(event: Record<string, any>): Promise<boolean> {
    this.backend.application.twitchController.setStreaming(false);
    return true;
  }

  async notifyDiscord(event: Record<string, any>): Promise<boolean> {
    const client = this.backend.discordClient;
    if (client) {
      const notifyChannel = this.backend.getParameter("discord_notify_channel");
      const channelId = client.channels.cache.findKey(
        (value, key, collection): boolean => {
          return (value as Discord.TextChannel).name == notifyChannel;
        }
      );
      if (!channelId) {
        console.warn(`Notify channel '${notifyChannel}' not found`);
        return false;
      }

      const twitchController = this.backend.application.twitchController;

      await this.backend.application.twitchController.refreshStream();

      let startedAt = undefined;
      if (twitchController.stream?.startedAt) {
        startedAt = new Date(
          twitchController.stream.startedAt
        ).toLocaleDateString("en", {
          year: "numeric",
          month: "short",
          day: "numeric",
          timeZoneName: "short",
          hour12: false,
          hour: "numeric",
          minute: "numeric",
        });
      }

      let thumbnailUrl = twitchController.stream?.thumbnailUrl;
      if (thumbnailUrl) {
        thumbnailUrl = thumbnailUrl.replace("{width}", "640");
        thumbnailUrl = thumbnailUrl.replace("{height}", "360");
        const thumbResponse = await axios.get(thumbnailUrl, {
          responseType: "arraybuffer",
        });
        if (thumbResponse.status == 200) {
          const bucket = this.backend.getParameter(S3_BUCKET_PARAM);
          const key = `${twitchController.stream?.externalId}-640x360.jpg`;
          try {
            const result = await s3
              .putObject({
                Body: Buffer.from(thumbResponse.data, "binary"),
                Bucket: `${bucket}/${STREAM_IMAGES}`,
                Key: key,
              })
              .promise();
            thumbnailUrl = getS3Url(bucket, `${STREAM_IMAGES}/${key}`);
          } catch (e) {
            console.error("Caught error in s3 putObject", e);
          }
        }
      }

      const twitchWebUrl = this.backend.getParameter("twitch_web_url");
      this.sendDiscordMessage(channelId, {
        embed: {
          color: "#002768",
          title: `${twitchController.stream?.userName} went live on Twitch`,
          url: `${twitchWebUrl}/${twitchController.channel?.displayName}`,
          description: `${twitchController.stream?.title}`,
          thumbnail: {
            url: twitchController.user?.profilePictureUrl,
          },
          fields: [
            {
              name: "Playing",
              value: `\`\`\`${twitchController.channel?.gameName}\`\`\``,
              inline: true,
            },
            {
              name: "Started at",
              value: `\`\`\`${startedAt}\`\`\``,
              inline: true,
            },
          ],
          image: {
            url: thumbnailUrl,
          },
          timestamp: new Date(),
        },
      });
      return true;
    }
    return false;
  }

  async sendDiscordMessage(channelId: string, message: Discord.MessageOptions) {
    console.log("sending", channelId, message);
    const client = this.backend.discordClient;
    if (client && channelId) {
      const channel = (await client.channels.fetch(
        channelId
      )) as Discord.TextChannel;
      channel.send(message);
    }
  }
}
