import WebSocket from "ws";
import { Backend, PayloadAction } from "./Backend";
import {
  TwitchController,
  TwitchProfile,
} from "../mud/lib/twitch/TwitchController";
import {
  GitlabController,
  GitlabId,
  GitlabProfile,
} from "../mud/lib/gitlab/GitlabController";
import { API } from "../api";
import { ApplicationContext } from "../mud/lib/ApplicationContext";
import callsites from "callsites";
import { TwitchUser } from "../mud/lib/twitch/TwitchUser";
import { Interactive } from "../mud/lib/connection/Interactive";
import { GitlabUser } from "../mud/lib/gitlab/GitlabUser";
import { Avatar, AvatarType } from "../mud/obj/Avatar";
import { hasMixin, Mixins } from "../util/mixin";
import { SessionData } from "express-session";
import moment from "moment";
import { StreamStatusController } from "../mud/obj/notify/StreamStatusController";
import { ConnectionController } from "../mud/obj/notify/ConnectionController";

export class Application {
  private backend: Backend;
  api: API;
  twitchController: TwitchController;
  gitlabController: GitlabController;

  constructor(backend: Backend) {
    this.backend = backend;
    this.twitchController = new TwitchController(this);
    this.gitlabController = new GitlabController(this);
    this.api = new API(this);
  }

  static fromContext(): Application {
    let stack = callsites();
    // const db: any[] = [];
    // for (let callsite of stack) {
    //   db.push({
    //     ob: callsite.getThis(),
    //     function: callsite.getFunction(),
    //     columnNumber: callsite.getColumnNumber(),
    //     evalOrigin: callsite.getEvalOrigin(),
    //     fileName: callsite.getFileName(),
    //     functionName: callsite.getFunctionName(),
    //     lineNumber: callsite.getLineNumber(),
    //     methodName: callsite.getMethodName(),
    //     typeName: callsite.getTypeName(),
    //     isConstructor: callsite.isConstructor(),
    //     isEval: callsite.isEval(),
    //   });
    // }
    const that = stack[1].getThis();
    stack = stack.splice(2);

    for (let callsite of stack) {
      const ob = callsite.getThis();
      if (!ob) {
        continue;
      }

      if (hasMixin(ob, Mixins.ApplicationContext)) {
        if (ob === that || !(ob as ApplicationContext).application) {
          continue;
        }
        return (ob as ApplicationContext).application;
      }

      if (ob instanceof Application) {
        return ob as Application;
      }
    }
    throw new Error("Unable to determine Application from context");
  }

  async initialize() {
    await this.api.initialize();
  }

  async start() {
    // XXX move this singleton stuff somewhere else
    try {
      await this.api.object.singleton(StreamStatusController);
    } catch (e) {
      console.warn(`Could not load stream status controller`, e);
    }
    try {
      await this.api.object.singleton(ConnectionController);
    } catch (e) {
      console.warn(`Could not load connection controller`, e);
    }
    ///////////////////////////////

    this.twitchController.start(this.backend.twitchApi);
    this.gitlabController.start(this.backend.gitlabApi);

    await this.twitchController.setChannel(
      this.backend.getParameter("twitch_channel_name")
    );
    await this.gitlabController.setRootGroup(
      parseInt(this.backend.getParameter("gitlab_group_id"))
    );
    const stream = await this.twitchController.refreshStream();
    this.twitchController.setStreaming(Boolean(stream));
  }

  async connectTwitchUser(
    interactive: Interactive,
    twitchId: string
  ): Promise<TwitchUser> {
    const twitchUser = await TwitchUser.findByExternalId(twitchId);
    if (!twitchUser) {
      throw new Error("Twitch user id in session not saved");
    }

    if (!interactive.avatar) {
      interactive.twitchUser = twitchUser;
      return twitchUser;
    }

    if (interactive.avatar.player.user.isGuest) {
      // promote guest user to normal user
      const player = await this.api.player.loadTwitchPlayer(twitchUser);
      interactive.avatar.player = player;
    } else {
      // regular user
      this.api.user.userForTwitchUser(
        interactive.avatar.player.user,
        twitchUser
      );
      await interactive.avatar.player.user.save();
    }
    if (interactive.twitchUser && interactive.twitchUser.id != twitchUser.id) {
      console.info(
        "User re-authenticated as new Twitch user",
        interactive.avatar.player.user
      );
    }

    interactive.twitchUser = twitchUser;
    return twitchUser;
  }

  async connectGitlabUser(
    interactive: Interactive,
    gitlabId: GitlabId
  ): Promise<GitlabUser> {
    const gitlabUser = await GitlabUser.findByExternalId(gitlabId);
    if (!gitlabUser) {
      throw new Error("Gitlab user in session not saved");
    }
    if (interactive.gitlabUser && interactive.gitlabUser.id != gitlabUser.id) {
      console.info(
        "User re-authenticated as new Gitlab user",
        interactive.avatar?.player.user
      );
    }
    interactive.gitlabUser = gitlabUser;
    return gitlabUser;
  }

  newInteractive(ws: WebSocket, session: SessionData) {
    return new Interactive(ws, session);
  }

  newGuestAvatar(): AvatarType {
    const player = this.api.player.newGuest();
    const avatar = new Avatar(player);
    return avatar;
  }

  async newTwitchAvatar(twitchUser: TwitchUser): Promise<AvatarType> {
    const player = await this.api.player.loadTwitchPlayer(twitchUser);
    const avatar = new Avatar(player);
    return avatar;
  }

  async onConnect(interactive: Interactive) {
    interactive.onConnect();
    if (!interactive.avatar) {
      const avatar = interactive.twitchUser
        ? await this.newTwitchAvatar(interactive.twitchUser)
        : this.newGuestAvatar();
      interactive.login(avatar);
    } else {
      interactive.login(interactive.avatar);
    }
  }

  onDisconnect(interactive: Interactive) {
    interactive.onDisconnect();
  }

  async registerTwitchUser(profile: TwitchProfile) {
    const twitchId = profile.id;
    let twitchUser = await TwitchUser.findByExternalId(twitchId);
    if (!twitchUser) {
      const api = this.backend.twitchApi?.helix;
      if (!api) {
        throw new Error("Twitch API not available");
      }
      const user = await api.users.getUserById(twitchId);
      if (!user) {
        throw new Error("User not found: " + twitchId);
      }
      twitchUser = await TwitchUser.fromApi(user);
    }

    twitchUser.email = profile.email;
    twitchUser.accessToken = profile.accessToken;
    twitchUser.refreshToken = profile.refreshToken;
    twitchUser.authenticatedAt = new Date();
    await twitchUser.save();
    return twitchUser;
  }

  async registerGitlabUser(profile: GitlabProfile) {
    const gitlabId = profile.apiResult.id;

    let gitlabUser = await GitlabUser.findByExternalId(gitlabId);
    if (!gitlabUser) {
      gitlabUser = await GitlabUser.fromApi(profile.apiResult);
    }
    gitlabUser.accessToken = profile.accessToken;
    gitlabUser.refreshToken = profile.refreshToken;
    gitlabUser.authenticatedAt = new Date();
    await gitlabUser.save();
    return gitlabUser;
  }

  allInteractives(): Interactive[] {
    return Array.from(this.backend.websockets.values());
  }

  findInteractive(avatar: AvatarType) {
    for (let interactive of this.allInteractives()) {
      if (interactive.avatar == avatar) {
        return interactive;
      }
    }
    return undefined;
  }

  findInteractiveByPlayer(playerModel: string) {
    for (let interactive of this.allInteractives()) {
      if (interactive.avatar?.player._id == playerModel) {
        return interactive;
      }
    }
    return undefined;
  }

  findInteractiveByUser(userModel: string) {
    for (let interactive of this.allInteractives()) {
      if (interactive.avatar?.player.user._id == userModel) {
        return interactive;
      }
    }
    return undefined;
  }

  async sendMessage(
    ws: WebSocket,
    action: PayloadAction<any>
  ): Promise<boolean> {
    if (!action.meta) {
      action.meta = {};
    }
    action.meta.sentTime = moment().valueOf();
    return await this.backend.sendClientMessage(ws, action);
  }

  onAction(interactive: Interactive, action: PayloadAction<any>) {
    if (!action.meta) {
      action.meta = {};
    }
    action.meta.receivedTime = moment().valueOf();
    interactive.onAction(action);
  }
}
