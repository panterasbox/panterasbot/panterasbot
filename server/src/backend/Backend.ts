import WebSocket from "ws";
import { SessionData } from "express-session";
import { ApiClient } from "twitch";
import * as Discord from "discord.js";
import { Gitlab } from "@gitbeaker/node";
import { getSecrets, getParameters } from "../util/aws";
import { Application } from "./Application";
import { Interactive } from "../mud/lib/connection/Interactive";
import { initialize as initModel } from "../model";
import { connectApp as connectTwitch } from "../server/twitch";
import { connectApp as connectGitlab } from "../server/gitlab";
import { TwitchBot } from "./TwitchBot";
import { TwitchWebhook } from "./TwitchWebhook";
import { GitlabUser } from "../mud/lib/gitlab/GitlabUser";
import { GitlabApiResult } from "../mud/lib/gitlab/GitlabController";

export type ActionMeta = {
  sentTime?: number;
  receivedTime?: number;
};

export type PayloadAction<P = void> = {
  type: string;
  meta?: ActionMeta;
  payload: P;
};

export class Backend {
  websocketServer: WebSocket.Server | undefined;
  websockets: Map<WebSocket, Interactive>;
  sessions: Map<string, Interactive>;
  secrets: { [key: string]: any } | undefined;
  parameters: { [key: string]: any } | undefined;
  twitchApi: ApiClient | undefined;
  gitlabApi: InstanceType<typeof Gitlab> | undefined;
  twitchBot: TwitchBot;
  twitchWebhook: TwitchWebhook;
  discordClient: Discord.Client | undefined;
  application: Application;

  constructor() {
    this.websockets = new Map();
    this.sessions = new Map();
    this.application = new Application(this);
    this.twitchBot = new TwitchBot(this);
    this.twitchWebhook = new TwitchWebhook(this);
  }

  async initialize() {
    this.secrets = await getSecrets();
    this.parameters = await getParameters();

    await initModel();
    await this.twitchWebhook.initialize();
    await this.application.initialize();
  }

  async start() {
    try {
      this.twitchApi = await connectTwitch(this);
    } catch (e) {
      console.error("Error initializing twitchApi", e);
    }
    try {
      this.gitlabApi = await connectGitlab(this);
    } catch (e) {
      console.error("Error initializing gitlabApi", e);
    }
    try {
      this.discordClient = await this.connectDiscord();
    } catch (e) {
      console.error("Error initializing discordClient", e);
    }

    await this.application.start();

    const subs = await this.twitchWebhook.getAllSubscriptions();
    for (const sub of subs) {
      await this.twitchWebhook.unsubscribe(sub);
    }

    await this.twitchWebhook.subscribe("stream.online");
    await this.twitchWebhook.subscribe("stream.offline");

    try {
      await this.twitchBot.start();
    } catch (e) {
      console.error("Error starting Twitch Bot", e);
    }

    console.info("Backend started.");
  }

  connectWsServer(wss: WebSocket.Server) {
    if (this.websocketServer && this.websocketServer.clients.size > 0) {
      throw new Error(
        "Can't reconnect websocket server while backend has active clients"
      );
    }
    this.websocketServer = wss;
    this.websockets.clear();
    this.sessions.clear();
  }

  async onClientConnection(ws: WebSocket, session: SessionData) {
    let interactive = this.sessions.get(session.sid);
    console.debug("Connected", interactive?.avatar?.player.user.id);
    if (interactive) {
      interactive.websocket = ws;
    } else {
      interactive = this.application.newInteractive(ws, session);
    }

    this.websockets.set(ws, interactive);
    this.sessions.set(session.sid, interactive);

    if (session.twitchId) {
      this.application.connectTwitchUser(interactive, session.twitchId);
    }
    if (session.gitlabId) {
      this.application.connectGitlabUser(interactive, session.gitlabId);
    }

    this.application.onConnect(interactive);
  }

  onClientMessage(ws: WebSocket, msg: any) {
    console.debug("received: %s", msg);
    let action = JSON.parse(msg);
    const interactive = this.websockets.get(ws);
    if (interactive) {
      this.application.onAction(interactive, action);
    }
  }

  onClientClose(ws: WebSocket, code: number, reason: string) {
    console.log("Websocket closed", code, reason);
    const interactive = this.websockets.get(ws);
    if (interactive) {
      interactive.websocket = undefined;
    }
    this.websockets.delete(ws);
    if (interactive) {
      this.application.onDisconnect(interactive);
    }
  }

  async sendClientMessage(ws: WebSocket, action: any): Promise<boolean> {
    let msg = JSON.stringify(action);
    console.debug("sending: %s", msg);
    const promise = new Promise<void>(function (resolve, reject) {
      ws.send(msg, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
    try {
      await promise;
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  }

  getSecret(secret: string): any {
    return this.secrets?.[secret];
  }

  getParameter(param: string): string {
    return this.parameters?.[param];
  }

  async onTwitchConnection(session: SessionData) {
    const twitchId = session?.passport?.user.data[0].id;
    const interactive = this.sessions.get(session.sid);

    if (twitchId && interactive) {
      await this.application.connectTwitchUser(interactive, twitchId);
      // XXX update client?
    }
  }

  async onGitlabConnection(session: SessionData) {
    const gitlabId = session?.passport?.user.id;
    const interactive = this.sessions.get(session.sid);
    if (gitlabId && interactive) {
      await this.application.connectGitlabUser(interactive, gitlabId);
      // XXX update client?
    }
  }

  async connectDiscord(): Promise<Discord.Client> {
    const client = new Discord.Client();

    const token = await client.login(this.getSecret("discord_bot_token"));

    return client;
  }

  async onTwitchAuth(profile: any, accessToken: any, refreshToken: any) {
    return await this.application.registerTwitchUser({
      id: profile.data[0].id,
      email: profile.data[0].email,
      accessToken: accessToken,
      refreshToken: refreshToken,
    });
  }

  async onGitlabAuth(
    profile: GitlabApiResult,
    accessToken: any,
    refreshToken: any
  ): Promise<GitlabUser> {
    return await this.application.registerGitlabUser({
      apiResult: profile,
      accessToken: accessToken,
      refreshToken: refreshToken,
    });
  }
}
