# Pantera's Bot Game Server

The game server. There's basically a backend that drives the game world, and then a websocket interface along with a graphql interface.

Check out the src/ directory for a deeper breakdown on how the code is organized.
